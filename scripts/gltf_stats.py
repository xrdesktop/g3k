#!/usr/bin/env python3

# xrdesktop
# Copyright 2022 Collabora Ltd.
# Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
# SPDX-License-Identifier: MIT

import subprocess
import os
import time
from tabulate import tabulate
import glob
import sys
import json
import pprint


def find_models(model_dir, filter_files=False):
    files = glob.glob(f'{model_dir}/**/*.gltf', recursive=True)
    # files += glob.glob(f'{model_dir}/**/*.glb', recursive=True)

    if not filter_files:
        files.sort()
        return files

    ignore_list = ["Draco", "Embedded", "Quantized", "IBL"]
    filtered_files = []

    for file in files:
        ignore = False
        for word in ignore_list:
            if word in file:
                ignore = True
        if not ignore:
            filtered_files.append(file)

    filtered_files.sort()

    return filtered_files


def print_usage():
    print("Usage:")
    print(f"{sys.argv[0]} <model_dir>")

def analyse_model(model_path):
    with open(model_path) as f:
        data = json.load(f)

    material_keys = set()

    extensions = set()
    pbr = set()

    if "materials" in data:
        for mat in data["materials"]:
            material_keys.update(mat.keys())

            if "extensions" in mat:
                extensions.update(mat["extensions"].keys())

            if "pbrMetallicRoughness" in mat:
                pbr.update(mat["pbrMetallicRoughness"].keys())

        if "name" in material_keys:
            material_keys.remove("name")

    attribs = set()
    for mesh in data["meshes"]:
        for p in mesh["primitives"]:
            attribs.update(p["attributes"].keys())

    return material_keys, attribs, extensions, pbr

def print_stats(all_by_name):
    unique = set()
    for keys in all_by_name.values():
        unique.update(keys)
    unique = sorted(unique)

    table = []
    for name, keys in all_by_name.items():
        row = [name]
        for k in unique:
            row.append("🟢" if k in keys else "🔴")
        table.append(row)

    header = [""] + list(unique)
    print(tabulate(table, headers=header))

def main():
    if len(sys.argv) != 2:
        print_usage()
        return

    models_dir = sys.argv[1]
    models = find_models(models_dir, True)

    all_material_keys = {}
    all_attribs = {}
    all_extensions = {}
    all_pbr = {}

    for model in models:
        file_name = os.path.basename(model)
        material_keys, attribs, extensions, pbr = analyse_model(model)

        all_material_keys[file_name] = material_keys
        all_attribs[file_name] = attribs
        if extensions:
            all_extensions[file_name] = extensions
        if pbr:
            all_pbr[file_name] = pbr

    print_stats(all_attribs)
    print_stats(all_material_keys)
    print_stats(all_extensions)
    print_stats(all_pbr)
main()
