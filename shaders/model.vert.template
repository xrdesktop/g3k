/*
 * gulkan
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#extension GL_EXT_multiview : enable

layout (binding = 0) uniform Transformation
{
  mat4 projection[2];
  mat4 view[2];
  mat4 model;
  vec3 eye_position[2];
}
transformation;

layout (location = 0) in vec3 position;
#ifdef HAVE_COLOR
layout (location = 1) in vec3 color;
#endif
#ifdef HAVE_NORMAL
layout (location = 2) in vec3 normal;
#endif
#ifdef HAVE_TANGENT
layout (location = 3) in vec3 tangent;
#endif
#ifdef HAVE_TEXCOORD_0
layout (location = 4) in vec2 uv;
#endif

layout (location = 0) out vec4 out_world_position;
layout (location = 1) out vec3 out_eye_position;
#ifdef HAVE_COLOR
layout (location = 2) out vec3 out_color;
#endif
#ifdef HAVE_NORMAL
layout (location = 3) out vec3 out_world_normal;
#endif
#ifdef HAVE_TANGENT
layout (location = 4) out vec3 out_world_tangent;
#endif
#ifdef HAVE_TEXCOORD_0
layout (location = 5) out vec2 out_uv;
#endif

out gl_PerVertex { vec4 gl_Position; };

const vec3 lights[3] = {
  vec3 (5.0f, 5.0f, -5.0f),
  vec3 (-10.0, 10.0, -10.0),
  vec3 (10.0, 10.0, 10.0),
};

void
main ()
{
  gl_Position = transformation.projection[gl_ViewIndex]
                * transformation.view[gl_ViewIndex] * transformation.model
                * vec4 (position, 1.0);
  gl_Position.y = -gl_Position.y;
#ifdef HAVE_COLOR
  out_color = color;
#endif
#ifdef HAVE_NORMAL
  out_world_normal = mat3 (transformation.model) * normal;
#endif
#ifdef HAVE_TANGENT
  out_world_tangent = mat3 (transformation.model) * tangent;
#endif
#ifdef HAVE_TEXCOORD_0
  out_uv = uv;
#endif
  out_world_position = transformation.model * vec4 (position, 1.0f);

  out_eye_position = transformation.eye_position[gl_ViewIndex];
}
