/*
 * Graphene Extensions
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-button-priv.h"

#include <cairo.h>
#include <gdk/gdk.h>
#include <pango/pangocairo.h>

#include "g3k-math.h"
#include "g3k-plane-priv.h"
#include "g3k-ppm.h"
#include "g3k-renderer.h"
#include "g3k-selection.h"

// TODO: Make this a setting or optimally determine it by swapchain size
#define G3K_DEFAULT_PPM 450.0f

typedef struct
{
  alignas (16) float color[4];
} G3kButtonShadingUniformBuffer;

typedef struct
{
  alignas (32) float mvp[2][16];
  alignas (32) float mv[2][16];
  alignas (16) float m[16];
} G3kButtonTransformationBuffer;

typedef struct _G3kButtonPrivate
{
  G3kPlane parent;

  GulkanUniformBuffer          *shading_buffer;
  G3kButtonShadingUniformBuffer shading_buffer_data;

  VkExtent2D extent;
} G3kButtonPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (G3kButton, g3k_button, G3K_TYPE_PLANE)

static void
_finalize (GObject *gobject);

static void
_draw (G3kObject *self, VkCommandBuffer cmd_buffer);

static gboolean
_update_selection (G3kObject *self, G3kSelection *selection)
{
  graphene_matrix_t window_transformation;
  g3k_object_get_matrix (G3K_OBJECT (self), &window_transformation);
  g3k_object_set_matrix (G3K_OBJECT (selection), &window_transformation);

  graphene_size_t size_meters = g3k_plane_get_mesh_size (G3K_PLANE (self));

  g3k_selection_set_quad (selection, size_meters.width, size_meters.height);

  return TRUE;
}

static void
g3k_button_class_init (G3kButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
  g3k_object_class->update_selection = _update_selection;
}

static void
g3k_button_init (G3kButton *self)
{
  (void) self;
}

gboolean
g3k_button_initialize (G3kButton       *self,
                       G3kContext      *g3k,
                       graphene_size_t *size_meters)
{
  g_assert (size_meters->width > 0.f && size_meters->height > 0.f);
  G3kButtonPrivate *priv = g3k_button_get_instance_private (self);
  priv->extent = g3k_size_to_extent (size_meters, G3K_DEFAULT_PPM);

  VkDeviceSize ubo_size = sizeof (G3kButtonTransformationBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "button");

  if (!g3k_object_initialize (G3K_OBJECT (self), g3k, pipeline, ubo_size))
    return FALSE;

  VkDeviceSize shading_ubo_size = sizeof (G3kButtonShadingUniformBuffer);

  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  priv->shading_buffer = gulkan_uniform_buffer_new (device, shading_ubo_size);
  if (!priv->shading_buffer)
    return FALSE;

  GulkanDescriptorSet *descriptor_set
    = g3k_object_get_descriptor_set (G3K_OBJECT (self));

  gulkan_descriptor_set_update_buffer (descriptor_set, 2, priv->shading_buffer);

  GulkanUniformBuffer *lights = g3k_renderer_get_lights_buffer (renderer);
  gulkan_descriptor_set_update_buffer (descriptor_set, 3, lights);

  g3k_button_reset_color (self);
  g3k_plane_set_mesh_size (G3K_PLANE (self), size_meters);

  return TRUE;
}

static void
_finalize (GObject *object)
{
  G3kButton        *self = G3K_BUTTON (object);
  G3kButtonPrivate *priv = g3k_button_get_instance_private (self);

  g_clear_object (&priv->shading_buffer);

  G_OBJECT_CLASS (g3k_button_parent_class)->finalize (object);
}

static void
_update_ubo (G3kButton *self)
{
  G3kButtonTransformationBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);
  graphene_matrix_to_float (&m_matrix, ub.m);

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *views = g3k_context_get_views (context);
  graphene_matrix_t *projections = g3k_context_get_projections (context);

#if 0
  graphene_point3d_t p;
  graphene_ext_matrix_get_translation_point3d (&m_matrix, &p);
  g_debug ("ubo model at %f %f %f", p.x, p.y, p.z);
#endif

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mv_matrix;
      graphene_matrix_multiply (&m_matrix, &views[eye], &mv_matrix);
      graphene_matrix_to_float (&mv_matrix, ub.mv[eye]);

      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&mv_matrix, &projections[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kButton *self = G3K_BUTTON (obj);
  if (!g3k_plane_has_texture (G3K_PLANE (self)))
    {
      /* g_warning ("Trying to draw window with no texture.\n"); */
      return;
    }

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);

  GulkanVertexBuffer *vb = g3k_plane_get_vertex_buffer (G3K_PLANE (self));
  gulkan_vertex_buffer_draw (vb, cmd_buffer);
}

/**
 * g3k_button_new:
 * @g3k: The #G3kContext
 * @size_meters: Size in meters.
 *
 * Creates a button .
 *
 * Returns: (transfer full) (nullable): a new #G3kButton representing the button
 */
G3kButton *
g3k_button_new (G3kContext *g3k, graphene_size_t *size_meters)
{
  G3kButton *self = (G3kButton *) g_object_new (G3K_TYPE_BUTTON, 0);
  if (!g3k_button_initialize (self, g3k, size_meters))
    {
      g_object_unref (self);
      g_printerr ("Could not create button.\n");
      return NULL;
    }

  return self;
}

static GdkPixbuf *
_load_pixbuf (const gchar *name)
{
  GError    *error = NULL;
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_resource (name, &error);

  if (error != NULL)
    {
      g_printerr ("Unable to read file: %s\n", error->message);
      g_error_free (error);
      return NULL;
    }

  return pixbuf;
}

static void
_draw_background (cairo_t *cr, VkExtent2D extent)
{
  double r0;
  if (extent.width < extent.height)
    r0 = (double) extent.height / 3.0;
  else
    r0 = (double) extent.width / 3.0;

  double radius = r0 * 4.0;
  double r1 = r0 * 5.0;

  double center_x = (double) extent.width / 2.0;
  double center_y = (double) extent.height / 2.0;

  double cx0 = center_x - r0 / 2.0;
  double cy0 = center_y - r0;
  double cx1 = center_x - r0;
  double cy1 = center_y - r0;

  cairo_pattern_t *pat = cairo_pattern_create_radial (cx0, cy0, r0, cx1, cy1,
                                                      r1);
  cairo_pattern_add_color_stop_rgba (pat, 0, .3, .3, .3, 1);
  cairo_pattern_add_color_stop_rgba (pat, 1, 0, 0, 0, 1);
  cairo_set_source (cr, pat);
  cairo_arc (cr, center_x, center_y, radius, 0, (double) (2.0f * M_PI));
  cairo_fill (cr);
  cairo_pattern_destroy (pat);
}

static void
_draw_icon (cairo_t *cr, GdkPixbuf *pixbuf, uint32_t width, int padding)
{
  /* Draw icon with padding */
  int    icon_w = gdk_pixbuf_get_width (pixbuf);
  double scale = width / (double) (icon_w + 2 * padding);
  cairo_scale (cr, scale, scale);
  gdk_cairo_set_source_pixbuf (cr, pixbuf, padding, padding);
  cairo_paint (cr);
}

static cairo_surface_t *
_create_surface_icon (unsigned char *image,
                      VkExtent2D     extent,
                      const gchar   *icon_url)
{
  cairo_surface_t *surface
    = cairo_image_surface_create_for_data (image, CAIRO_FORMAT_ARGB32,
                                           (int) extent.width,
                                           (int) extent.height,
                                           (int) extent.width * 4);

  cairo_t *cr = cairo_create (surface);
  _draw_background (cr, extent);

  GdkPixbuf *icon_pixbuf = _load_pixbuf (icon_url);
  if (icon_pixbuf == NULL)
    {
      g_printerr ("Could not load icon %s.\n", icon_url);
      return NULL;
    }
  _draw_icon (cr, icon_pixbuf, extent.width, 100);
  cairo_destroy (cr);
  g_object_unref (icon_pixbuf);

  return surface;
}

static void
_draw_text (cairo_t *cr, int lines, gchar *const *text, VkExtent2D extent)
{
  PangoLayout          *layout = pango_cairo_create_layout (cr);
  PangoFontDescription *description
    = pango_font_description_from_string ("Sans 32");
  pango_layout_set_font_description (layout, description);
  pango_font_description_free (description);

  uint64_t longest_line = 0;
  for (int i = 0; i < lines; i++)
    {
      if (strlen (text[i]) > longest_line)
        longest_line = strlen (text[i]);
    }

  double center_x = (double) extent.width / 2.0;
  double center_y = (double) extent.height / 2.0;

  for (int i = 0; i < lines; i++)
    {
      int text_width, text_height;
      pango_layout_set_text (layout, text[i], -1);
      pango_layout_get_size (layout, &text_width, &text_height);

      /* horizontally centered*/
      double x = center_x - (double) text_width / PANGO_SCALE / 2.0;
      double line_height = (double) text_height / PANGO_SCALE;
      double line_spacing = 0.25 * line_height;

      double y = center_y
                 - (0.5 * (lines * (line_height + line_spacing) - line_spacing))
                 + i * (line_height + line_spacing);

      cairo_move_to (cr, x, y);
      cairo_set_source_rgb (cr, 0.9, 0.9, 0.9);
      pango_cairo_update_layout (cr, layout);
      pango_cairo_show_layout (cr, layout);
    }

  g_object_unref (layout);
}

static cairo_surface_t *
_create_surface_text (unsigned char *image,
                      VkExtent2D     extent,
                      int            lines,
                      gchar *const  *text)
{
  cairo_surface_t *surface
    = cairo_image_surface_create_for_data (image, CAIRO_FORMAT_ARGB32,
                                           (int) extent.width,
                                           (int) extent.height,
                                           (int) extent.width * 4);

  cairo_t *cr = cairo_create (surface);

  _draw_background (cr, extent);
  _draw_text (cr, lines, text, extent);

  cairo_destroy (cr);

  return surface;
}

static void
_submit_cairo_surface (G3kButton *self, cairo_surface_t *surface)
{
  G3kContext *g3k = g3k_object_get_context (G3K_OBJECT (self));
  g_assert (g3k);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  VkImageLayout  layout = g3k_context_get_upload_layout (g3k);

  GulkanTexture *texture
    = gulkan_texture_new_from_cairo_surface (gulkan, surface,
                                             VK_FORMAT_B8G8R8A8_SRGB, layout);

  if (!texture)
    {
      g_printerr ("Could not create texture from cairo surface.\n");
      return;
    }

  g3k_plane_set_texture (G3K_PLANE (self), texture);
}

void
g3k_button_set_text (G3kButton *self, int label_count, gchar **label)
{
  G3kButtonPrivate *priv = g3k_button_get_instance_private (self);
  gsize             size = sizeof (unsigned char) * 4 * priv->extent.width
               * priv->extent.height;
  g_assert (size > 0);
  unsigned char *image = g_malloc (size);

  // g_print ("Create %d x %d surface\n", dim.width, dim.height);

  cairo_surface_t *surface = _create_surface_text (image, priv->extent,
                                                   label_count, label);

  if (!surface)
    {
      g_printerr ("Could not create cairo surface.\n");
      return;
    }

  _submit_cairo_surface (self, surface);

  g_free (image);

  cairo_surface_destroy (surface);
}

void
g3k_button_set_icon (G3kButton *self, const gchar *url)
{
  G3kButtonPrivate *priv = g3k_button_get_instance_private (self);
  gsize             size = sizeof (unsigned char) * 4 * priv->extent.width
               * priv->extent.height;
  g_assert (size > 0);
  unsigned char *image = g_malloc (size);

  cairo_surface_t *surface = _create_surface_icon (image, priv->extent, url);

  if (!surface)
    {
      g_printerr ("Could not create cairo surface.\n");
      return;
    }

  _submit_cairo_surface (self, surface);

  g_free (image);

  cairo_surface_destroy (surface);
}

void
g3k_button_set_color (G3kButton *self, const graphene_vec4_t *color)
{
  G3kButtonPrivate *priv = g3k_button_get_instance_private (self);
  graphene_vec4_to_float (color, priv->shading_buffer_data.color);
  gulkan_uniform_buffer_update (priv->shading_buffer,
                                (gpointer) &priv->shading_buffer_data);
}

void
g3k_button_set_selection_color (G3kButton *self, gboolean is_selected)
{
  graphene_vec4_t color;
  if (is_selected)
    {
      graphene_vec4_init (&color, 0.0f, 0.0f, 1.0f, 1.0f);
    }
  else
    {
      graphene_vec4_init (&color, 0.1f, 0.1f, 0.1f, 1.0f);
    }
  g3k_button_set_color (self, &color);
}

void
g3k_button_reset_color (G3kButton *self)
{
  graphene_vec4_t unmarked_color;
  graphene_vec4_init (&unmarked_color, 1.f, 1.f, 1.f, 1.0f);
  g3k_button_set_color (self, &unmarked_color);
}
