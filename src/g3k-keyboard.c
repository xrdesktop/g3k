/*
 * g3k
 * Copyright 2021 Remco Kranenburg <remco@burgsoft.nl>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-keyboard.h"

#include <json-glib/json-glib.h>

#include "g3k-container.h"
#include "g3k-types.h"

enum
{
  KEY_PRESS_EVENT,
  PRE_SHOW_EVENT,
  POST_HIDE_EVENT,
  LAST_SIGNAL
};

struct _G3kKeyboard
{
  GObject parent;

  gboolean    is_visible;
  GHashTable *modes;
  gchar      *active_mode;
};

static guint keyboard_signals[LAST_SIGNAL] = {0};

G_DEFINE_TYPE (G3kKeyboard, g3k_keyboard, G_TYPE_OBJECT)

static void
g3k_keyboard_finalize (GObject *gobject)
{
  G3kKeyboard *self = G3K_KEYBOARD (gobject);
  g_free (self->active_mode);
  g_hash_table_destroy (self->modes);
}

static void
g3k_keyboard_class_init (G3kKeyboardClass *klass)
{
  keyboard_signals[KEY_PRESS_EVENT]
    = g_signal_new ("key-press-event", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    G_TYPE_POINTER | G_SIGNAL_TYPE_STATIC_SCOPE);
  keyboard_signals[PRE_SHOW_EVENT] = g_signal_new ("pre-show-event",
                                                   G_TYPE_FROM_CLASS (klass),
                                                   G_SIGNAL_RUN_LAST, 0, NULL,
                                                   NULL, NULL, G_TYPE_NONE, 0,
                                                   NULL);
  keyboard_signals[POST_HIDE_EVENT] = g_signal_new ("post-hide-event",
                                                    G_TYPE_FROM_CLASS (klass),
                                                    G_SIGNAL_RUN_LAST, 0, NULL,
                                                    NULL, NULL, G_TYPE_NONE, 0,
                                                    NULL);

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_keyboard_finalize;
}

static void
g3k_keyboard_init (G3kKeyboard *self)
{
  self->is_visible = FALSE;
  self->modes = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                       g_object_unref);
}

static gboolean
_target_is_mode (G3kKeyboard *self, gchar *mode)
{
  guint                    length;
  g_autofree const gchar **mode_names = (const gchar **)
    g_hash_table_get_keys_as_array (self->modes, &length);

  for (guint i = 0; i < length; i++)
    {
      if (g_strcmp0 (mode, mode_names[i]) == 0)
        return TRUE;
    }

  return FALSE;
}

static void
_switch_mode (G3kKeyboard *self, gchar *mode)
{
  g_free (self->active_mode);
  self->active_mode = mode;

  guint                    length;
  g_autofree const gchar **mode_names = (const gchar **)
    g_hash_table_get_keys_as_array (self->modes, &length);

  for (guint i = 0; i < length; i++)
    {
      G3kContainer *container = g_hash_table_lookup (self->modes,
                                                     mode_names[i]);

      if (g_strcmp0 (mode, mode_names[i]) == 0)
        g3k_container_show (container);
      else
        g3k_container_hide (container);
    }

  g_debug ("Active mode is now: %s", mode);
}

void
g3k_keyboard_click_cb (G3kKeyboardButton *button,
                       GxrController     *controller,
                       gpointer           user_data)
{
  (void) controller;
  G3kKeyboard *keyboard = G3K_KEYBOARD (user_data);

  gchar *button_target = NULL;
  gchar *button_character = NULL;
  guint  button_keyval = 0;

  g_object_get (button, "target", &button_target, "character",
                &button_character, "keyval", &button_keyval, NULL);

  if (_target_is_mode (keyboard, button_target))
    {
      _switch_mode (keyboard, button_target);
    }
  else
    {
      G3kKeyEvent event = {
        .string = g_strdup (button_character),
        .keyval = button_keyval,
      };
      g_signal_emit (keyboard, keyboard_signals[KEY_PRESS_EVENT], 0, &event);
      g_free ((gchar *) event.string);
    }
}

static void
_init_buttons (G3kKeyboard *self, G3kContext *context)
{
  const gchar *const *language_names = g_get_language_names ();

  g_autoptr (GError) error = NULL;
  g_autoptr (GInputStream) layout_stream = NULL;

  for (guint i = 0; language_names[i] != NULL; i++)
    {
      g_autoptr (GString) filename = g_string_new ("/keyboard-layouts/");
      g_string_append (filename, language_names[i]);
      g_string_append (filename, ".json");

      layout_stream = g_resources_open_stream (filename->str,
                                               G_RESOURCE_LOOKUP_FLAGS_NONE,
                                               &error);

      if (error == NULL)
        {
          g_debug ("Loaded keyboard layout %s", language_names[i]);
          break;
        }

      g_error_free (error);
      error = NULL;
    }

  /* Try 'en' locale as fallback */
  if (error != NULL)
    {
      g_error_free (error);
      error = NULL;
      layout_stream = g_resources_open_stream ("/keyboard-layouts/en.json",
                                               G_RESOURCE_LOOKUP_FLAGS_NONE,
                                               &error);
    }

  if (error != NULL)
    {
      g_printerr ("Unable to open keyboard layout: %s\n", error->message);
      return;
    }

  if (layout_stream == NULL)
    {
      g_printerr ("Unable to open keyboard layout resource\n");
      return;
    }

  g_autoptr (GError) json_error = NULL;
  g_autoptr (JsonParser) parser = json_parser_new ();
  json_parser_load_from_stream (parser, layout_stream, NULL, &json_error);

  if (json_error)
    {
      g_printerr ("Unable to parse keyboard layout: %s\n", json_error->message);
      return;
    }

  JsonNode *jn_root = json_parser_get_root (parser);
  json_node_seal (jn_root);

  JsonObject  *jo_root = json_node_get_object (jn_root);
  const gchar *layout_name = json_object_get_string_member (jo_root, "name");
  g_debug ("Loading layout %s", layout_name);

  JsonArray *ja_modes = json_object_get_array_member (jo_root, "modes");
  guint      ja_modes_length = json_array_get_length (ja_modes);

  g_debug ("Loading keyboards");
  for (guint i = 0; i < ja_modes_length; i++)
    {
      G3kContainer *mode = g3k_container_new (context);
      g3k_container_set_attachment (mode, G3K_CONTAINER_ATTACHMENT_HEAD, NULL);
      g3k_container_set_layout (mode, G3K_CONTAINER_RELATIVE);
      g3k_container_set_distance (mode, 2.5f);
      g3k_container_hide (mode);

      JsonObject  *jo_mode = json_array_get_object_element (ja_modes, i);
      const gchar *mode_name = json_object_get_string_member (jo_mode, "name");

      g_debug ("Loading keyboard mode %s", mode_name);

      if (i == 0)
        self->active_mode = g_strdup (mode_name);

      JsonArray *ja_keys = json_object_get_array_member (jo_mode, "keys");
      guint      ja_keys_length = json_array_get_length (ja_keys);

      g_autoptr (GString) labels = g_string_new ("");
      for (guint j = 0; j < ja_keys_length; j++)
        {
          JsonObject *jo_key = json_array_get_object_element (ja_keys, j);

          const gchar *label;

          if (json_object_has_member (jo_key, "label"))
            label = json_object_get_string_member (jo_key, "label");
          else if (json_object_has_member (jo_key, "character"))
            label = json_object_get_string_member (jo_key, "character");
          else
            label = "";

          gfloat x = (float) json_object_get_double_member (jo_key, "x");
          gfloat y = (float) json_object_get_double_member (jo_key, "y");
          gfloat w = (float) json_object_get_double_member (jo_key, "w");

          graphene_size_t size_meters = {0.3f * w, 0.3f};

          G3kKeyboardButton *button = g3k_keyboard_button_new (context,
                                                               &size_meters);
          g3k_button_set_text (G3K_BUTTON (button), 1, (gchar **) &label);

          if (!button)
            {
              g_printerr ("Unable to create button: %s\n", label);
              return;
            }

          if (json_object_has_member (jo_key, "target"))
            {
              const gchar *target = json_object_get_string_member (jo_key,
                                                                   "target");
              g_object_set (button, "target", target, NULL);
            }

          if (json_object_has_member (jo_key, "character"))
            {
              const gchar *character
                = json_object_get_string_member (jo_key, "character");
              g_object_set (button, "character", character, NULL);
            }

          if (json_object_has_member (jo_key, "keyval"))
            {
              const guint keyval = (guint)
                json_object_get_int_member (jo_key, "keyval");
              g_object_set (button, "keyval", keyval, NULL);
            }

          graphene_point3d_t button_pos = {
            .x = 0.3f * (-5.0f + w / 2.0f + x),
            .y = size_meters.height * (2.0f - y),
            .z = 0.0f,
          };

          graphene_matrix_t relative_transform;
          graphene_matrix_init_translate (&relative_transform, &button_pos);

          g3k_container_add_child (mode, G3K_OBJECT (button),
                                   &relative_transform);

          if (j != 0)
            g_string_append (labels, ", ");
          g_string_append (labels, label);
        }
      g_debug ("Loaded buttons %s", labels->str);

      g_hash_table_insert (self->modes, g_strdup (mode_name), mode);
    }
}

G3kKeyboard *
g3k_keyboard_new (G3kContext *context)
{
  G3kKeyboard *self = (G3kKeyboard *) g_object_new (G3K_TYPE_KEYBOARD, NULL);
  _init_buttons (self, context);
  return self;
}

GHashTable *
g3k_keyboard_get_modes (G3kKeyboard *self)
{
  return self->modes;
}

void
g3k_keyboard_show (G3kKeyboard *self)
{
  g_signal_emit (self, keyboard_signals[PRE_SHOW_EVENT], 0, NULL);

  G3kContainer *mode = g_hash_table_lookup (self->modes, self->active_mode);
  g3k_container_show (mode);

  self->is_visible = TRUE;
}

void
g3k_keyboard_hide (G3kKeyboard *self)
{
  G3kContainer *mode = g_hash_table_lookup (self->modes, self->active_mode);
  g3k_container_hide (mode);

  self->is_visible = FALSE;

  g_signal_emit (self, keyboard_signals[POST_HIDE_EVENT], 0, NULL);
}

gboolean
g3k_keyboard_is_visible (G3kKeyboard *self)
{
  return self->is_visible;
}
