/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-container.h"

#include <graphene-ext.h>
#include <gxr.h>

#include "g3k-math.h"
#include "g3k-object-priv.h"

typedef struct
{
  G3kObject        *object;
  graphene_matrix_t relative_transform;
} ContainerObject;

struct _G3kContainer
{
  G3kObject parent;

  GSList *children;
  float   distance;

  graphene_matrix_t transform;
  GxrController    *controller;

  G3kContainerAttachment attachment;
  G3kContainerLayout     layout;
  gboolean               visible;

  gint64 last_step_timestamp;
};

G_DEFINE_TYPE (G3kContainer, g3k_container, G3K_TYPE_OBJECT)

static void
g3k_container_finalize (GObject *gobject);

static void
g3k_container_class_init (G3kContainerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = g3k_container_finalize;
}

static void
g3k_container_init (G3kContainer *self)
{
  self->children = NULL;
  self->distance = 0;
  self->layout = G3K_CONTAINER_VERTICAL;
  self->attachment = G3K_CONTAINER_ATTACHMENT_NONE;
  self->visible = TRUE;
  self->controller = NULL;
  self->last_step_timestamp = g_get_monotonic_time ();
}

/**
 * g3k_container_add_window:
 * @self: The container
 * @window: The window to add
 * @relative_transform: the transform of the window's center relative to the
 * container's center when G3K_CONTAINER_RELATIVE is used, ignored else (may
 * be NULL then).
 */
void
g3k_container_add_child (G3kContainer      *self,
                         G3kObject         *object,
                         graphene_matrix_t *relative_transform)
{
  ContainerObject *cw = g_malloc (sizeof (ContainerObject));
  cw->object = object;
  if (relative_transform != NULL)
    graphene_matrix_init_from_matrix (&cw->relative_transform,
                                      relative_transform);

  self->children = g_slist_append (self->children, cw);

  /* initial positioning not important, will be overridden by attachment */
  graphene_matrix_init_identity (&self->transform);

  g3k_object_set_visibility (object, self->visible);
}

void
g3k_container_remove_child (G3kContainer *self, G3kObject *object)
{
  int index = g_slist_index (self->children, object);
  if (index != -1)
    {
      self->children = g_slist_remove (self->children, object);
      self->children = g_slist_remove (self->children,
                                       g_slist_nth_data (self->children,
                                                         (guint) index));
    }
}

void
g3k_container_set_distance (G3kContainer *self, float distance)
{
  self->distance = distance;
}

/**
 * g3k_container_get_children:
 * @self: The #G3kContainer
 *
 * Returns: (element-type XrdWindow) (transfer container): A list of #XrdWindow
 * contained in this container. The list must be destroyed by the caller.
 */
GSList *
g3k_container_get_children (G3kContainer *self)
{
  GSList *l = NULL;
  for (GSList *cwl = self->children; cwl; cwl = cwl->next)
    {
      ContainerObject *cw = cwl->data;
      l = g_slist_append (l, cw->object);
    }

  return l;
}

float
g3k_container_get_distance (G3kContainer *self)
{
  return self->distance;
}

static void
_hmd_facing_pose (graphene_matrix_t  *hmd_pose,
                  graphene_point3d_t *look_at_point_ws,
                  graphene_matrix_t  *pose_ws)
{
  graphene_point3d_t hmd_location;
  graphene_ext_matrix_get_translation_point3d (hmd_pose, &hmd_location);

  graphene_point3d_t look_at_from_hmd = {
    .x = look_at_point_ws->x - hmd_location.x,
    .y = look_at_point_ws->y - hmd_location.y,
    .z = look_at_point_ws->z - hmd_location.z,
  };

  graphene_vec3_t look_at_direction;
  graphene_point3d_to_vec3 (&look_at_from_hmd, &look_at_direction);

  float azimuth, inclination;
  g3k_math_get_rotation_angles (&look_at_direction, &azimuth, &inclination);

  graphene_matrix_init_identity (pose_ws);
  graphene_matrix_rotate_x (pose_ws, inclination);
  graphene_matrix_rotate_y (pose_ws, -azimuth);

  g3k_math_matrix_set_translation_point (pose_ws, look_at_point_ws);
}

static void
_window_container_set_transformation (G3kContainer      *self,
                                      graphene_matrix_t *transform)
{
  float container_width = 0;
  float container_height = 0;

  for (GSList *cwl = self->children; cwl; cwl = cwl->next)
    {
      ContainerObject *cw = cwl->data;
      G3kObject       *object = cw->object;
      graphene_size_t  window_size
        = g3k_plane_get_global_size_meters (G3K_PLANE (object));
      if (self->layout == G3K_CONTAINER_VERTICAL)
        {
          container_height += window_size.height;
          container_width = fmaxf (container_width, window_size.width);
        }
      else if (self->layout == G3K_CONTAINER_HORIZONTAL)
        {
          container_height = fmaxf (container_height, window_size.height);
          container_width += window_size.height;
        }
    }

  /* How windows are placed:
   * Keep tally of left / top edge of unoccupied space in container in
   * x_offset/y_offset (-x left, +y up).
   *
   * In vertical layout:
   * Place window's center half the window height below the edge.
   * Then move the edge down the height of this window and repeat.
   * Finally, multiply with container's transform to place in world space.
   *
   * In horizontal layout: Same, but with width.
   */
  if (self->layout == G3K_CONTAINER_VERTICAL)
    {
      float y_offset = +container_height / 2.f;
      for (GSList *cwl = self->children; cwl; cwl = cwl->next)
        {
          ContainerObject *cw = cwl->data;
          G3kObject       *object = cw->object;
          graphene_size_t  window_size
            = g3k_plane_get_global_size_meters (G3K_PLANE (object));

          graphene_point3d_t xyoffset = {
            .x = 0.f,
            .y = y_offset - window_size.height / 2.f,
            .z = 0.f,
          };
          graphene_matrix_t window_transform;
          graphene_matrix_init_translate (&window_transform, &xyoffset);
          graphene_matrix_multiply (&window_transform, transform,
                                    &window_transform);

          y_offset -= window_size.height;

          g3k_object_set_matrix (object, &window_transform);
        }
    }
  else if (self->layout == G3K_CONTAINER_HORIZONTAL)
    {
      float x_offset = -container_width / 2.f;
      for (GSList *cwl = self->children; cwl; cwl = cwl->next)
        {
          ContainerObject *cw = cwl->data;
          G3kObject       *object = cw->object;
          graphene_size_t  window_size
            = g3k_plane_get_global_size_meters (G3K_PLANE (object));

          graphene_point3d_t xyoffset = {
            .x = x_offset + window_size.width / 2.f,
            .y = 0.f,
            .z = 0.f,
          };
          graphene_matrix_t window_transform;
          graphene_matrix_init_translate (&window_transform, &xyoffset);
          graphene_matrix_multiply (&window_transform, transform,
                                    &window_transform);

          x_offset += window_size.width;

          g3k_object_set_matrix (object, &window_transform);
        }
    }
  else if (self->layout == G3K_CONTAINER_RELATIVE)
    {
      for (GSList *cwl = self->children; cwl; cwl = cwl->next)
        {
          ContainerObject *cw = cwl->data;
          G3kObject       *object = cw->object;

          graphene_matrix_t window_transform;
          graphene_matrix_multiply (&cw->relative_transform, transform,
                                    &window_transform);
          g3k_object_set_matrix (object, &window_transform);
        }
    }

  graphene_matrix_init_from_matrix (&self->transform, transform);
}

static gboolean
_step_fov (G3kContainer *self)
{
  /* Containers outside fov_factor_outer * FOV size will be snapped to
   * fov_factor_outer * FOV size.
   *
   * Containers between fov_factor_outer * FOV size and
   * fov_factor_inner * FOV size will smoothly move in center direction */
  const float fov_factor_outer = 0.6f;
  const float fov_factor_inner = 0.25f;

  G3kContext *context = g3k_object_get_context (G3K_OBJECT (self));
  GxrContext *gxr = g3k_context_get_gxr (context);

  graphene_matrix_t hmd_pose;
  gxr_context_get_head_pose (gxr, &hmd_pose);
  graphene_matrix_t hmd_pose_inv;
  graphene_matrix_inverse (&hmd_pose, &hmd_pose_inv);

  /* _cs means camera (hmd) space, _ws means world space. */
  graphene_matrix_t wc_transform_ws = self->transform;
  graphene_matrix_t wc_transform_cs;
  graphene_matrix_multiply (&wc_transform_ws, &hmd_pose_inv, &wc_transform_cs);

  graphene_vec3_t wc_vec_cs;
  graphene_ext_matrix_get_translation_vec3 (&wc_transform_cs, &wc_vec_cs);

  float left, right, top, bottom;
  gxr_context_get_frustum_angles (gxr, GXR_EYE_LEFT, &left, &right, &top,
                                  &bottom);

  float left_inner = left * fov_factor_inner;
  float right_inner = right * fov_factor_inner;
  float top_inner = top * fov_factor_inner;
  float bottom_inner = bottom * fov_factor_inner;

  float left_outer = left * fov_factor_outer;
  float right_outer = right * fov_factor_outer;
  float top_outer = top * fov_factor_outer;
  float bottom_outer = bottom * fov_factor_outer;

  float radius = g3k_container_get_distance (self);

  /* azimuth: angle between view direction to window, "left-right" component.
   * inclination: angle between view directiont to window, "up-down" component.
   *
   * This reduces the problem in 3D space to a problem in 2D "angle space"
   * where azimuth and inclination are in [-180°,180°]x[-180°,180°].
   *
   * First the case where the window is completely out of view is handled:
   * Snapping the window towards the view direction is done by clamping
   * azimuth and inclination both towards the view direction angles (0, 0) until
   * any of the angles of the target FOV is hit.
   *
   * Then the case where the window is "too close" to being out of view is
   * handled: Angles describing a final target location for the window are
   * calculated by repeating the process towards a scaled down FOV, but it is
   * not snapped to the new position, but per frame moves proportional to the
   * remaining distance towards the target location.
   * */

  float azimuth, inclination;
  g3k_math_get_rotation_angles (&wc_vec_cs, &azimuth, &inclination);

  /* Bail early when the window already is in the "center area".
   * However still update the pose to reflect movement towards/away. */
  if (azimuth > left_inner && azimuth < right_inner && inclination < top_inner
      && inclination > bottom_inner)
    {
      // g_print ("Not moving head following window!\n");
      graphene_point3d_t new_pos_ws;
      g3k_math_sphere_to_3d_coords (azimuth, inclination, radius, &new_pos_ws);
      graphene_matrix_transform_point3d (&hmd_pose, &new_pos_ws, &new_pos_ws);

      graphene_matrix_t new_wc_pose_ws;
      _hmd_facing_pose (&hmd_pose, &new_pos_ws, &new_wc_pose_ws);

      _window_container_set_transformation (self, &new_wc_pose_ws);
      return TRUE;
    }

  /* Window is not visible: snap it onto the edge of the visible area. */
  if (azimuth < left_outer || azimuth > right_outer || inclination > top_outer
      || inclination < bottom_outer)
    {

      /* delta is used to snap windows a little closer towards the view center
       * because natural head movement doesn't suddenly stop, it slows down,
       * making it snap very small distances before it leaves the snap phase
       * and enters the smooth movement phase. This would make the transition
       * from slowing snapping to fast smooth movement look like a jump. */
      float            delta = 1.0;
      graphene_point_t bottom_left = {
        .x = left_outer + delta,
        .y = bottom_outer + delta,
      };
      graphene_point_t top_right = {
        .x = right_outer - delta,
        .y = top_outer - delta,
      };
      graphene_point_t azimuth_inclination = {.x = azimuth, .y = inclination};

      graphene_point_t intersection_azimuth_inclination;
      gboolean         intersects
        = g3k_math_clamp_towards_zero_2d (&bottom_left, &top_right,
                                          &azimuth_inclination,
                                          &intersection_azimuth_inclination);

      /* doesn't happen */
      if (!intersects)
        {
          g_print ("Head Following Window should intersect, but doesn't!\n");
          return TRUE;
        }

      graphene_point3d_t new_pos_ws;
      g3k_math_sphere_to_3d_coords (intersection_azimuth_inclination.x,
                                    intersection_azimuth_inclination.y, radius,
                                    &new_pos_ws);
      graphene_matrix_transform_point3d (&hmd_pose, &new_pos_ws, &new_pos_ws);

      graphene_matrix_t new_wc_pose_ws;
      _hmd_facing_pose (&hmd_pose, &new_pos_ws, &new_wc_pose_ws);

      _window_container_set_transformation (self, &new_wc_pose_ws);

      graphene_vec2_t velocity;
      graphene_vec2_init (&velocity,
                          inclination - intersection_azimuth_inclination.y,
                          azimuth - intersection_azimuth_inclination.x);

      // g_print ("Snap window to view frustum edge!\n");
      return TRUE;
    }

  /* Window is visible, but not in center area: move it towards center area*/
  graphene_point_t bottom_left = {.x = left_inner, .y = bottom_inner};
  graphene_point_t top_right = {.x = right_inner, .y = top_inner};
  graphene_point_t azimuth_inclination = {.x = azimuth, .y = inclination};

  graphene_point_t intersection_azimuth_inclination;
  gboolean         intersects
    = g3k_math_clamp_towards_zero_2d (&bottom_left, &top_right,
                                      &azimuth_inclination,
                                      &intersection_azimuth_inclination);

  /* doesn't happen */
  if (!intersects)
    {
      g_print ("Head Following Window should intersect, but doesn't!\n");
      return TRUE;
    }

  float azimuth_diff = azimuth - intersection_azimuth_inclination.x;
  float inclination_diff = inclination - intersection_azimuth_inclination.y;

  float ms_since_last_step = (float) (g_get_monotonic_time ()
                                      - self->last_step_timestamp)
                             / 1000.f;

  /* Container moves by a fixed fraction of the distance from current point
   * to target point, i.e. on a linear deceleration curve. */
  graphene_vec2_t angle_velocity;
  graphene_vec2_init (&angle_velocity, azimuth_diff, inclination_diff);
  float remaining_angle_distance = graphene_vec2_length (&angle_velocity);
  float distance_speed_factor = ms_since_last_step / 1000.f * 7.f;
  float angle_speed = remaining_angle_distance * distance_speed_factor;

  graphene_vec2_normalize (&angle_velocity, &angle_velocity);
  graphene_vec2_scale (&angle_velocity, angle_speed, &angle_velocity);

  graphene_vec2_t current_angles;
  graphene_vec2_init (&current_angles, azimuth, inclination);

  graphene_vec2_t next_angles;
  graphene_vec2_subtract (&current_angles, &angle_velocity, &next_angles);

  graphene_point3d_t next_point_cs;
  g3k_math_sphere_to_3d_coords (graphene_vec2_get_x (&next_angles),
                                graphene_vec2_get_y (&next_angles), radius,
                                &next_point_cs);

  graphene_point3d_t next_point_ws;
  graphene_matrix_transform_point3d (&hmd_pose, &next_point_cs, &next_point_ws);

  graphene_matrix_t new_wc_pose_ws;
  _hmd_facing_pose (&hmd_pose, &next_point_ws, &new_wc_pose_ws);
  _window_container_set_transformation (self, &new_wc_pose_ws);

  // g_print ("Moving head following window with %f!\n", angle_speed);
  return TRUE;
}

static gboolean
_step_hand (G3kContainer *self)
{
  if (!G_IS_OBJECT (self->controller))
    {
      /* controller is turned off */
      self->controller = NULL;
      return FALSE;
    }

  graphene_matrix_t container_transform;
  /* hand_grip "lays" in xz plane, but window is in xy plane. */
  graphene_matrix_init_rotate (&container_transform, -60,
                               graphene_vec3_x_axis ());

  graphene_point3d_t offset = {.x = .0f, .y = .055f, .z = -.3f};
  graphene_matrix_translate (&container_transform, &offset);

  graphene_matrix_t controller_pose;
  gxr_controller_get_hand_grip_pose (self->controller, &controller_pose);

  graphene_matrix_multiply (&container_transform, &controller_pose,
                            &container_transform);

  _window_container_set_transformation (self, &container_transform);
  return TRUE;
}

/**
 * g3k_container_step:
 * @self: The #G3kContainer
 *
 * Updates the container's position based on its attachment.
 *
 * Returns: A #gboolean if that is %TRUE the step was successful.
 */
gboolean
g3k_container_step (G3kContainer *self)
{
  gboolean ret = FALSE;
  switch (self->attachment)
    {
      case (G3K_CONTAINER_ATTACHMENT_HEAD):
        {
          ret = _step_fov (self);
          break;
        }
      case (G3K_CONTAINER_ATTACHMENT_HAND):
        {
          ret = _step_hand (self);
          break;
        }
      case (G3K_CONTAINER_ATTACHMENT_NONE):
        {
          ret = TRUE;
          break;
        }
    }

  self->last_step_timestamp = g_get_monotonic_time ();
  return ret;
}

/**
 * g3k_container_set_attachment:
 * @self: The container.
 * @attachment: The attachment to set.
 * @controller: A controller used for G3K_CONTAINER_ATTACHMENT_HAND. May be
 * NULL for other attachments.
 */
void
g3k_container_set_attachment (G3kContainer          *self,
                              G3kContainerAttachment attachment,
                              GxrController         *controller)
{
  self->attachment = attachment;
  self->controller = controller;

  switch (attachment)
    {
      case (G3K_CONTAINER_ATTACHMENT_HEAD):
        {
          break;
        }
      case (G3K_CONTAINER_ATTACHMENT_HAND):
        {
          break;
        }
      case (G3K_CONTAINER_ATTACHMENT_NONE):
        {
          break;
        }
    }
}

void
g3k_container_set_layout (G3kContainer *self, G3kContainerLayout layout)
{
  self->layout = layout;
}

void
g3k_container_hide (G3kContainer *self)
{
  for (GSList *cwl = self->children; cwl; cwl = cwl->next)
    {
      ContainerObject *cw = cwl->data;
      G3kObject       *object = cw->object;
      g3k_object_set_visibility (object, FALSE);
    }
  self->visible = FALSE;
}

void
g3k_container_show (G3kContainer *self)
{
  for (GSList *cwl = self->children; cwl; cwl = cwl->next)
    {
      ContainerObject *cw = cwl->data;
      G3kObject       *object = cw->object;
      g3k_object_set_visibility (object, TRUE);
    }
  self->visible = TRUE;
}

gboolean
g3k_container_is_visible (G3kContainer *self)
{
  return self->visible;
}

/**
 * g3k_container_center_view:
 * @self: The container.
 * @context: A #GxrContext
 * @distance: The distance from the HMD the container should have.
 *
 * Places the container in the center of the FOV at the given distance.
 */
void
g3k_container_center_view (G3kContainer *self, float distance)
{
  G3kContext       *context = g3k_object_get_context (G3K_OBJECT (self));
  GxrContext       *gxr = g3k_context_get_gxr (context);
  graphene_matrix_t hmd_transform;
  if (!gxr_context_get_head_pose (gxr, &hmd_transform))
    {
      g_printerr ("Could not get head pose.\n");
      return;
    }

  graphene_point3d_t distance_point = {
    .x = 0,
    .y = 0,
    .z = -distance,
  };

  graphene_matrix_t container_transform;
  graphene_matrix_init_translate (&container_transform, &distance_point);

  graphene_matrix_multiply (&container_transform, &hmd_transform,
                            &container_transform);

  _window_container_set_transformation (self, &container_transform);
}

G3kContainer *
g3k_container_new (G3kContext *context)
{
  G3kContainer *self = (G3kContainer *) g_object_new (G3K_TYPE_CONTAINER, 0);
  g3k_object_set_context (G3K_OBJECT (self), context);
  return self;
}

static void
g3k_container_finalize (GObject *gobject)
{
  G3kContainer *self = G3K_CONTAINER (gobject);
  g_slist_free_full (self->children, g_free);
  (void) self;

  G_OBJECT_CLASS (g3k_container_parent_class)->finalize (gobject);
}
