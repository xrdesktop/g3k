/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-renderer.h"

#include "g3k-controller.h"

#if defined(RENDERDOC)
#include "renderdoc_app.h"
#include <dlfcn.h>
static RENDERDOC_API_1_1_2 *rdoc_api = NULL;

static void
_init_renderdoc ()
{
  if (rdoc_api != NULL)
    return;

  void *mod = dlopen ("librenderdoc.so", RTLD_NOW | RTLD_NOLOAD);
  if (mod)
    {
      pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)
        dlsym (mod, "RENDERDOC_GetAPI");
      int ret = RENDERDOC_GetAPI (eRENDERDOC_API_Version_1_1_2,
                                  (void **) &rdoc_api);
      if (ret != 1)
        g_debug ("Failed to init renderdoc");
    }
}
#endif

typedef struct
{
  graphene_point3d_t position;
  graphene_point_t   uv;
} G3kVertex;

typedef struct
{
  float position[4];
  float color[3];
  float radius;
} G3kLight;

typedef struct
{
  G3kLight lights[2];
  int      active_lights;
} G3kLights;

struct _G3kRenderer
{
  GulkanRenderer parent;

  G3kContext *context;

  GHashTable *pipelines;

  GulkanRenderPass *render_pass;

  G3kLights            lights;
  GulkanUniformBuffer *lights_buffer;

  VkSampleCountFlagBits sample_count;
  float                 render_scale;
};

G_DEFINE_TYPE (G3kRenderer, g3k_renderer, GULKAN_TYPE_RENDERER)

static void
g3k_renderer_init (G3kRenderer *self)
{
  self->render_scale = 1.0f;

  self->lights.active_lights = 0;
  graphene_vec4_t position;
  graphene_vec4_init (&position, 0, 0, 0, 1);

  graphene_vec4_t color;
  graphene_vec4_init (&color, .078f, .471f, .675f, 1);

  for (uint32_t i = 0; i < 2; i++)
    {
      graphene_vec4_to_float (&position, self->lights.lights[i].position);
      graphene_vec4_to_float (&color, self->lights.lights[i].color);
      self->lights.lights[i].radius = 0.1f;
    }

  self->context = NULL;

  self->pipelines = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                           g_object_unref);
}

static void
_finalize (GObject *gobject)
{
  G3kRenderer *self = G3K_RENDERER (gobject);

  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  gulkan_device_wait_idle (device);

  g_clear_object (&self->lights_buffer);
  g_hash_table_destroy (self->pipelines);
  g_clear_object (&self->render_pass);

  G_OBJECT_CLASS (g3k_renderer_parent_class)->finalize (gobject);
}

static void
g3k_renderer_class_init (G3kRendererClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static gboolean
_init (G3kRenderer *self);

G3kRenderer *
g3k_renderer_new (G3kContext *context)
{
  G3kRenderer *self = (G3kRenderer *) g_object_new (G3K_TYPE_RENDERER, 0);
  self->context = context;
  if (!_init (self))
    {
      g_object_unref (self);
      g_printerr ("Could not init Vulkan.\n");
      return NULL;
    }
  return self;
}

static gboolean
_init_framebuffers (G3kRenderer *self)
{
  GxrContext *gxr = g3k_context_get_gxr (self->context);
  VkExtent2D  extent = gxr_context_get_swapchain_extent (gxr, 0);
  extent.width = (uint32_t) (self->render_scale * (float) extent.width);
  extent.height = (uint32_t) (self->render_scale * (float) extent.height);

  gulkan_renderer_set_extent (GULKAN_RENDERER (self), extent);

  if (!gxr_context_init_framebuffers (gxr, extent, self->sample_count,
                                      &self->render_pass))
    return FALSE;

  return TRUE;
}

static gboolean
_init_pipelines (G3kRenderer *self)
{
  VkExtent2D extent = gulkan_renderer_get_extent (GULKAN_RENDERER (self));

  {
    VkDescriptorSetLayoutBinding bindings[] = {
      // mvp buffer
      {
        .binding = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
      },
      // Window and device texture
      {
        .binding = 1,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      },
      // Window buffer
      {
        .binding = 2,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      },
      // Lights buffer
      {
        .binding = 3,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      },
    };

    GulkanPipelineConfig window_config = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/window.vert.spv",
      .fragment_shader_uri = "/shaders/window.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32_SFLOAT, offsetof (G3kVertex, uv)},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (G3kVertex),
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_TRUE,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                          VK_COLOR_COMPONENT_G_BIT |
                          VK_COLOR_COMPONENT_B_BIT |
                          VK_COLOR_COMPONENT_A_BIT,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .lineWidth = 1.0f,
      },
    };

    G3kPipeline *window_pipeline = g3k_pipeline_new (self->context, bindings,
                                                     G_N_ELEMENTS (bindings),
                                                     512, &window_config,
                                                     self->render_pass);

    g_hash_table_insert (self->pipelines, "window", window_pipeline);

    GulkanPipelineConfig button_config = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/button.vert.spv",
      .fragment_shader_uri = "/shaders/button.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32_SFLOAT, offsetof (G3kVertex, uv)},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (G3kVertex),
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_FALSE,
        .colorWriteMask = 0xf,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .lineWidth = 1.0f,
      },
    };

    G3kPipeline *button_pipeline = g3k_pipeline_new (self->context, bindings,
                                                     G_N_ELEMENTS (bindings),
                                                     512, &button_config,
                                                     self->render_pass);

    g_hash_table_insert (self->pipelines, "button", button_pipeline);
  }
  {
    VkDescriptorSetLayoutBinding bindings[] = {
      // mvp buffer
      {
        .binding = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
      },
      // Window and device texture
      {
        .binding = 1,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
      },
    };

    GulkanPipelineConfig config_tip = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/quad.vert.spv",
      .fragment_shader_uri = "/shaders/quad.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32_SFLOAT, offsetof (G3kVertex, uv)},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (G3kVertex),
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_FALSE,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_TRUE,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                          VK_COLOR_COMPONENT_G_BIT |
                          VK_COLOR_COMPONENT_B_BIT |
                          VK_COLOR_COMPONENT_A_BIT,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
      },
    };

    G3kPipeline *tip_pipeline = g3k_pipeline_new (self->context, bindings,
                                                  G_N_ELEMENTS (bindings), 16,
                                                  &config_tip,
                                                  self->render_pass);

    g_hash_table_insert (self->pipelines, "tip", tip_pipeline);
  }
  {
    VkDescriptorSetLayoutBinding bindings[] = {
      // mvp buffer
      {
        .binding = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
      },
    };

    GulkanPipelineConfig config_pointer = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/pointer.vert.spv",
      .fragment_shader_uri = "/shaders/pointer.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32B32_SFLOAT, sizeof (float) * 3},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (float) * 6,
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_FALSE,
        .colorWriteMask = 0xf,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_LINE,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .lineWidth = 4.0f,
      },
    };

    G3kPipeline *pointer_pipeline = g3k_pipeline_new (self->context, bindings,
                                                      G_N_ELEMENTS (bindings),
                                                      16, &config_pointer,
                                                      self->render_pass);

    g_hash_table_insert (self->pipelines, "pointer", pointer_pipeline);

    GulkanPipelineConfig config_selection = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/pointer.vert.spv",
      .fragment_shader_uri = "/shaders/pointer.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32B32_SFLOAT, sizeof (float) * 3},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (float) * 6,
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_FALSE,
        .colorWriteMask = 0xf,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_LINE,
        .lineWidth = 2.0f,
      },
    };

    G3kPipeline *selection_pipeline = g3k_pipeline_new (self->context, bindings,
                                                        G_N_ELEMENTS (bindings),
                                                        16, &config_selection,
                                                        self->render_pass);

    g_hash_table_insert (self->pipelines, "selection", selection_pipeline);

    GulkanPipelineConfig config_background = {
      .extent = extent,
      .sample_count = self->sample_count,
      .vertex_shader_uri = "/shaders/pointer.vert.spv",
      .fragment_shader_uri = "/shaders/pointer.frag.spv",
      .topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
      .attribs = (VkVertexInputAttributeDescription []) {
        {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
        {1, 0, VK_FORMAT_R32G32B32_SFLOAT, sizeof (float) * 3},
      },
      .attrib_count = 2,
      .bindings = &(VkVertexInputBindingDescription) {
        .binding = 0,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        .stride = sizeof (float) * 6,
      },
      .binding_count = 1,
      .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
      },
      .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
        .blendEnable = VK_FALSE,
        .colorWriteMask = 0xf,
      },
      .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_LINE,
        .lineWidth = 1.0f,
      }
    };

    G3kPipeline *background_pipeline
      = g3k_pipeline_new (self->context, bindings, G_N_ELEMENTS (bindings), 1,
                          &config_background, self->render_pass);
    g_hash_table_insert (self->pipelines, "background", background_pipeline);
  }

  return TRUE;
}

static gboolean
_init (G3kRenderer *self)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);

  gulkan_renderer_set_context (GULKAN_RENDERER (self), gulkan);
  self->sample_count = VK_SAMPLE_COUNT_1_BIT;

  if (!_init_framebuffers (self))
    return FALSE;

  GulkanDevice *device = gulkan_context_get_device (gulkan);
  self->lights_buffer = gulkan_uniform_buffer_new (device, sizeof (G3kLights));

  if (!self->lights_buffer)
    return FALSE;

  if (!_init_pipelines (self))
    return FALSE;

  return TRUE;
}

static void
_render_stereo (G3kRenderer *self, VkCommandBuffer cmd_buffer)
{
  VkExtent2D extent = gulkan_renderer_get_extent (GULKAN_RENDERER (self));

  VkClearColorValue clear_color = {
    .float32 = {0.0f, 0.0f, 0.0f, 0.0f},
  };

  GxrContext        *gxr = g3k_context_get_gxr (self->context);
  GulkanFrameBuffer *framebuffer = gxr_context_get_acquired_framebuffer (gxr);

  if (!GULKAN_IS_FRAME_BUFFER (framebuffer))
    {
      g_printerr ("framebuffer invalid\n");
    }

  gulkan_render_pass_begin (self->render_pass, extent, clear_color, framebuffer,
                            cmd_buffer);

  g3k_object_draw (g3k_context_get_root (self->context), cmd_buffer);

  vkCmdEndRenderPass (cmd_buffer);
}

static void
_update_lights (G3kRenderer *self)
{
  GList *controllers = g3k_context_get_controllers (self->context);
  guint  controller_count = g_list_length (controllers);

  self->lights.active_lights = 0;

  for (guint i = 0; i < controller_count; i++)
    {
      G3kController *controller = G3K_CONTROLLER (g_list_nth_data (controllers,
                                                                   i));

      G3kTip *tip = g3k_controller_get_tip (controller);

      // TODO: should lighting be tied to tip visibility?
      if (!g3k_object_is_visible (G3K_OBJECT (tip)))
        {
          continue;
        }

      if (i > 2)
        {
          g_warning ("Update lights received more than 2 visible "
                     "controller tips.\n");
          break;
        }

      graphene_point3d_t tip_position;
      g3k_object_get_position (G3K_OBJECT (tip), &tip_position);

      self->lights.lights[i].position[0] = tip_position.x;
      self->lights.lights[i].position[1] = tip_position.y;
      self->lights.lights[i].position[2] = tip_position.z;

      self->lights.active_lights++;
    }

  gulkan_uniform_buffer_update (self->lights_buffer, (gpointer) &self->lights);
}

gboolean
g3k_renderer_draw (G3kRenderer *self)
{
#if defined(RENDERDOC)
  if (rdoc_api)
    rdoc_api->StartFrameCapture (NULL, NULL);
  else
    _init_renderdoc ();
#endif

  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);

  GulkanQueue *queue = gulkan_device_get_graphics_queue (device);

  GulkanCmdBuffer *cmd_buffer = gulkan_queue_request_cmd_buffer (queue);
  gulkan_cmd_buffer_begin_one_time (cmd_buffer);

  _update_lights (self);

  VkCommandBuffer cmd_handle = gulkan_cmd_buffer_get_handle (cmd_buffer);

  _render_stereo (self, cmd_handle);

  gulkan_queue_end_submit (queue, cmd_buffer);

  gulkan_queue_free_cmd_buffer (queue, cmd_buffer);

#if defined(RENDERDOC)
  if (rdoc_api)
    rdoc_api->EndFrameCapture (NULL, NULL);
#endif
  return TRUE;
}

GulkanUniformBuffer *
g3k_renderer_get_lights_buffer (G3kRenderer *self)
{
  return self->lights_buffer;
}

GulkanRenderPass *
g3k_renderer_get_render_pass (G3kRenderer *self)
{
  return self->render_pass;
}

G3kPipeline *
g3k_renderer_get_pipeline (G3kRenderer *self, const gchar *name)
{
  return g_hash_table_lookup (self->pipelines, name);
}

VkSampleCountFlagBits
g3k_renderer_get_sample_count (G3kRenderer *self)
{
  return self->sample_count;
}
