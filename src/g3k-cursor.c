/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-cursor.h"

#include <graphene-ext.h>

#include "g3k-plane-priv.h"
#include "g3k-renderer.h"
#include "g3k-settings.h"

/*
 * The distance in meters for which apparent size and regular size are equal.
 */
#define G3K_CURSOR_APPARENT_SIZE_DISTANCE 3.0f

struct _G3kCursor
{
  G3kPlane parent;

  gboolean keep_apparent_size;
  float    default_width_meters;

  VkOffset2D hotspot;
};

G_DEFINE_TYPE (G3kCursor, g3k_cursor, G3K_TYPE_PLANE)

static void
g3k_cursor_finalize (GObject *gobject);

static GulkanVertexBuffer *
_init_mesh (G3kPlane *plane)
{
  // Without a texture we can't calculate the hotspot offset.
  GulkanTexture *texture = g3k_plane_get_texture (plane);
  if (!texture)
    {
      return NULL;
    }

  G3kContext    *g3k = g3k_object_get_context (G3K_OBJECT (plane));
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);

  GulkanVertexBuffer *vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

  VkExtent2D extent = gulkan_texture_get_extent (texture);

  graphene_size_t size_meters = g3k_plane_get_mesh_size (plane);

  float ppm = (float) extent.width / size_meters.width;

  G3kCursor *self = G3K_CURSOR (plane);

  graphene_point_t hotspot_meters = {.x = (float) self->hotspot.x / ppm,
                                     .y = (float) self->hotspot.y / ppm};

  graphene_point_t from = {
    .x = 0.0f - hotspot_meters.x,
    .y = -size_meters.height + hotspot_meters.y,
  };
  graphene_point_t to = {
    .x = size_meters.width - hotspot_meters.x,
    .y = 0.f + hotspot_meters.y,
  };

  graphene_matrix_t m;
  graphene_matrix_init_identity (&m);
  gulkan_geometry_append_plane (vertex_buffer, &from, &to, &m);

  if (!gulkan_vertex_buffer_alloc_array (vertex_buffer))
    {
      g_object_unref (vertex_buffer);
      return NULL;
    }

  return vertex_buffer;
}

static void
_update_texture_size (G3kPlane *plane)
{
  // If we already have a texture, apply aspect ratio
  float          aspect_ratio = 1.0f;
  GulkanTexture *texture = g3k_plane_get_texture (plane);
  if (texture)
    {
      VkExtent2D extent = gulkan_texture_get_extent (texture);
      aspect_ratio = (float) extent.width / (float) extent.height;
    }

  G3kCursor      *self = G3K_CURSOR (plane);
  graphene_size_t size = {
    .width = self->default_width_meters,
    .height = self->default_width_meters / aspect_ratio,
  };
  g3k_plane_set_mesh_size (plane, &size);
}

static void
g3k_cursor_class_init (G3kCursorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = g3k_cursor_finalize;

  G3kPlaneClass *plane_class = G3K_PLANE_CLASS (klass);
  plane_class->init_mesh = _init_mesh;
  plane_class->update_texture_size = _update_texture_size;
}

static void
g3k_cursor_init (G3kCursor *self)
{
  self->keep_apparent_size = FALSE;
  self->hotspot.x = 0;
  self->hotspot.y = 0;
}

static void
_update_width_meters (GSettings *settings, gchar *key, gpointer _data)
{
  G3kCursor *self = (G3kCursor *) _data;
  self->default_width_meters = (float) g_settings_get_double (settings, key);

  _update_texture_size (G3K_PLANE (self));
}

static void
_update_keep_apparent_size (GSettings *settings, gchar *key, gpointer _data)
{
  G3kCursor *self = (G3kCursor *) _data;

  self->keep_apparent_size = g_settings_get_boolean (settings, key);

  if (!self->keep_apparent_size)
    {
      g3k_object_reset_local_scale (G3K_OBJECT (self));
    }
}

G3kCursor *
g3k_cursor_new (G3kContext *g3k)
{
  G3kCursor *self = (G3kCursor *) g_object_new (G3K_TYPE_CURSOR, 0);

  VkDeviceSize ubo_size = sizeof (G3kPlaneUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "tip");

  if (!g3k_object_initialize (G3K_OBJECT (self), g3k, pipeline, ubo_size))
    return FALSE;

  g3k_settings_connect_and_apply (G_CALLBACK (_update_width_meters), "org.g3k",
                                  "desktop-cursor-width-meters", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_keep_apparent_size),
                                  "org.g3k", "pointer-tip-keep-apparent-size",
                                  self);

  return self;
}

static void
g3k_cursor_finalize (GObject *gobject)
{
  G3kCursor *self = G3K_CURSOR (gobject);
  (void) self;

  G_OBJECT_CLASS (g3k_cursor_parent_class)->finalize (gobject);
}

/**
 * g3k_cursor_set_hotspot:
 * @self: The #G3kCursor
 * @hotspot_x: The x component of the hotspot.
 * @hotspot_y: The y component of the hotspot.
 *
 * A hotspot of (x, y) means that the hotspot is at x pixels right, y pixels
 * down from the top left corner of the texture.
 */
void
g3k_cursor_set_hotspot (G3kCursor *self, int hotspot_x, int hotspot_y)
{
  self->hotspot.x = hotspot_x;
  self->hotspot.y = hotspot_y;
  g3k_plane_update_mesh (G3K_PLANE (self));
}

static void
_update_apparent_size (G3kCursor *self, graphene_point3d_t *cursor_point)
{
  if (!self->keep_apparent_size)
    return;

  graphene_matrix_t hmd_pose;

  GxrContext *gxr
    = g3k_context_get_gxr (g3k_object_get_context (G3K_OBJECT (self)));
  gboolean has_pose = gxr_context_get_head_pose (gxr, &hmd_pose);
  if (!has_pose)
    {
      g3k_object_reset_local_scale (G3K_OBJECT (self));
      return;
    }

  graphene_point3d_t hmd_point;
  graphene_ext_matrix_get_translation_point3d (&hmd_pose, &hmd_point);

  float distance = graphene_point3d_distance (cursor_point, &hmd_point, NULL);

  /* divide distance by 3 so the width and the apparent width are the same at
   * a distance of 3 meters. This makes e.g. self->width = 0.3 look decent in
   * both cases at typical usage distances. */
  float w = distance / G3K_CURSOR_APPARENT_SIZE_DISTANCE;

  graphene_point3d_t apparent_scale = {.x = w, .y = w, .z = 1.0f};
  g3k_object_set_local_scale (G3K_OBJECT (self), &apparent_scale);
}

void
g3k_cursor_update (G3kCursor          *self,
                   G3kObject          *target,
                   graphene_point3d_t *intersection)
{
  if (!g3k_plane_has_texture (G3K_PLANE (self)))
    return;

  // use the window pose as basis and retain the window rotation so that the
  // cursor is rotated the same as the window.
  // Then, set the 3d intersection point as the cursor position, to move it to
  // the intersection location.
  G3kPose new_pose = g3k_object_get_pose (target);
  graphene_point3d_init_from_point (&new_pose.position, intersection);
  g3k_object_set_local_pose (G3K_OBJECT (self), &new_pose);

  _update_apparent_size (G3K_CURSOR (self), intersection);
}
