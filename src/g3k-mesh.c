/*
 * xrdesktop
 * Copyright 2018-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-mesh.h"

#include <shaderc/shaderc.h>

#include "g3k-renderer.h"

G_DEFINE_TYPE (G3kMesh, g3k_mesh, G_TYPE_OBJECT)

G3kMesh *
g3k_mesh_new ()
{
  G3kMesh *self = (G3kMesh *) g_object_new (G3K_TYPE_MESH, 0);
  return self;
}

static void
g3k_mesh_init (G3kMesh *self)
{
  self->name = NULL;
  self->primitives = g_ptr_array_new_with_free_func ((GDestroyNotify)
                                                       g_object_unref);
}

static void
g3k_mesh_finalize (GObject *gobject)
{
  G3kMesh *self = G3K_MESH (gobject);
  g_free (self->name);
  g_ptr_array_unref (self->primitives);
  G_OBJECT_CLASS (g3k_mesh_parent_class)->finalize (gobject);
}

static void
g3k_mesh_class_init (G3kMeshClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_mesh_finalize;
}

G_DEFINE_TYPE (G3kPrimitive, g3k_primitive, G_TYPE_OBJECT)

G3kPrimitive *
g3k_primitive_new (G3kContext *context)
{
  G3kPrimitive *self = (G3kPrimitive *) g_object_new (G3K_TYPE_PRIMITIVE, 0);
  self->context = g_object_ref (context);
  return self;
}

static void
g3k_primitive_init (G3kPrimitive *self)
{
  self->context = NULL;
  self->index_accessor = NULL;
  self->material = NULL;
  self->index_type = VK_INDEX_TYPE_UINT16;
  self->accessors = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                           NULL);
}

static void
g3k_primitive_finalize (GObject *gobject)
{
  G3kPrimitive *self = G3K_PRIMITIVE (gobject);
  g_hash_table_destroy (self->accessors);
  g_clear_object (&self->material);
  g_clear_object (&self->vertex_buffer);
  g_clear_object (&self->context);
  G_OBJECT_CLASS (g3k_mesh_parent_class)->finalize (gobject);
}

static void
g3k_primitive_class_init (G3kPrimitiveClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_primitive_finalize;
}

bool
g3k_primitive_add_index (G3kPrimitive *self, G3kAccessor *accessor)
{
  G3kAttributeArray *array = accessor->array;

  self->index_count = accessor->count;
  switch (array->type)
    {
      case G3K_ATTRIBUTE_TYPE_UINT16:
        self->index_type = VK_INDEX_TYPE_UINT16;
        break;
      case G3K_ATTRIBUTE_TYPE_UINT32:
        self->index_type = VK_INDEX_TYPE_UINT32;
        break;
      case G3K_ATTRIBUTE_TYPE_UINT8:
        self->index_type = VK_INDEX_TYPE_UINT8_EXT;
        break;
      case G3K_ATTRIBUTE_TYPE_DOUBLE:
      case G3K_ATTRIBUTE_TYPE_FLOAT:
      case G3K_ATTRIBUTE_TYPE_INT32:
      case G3K_ATTRIBUTE_TYPE_INT16:
      case G3K_ATTRIBUTE_TYPE_INT8:
      default:
        self->index_type = VK_INDEX_TYPE_UINT16;
        g_warning ("Unsupported index type %s.",
                   g3k_attribute_type_string (array->type));
        return FALSE;
    }

  if (self->index_type == VK_INDEX_TYPE_UINT8_EXT)
    {
      g_warning ("VK_EXT_index_type_uint8 is currently unsupported.");
      return FALSE;
    }

  self->index_accessor = accessor;
  return TRUE;
}

typedef struct
{
  const gchar *name;
  gsize        stride;
} G3kSupportedAttrib;

static const G3kSupportedAttrib supported_attribs[] = {
  {"POSITION", 3}, {"COLOR_0", 3},    {"NORMAL", 3},
  {"TANGENT", 4},  {"TEXCOORD_0", 2},
};

bool
g3k_primitive_add_attribute (G3kPrimitive *self,
                             const char   *name,
                             G3kAccessor  *accessor)
{
  G3kAttributeArray *array = accessor->array;
  if (array->type != G3K_ATTRIBUTE_TYPE_FLOAT)
    {
      g_warning ("Unsupported attribute type %s.",
                 g3k_attribute_type_string (array->type));
    }

  for (guint i = 0; i < G_N_ELEMENTS (supported_attribs); i++)
    {
      if (strcmp (name, supported_attribs[i].name) == 0)
        {
          g_hash_table_insert (self->accessors, g_strdup (name), accessor);
          return TRUE;
        }
    }

  g_warning ("Skipping unsupported attrib '%s'.", name);

  return TRUE;
}

static bool
_has_attribute (G3kPrimitive *self, const char *name)
{
  return g_hash_table_contains (self->accessors, name);
}

static gsize
_get_attribute_size (G3kPrimitive *self, const char *name)
{
  G3kAccessor       *accessor = g_hash_table_lookup (self->accessors, name);
  G3kAttributeArray *array = accessor->array;
  return g3k_attribute_type_size (array->type) * accessor->count
         * array->stride;
}

static gsize
_get_attribute_stride (G3kPrimitive *self, const char *name)
{
  G3kAccessor *accessor = g_hash_table_lookup (self->accessors, name);
  return accessor->array->stride;
}

static gsize
_get_attribute_count (G3kPrimitive *self, const char *name)
{
  G3kAccessor *accessor = g_hash_table_lookup (self->accessors, name);
  return accessor->count;
}

static GBytes *
_get_attribute_bytes (G3kPrimitive *self, const char *name)
{
  G3kAccessor *accessor = g_hash_table_lookup (self->accessors, name);
  return accessor->array->bytes;
}

static gsize
_get_attribute_offset (G3kPrimitive *self, const char *name)
{
  G3kAccessor       *accessor = g_hash_table_lookup (self->accessors, name);
  G3kAttributeArray *array = accessor->array;
  return accessor->item_offset * g3k_attribute_type_size (array->type);
}

static void
_init_attribute (G3kPrimitive *self,
                 const char   *name,
                 gsize         expected_stride,
                 gsize         vert_count,
                 gsize         element_size)
{
  if (!_has_attribute (self, name))
    {
      return;
    }

  gpointer data = NULL;
  gsize    expected_size = element_size * vert_count * expected_stride;
  gsize    attrib_size = expected_size;
  gsize    offset = 0;
  gsize    stride = expected_stride;
  stride = _get_attribute_stride (self, name);
  if (stride != expected_stride)
    {
      g_warning ("Unexpected stride for %s: Expected %ld Got %ld\n", name,
                 expected_stride, stride);
    }

  attrib_size = _get_attribute_size (self, name);
  if (expected_size != attrib_size)
    {
      g_warning ("%s size mismatch! %ld != %ld\n", name, expected_size,
                 attrib_size);
    }
  offset = _get_attribute_offset (self, name);
  GBytes *bytes = _get_attribute_bytes (self, name);
  data = (gpointer) g_bytes_get_data (bytes, NULL);

  gulkan_vertex_buffer_add_attribute (self->vertex_buffer, stride, attrib_size,
                                      offset, (const uint8_t *) data);
}

static void
_init_attributes (G3kPrimitive *self)
{
  g_assert (_has_attribute (self, "POSITION"));
  gsize position_count = _get_attribute_count (self, "POSITION");
  for (guint i = 0; i < G_N_ELEMENTS (supported_attribs); i++)
    {
      _init_attribute (self, supported_attribs[i].name,
                       supported_attribs[i].stride, position_count,
                       sizeof (float));
    }
}

gboolean
g3k_primitive_init_vertex_buffer (G3kPrimitive *self, G3kContext *g3k)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);

  self->vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
  _init_attributes (self);

  if (self->index_accessor)
    {
      gsize         size;
      gconstpointer array_data = g_bytes_get_data (self->index_accessor->array
                                                     ->bytes,
                                                   &size);

      gsize offset
        = self->index_accessor->item_offset
          * gulkan_vertex_buffer_get_index_type_size (self->index_type);

      void *data = (void *) ((uint8_t *) array_data + offset);

      if (!gulkan_vertex_buffer_alloc_index_data (self->vertex_buffer, data,
                                                  self->index_type,
                                                  (guint) self->index_count))
        return FALSE;
    }
  else
    {
      g_warning ("No indices available, skipping index buffer allocation.");
    }

  if (!gulkan_vertex_buffer_upload (self->vertex_buffer))
    return FALSE;

  return TRUE;
}

char *
g3k_primitive_get_attribute_config_key (G3kPrimitive *self)
{
  GString                 *s = g_string_new ("");
  guint                    length;
  g_autofree const gchar **attrib_names = (const gchar **)
    g_hash_table_get_keys_as_array (self->accessors, &length);

  for (guint i = 0; i < length; i++)
    {
      g_string_append (s, attrib_names[i]);
    }

  return g_string_free (s, FALSE);
}

VkVertexInputAttributeDescription *
g3k_primitive_create_attrib_desc (G3kPrimitive *self)
{
  VkVertexInputAttributeDescription *attrib_desc
    = gulkan_vertex_buffer_create_attrib_desc (self->vertex_buffer);

  // Fix up attrib locations
  uint32_t attrib_count
    = gulkan_vertex_buffer_get_attrib_count (self->vertex_buffer);
  uint32_t j = 0;
  for (guint i = 0; i < G_N_ELEMENTS (supported_attribs); i++)
    {
      if (_has_attribute (self, supported_attribs[i].name))
        {
          g_debug ("Setting binding %d (%s) to location %d\n", j,
                   supported_attribs[i].name, i);
          g_assert (j <= attrib_count);
          attrib_desc[j].location = i;
          j++;
        }
    }

  return attrib_desc;
}

char *
g3k_primitive_get_attrib_defines (G3kPrimitive *self)
{
  GString                 *s = g_string_new ("");
  guint                    length;
  g_autofree const gchar **attrib_names = (const gchar **)
    g_hash_table_get_keys_as_array (self->accessors, &length);

  for (guint i = 0; i < length; i++)
    {
      g_string_append_printf (s, "#define HAVE_%s\n", attrib_names[i]);
    }

  return g_string_free (s, FALSE);
}

static gboolean
_load_resource (const gchar *path, GBytes **res)
{
  GError *error = NULL;
  *res = g_resources_lookup_data (path, G_RESOURCE_LOOKUP_FLAGS_NONE, &error);

  if (error != NULL)
    {
      g_printerr ("Unable to read file: %s\n", error->message);
      g_error_free (error);
      return FALSE;
    }

  return TRUE;
}

static gboolean
_build_shader (G3kPrimitive           *self,
               const char             *uri,
               VkPipelineStageFlagBits stage,
               VkShaderModule         *module)
{
  GBytes *template_bytes;
  if (!_load_resource (uri, &template_bytes))
    return FALSE;

  GString *source_str = g_string_new ("#version 460\n");

  char *attrib_defines = g3k_primitive_get_attrib_defines (self);
  g_string_append (source_str, attrib_defines);
  g_free (attrib_defines);

  if (stage == VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
    {
      char *material_defines = g3k_material_get_defines (self->material);
      g_string_append (source_str, material_defines);
      g_free (material_defines);
    }

  gsize        template_size = 0;
  const gchar *template_string = g_bytes_get_data (template_bytes,
                                                   &template_size);

  g_string_append (source_str, template_string);

  // g_print ("%s", source_str->str);

  shaderc_compiler_t           compiler = shaderc_compiler_initialize ();
  shaderc_compilation_result_t result;

  if (stage == VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
    {
      result = shaderc_compile_into_spv (compiler, source_str->str,
                                         source_str->len,
                                         shaderc_glsl_fragment_shader,
                                         "main.frag", "main", NULL);
    }
  else if (stage == VK_PIPELINE_STAGE_VERTEX_SHADER_BIT)
    {
      result = shaderc_compile_into_spv (compiler, source_str->str,
                                         source_str->len,
                                         shaderc_glsl_vertex_shader,
                                         "main.vert", "main", NULL);
    }
  else
    {
      g_printerr ("Unsupported shader stage.\n");
      return FALSE;
    }

  shaderc_compilation_status status
    = shaderc_result_get_compilation_status (result);

  if (status != shaderc_compilation_status_success)
    {
      g_print ("Result code %d\n", (int) status);
      g_print ("shaderc error:\n%s\n",
               shaderc_result_get_error_message (result));
      shaderc_result_release (result);
      shaderc_compiler_release (compiler);
      return FALSE;
    }

  const char *bytes = shaderc_result_get_bytes (result);
  size_t      len = shaderc_result_get_length (result);

  /* Make clang happy and copy the data to fix alignment */
  uint32_t *code = g_malloc (len);
  memcpy (code, bytes, len);

  VkShaderModuleCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
    .codeSize = len,
    .pCode = code,
  };

  GulkanContext *gulkan = g3k_context_get_gulkan (self->context);
  VkDevice       device = gulkan_context_get_device_handle (gulkan);

  VkResult res = vkCreateShaderModule (device, &info, NULL, module);
  vk_check_error ("vkCreateShaderModule", res, FALSE);

  g_free (code);

  shaderc_result_release (result);
  shaderc_compiler_release (compiler);

  g_string_free (source_str, TRUE);

  return TRUE;
}

G3kPipeline *
g3k_primitive_create_pipeline (G3kPrimitive *self)
{
  uint32_t bindings_size = 0;

  GulkanVertexBuffer *vb = self->vertex_buffer;

  VkDescriptorSetLayoutBinding *bindings
    = g3k_material_create_descriptor_bindings (self->material, &bindings_size);

  G3kRenderer          *renderer = g3k_context_get_renderer (self->context);
  VkSampleCountFlagBits sample_count = g3k_renderer_get_sample_count (renderer);

  VkExtent2D extent = gulkan_renderer_get_extent (GULKAN_RENDERER (renderer));

  GulkanRenderPass *render_pass = g3k_renderer_get_render_pass (renderer);
  VkVertexInputBindingDescription *binding_desc
    = gulkan_vertex_buffer_create_binding_desc (vb);
  VkVertexInputAttributeDescription *attrib_desc
    = g3k_primitive_create_attrib_desc (self);

  uint32_t attrib_count = gulkan_vertex_buffer_get_attrib_count (vb);

  VkShaderModule vs;
  if (!_build_shader (self, "/shaders/model.vert.template",
                      VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, &vs))
    {
      g_printerr ("Shader compilation failed.\n");
      return NULL;
    }

  VkShaderModule fs;
  if (!_build_shader (self, "/shaders/model.frag.template",
                      VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, &fs))
    {
      g_printerr ("Shader compilation failed.\n");
      return NULL;
    }

  GulkanPipelineConfig config_model = {
    .extent = extent,
    .sample_count = sample_count,
    .vertex_shader = vs,
    .fragment_shader = fs,
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    .attribs = attrib_desc,
    .attrib_count = attrib_count,
    .bindings = binding_desc,
    .binding_count = gulkan_vertex_buffer_get_attrib_count (vb),
    .depth_stencil_state = &(VkPipelineDepthStencilStateCreateInfo) {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
      .depthTestEnable = VK_TRUE,
      .depthWriteEnable = VK_TRUE,
      .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
    },
    .blend_attachments = &(VkPipelineColorBlendAttachmentState) {
      .blendEnable = VK_FALSE,
      .colorWriteMask = 0xf,
    },
    .rasterization_state = &(VkPipelineRasterizationStateCreateInfo) {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_BACK_BIT,
      .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
      .lineWidth = 1.0f,
    },
    //.flip_y = TRUE,
  };
  G3kPipeline *pipeline = g3k_pipeline_new (self->context, bindings,
                                            bindings_size, 512, &config_model,
                                            render_pass);

  g_free (binding_desc);
  g_free (attrib_desc);
  g_free (bindings);

  return pipeline;
}

char *
g3k_primitive_get_pipeline_key (G3kPrimitive *self)
{
  char *attrib_key = g3k_primitive_get_attribute_config_key (self);
  char *material_key = g3k_material_get_config_key (self->material);

  GString *s = g_string_new ("");
  g_string_append (s, attrib_key);
  g_string_append (s, material_key);

  g_free (attrib_key);
  g_free (material_key);

  return g_string_free (s, FALSE);
}

void
g3k_primitive_set_pipeline (G3kPrimitive *self, G3kPipeline *pipeline)
{
  self->pipeline = g_object_ref (pipeline);
}

void
g3k_primitive_draw (G3kPrimitive        *self,
                    GulkanDescriptorSet *descriptor_set,
                    VkCommandBuffer      cmd_buffer)
{
  g3k_pipeline_bind (self->pipeline, cmd_buffer);

  VkPipelineLayout layout = g3k_pipeline_get_layout (self->pipeline);
  gulkan_descriptor_set_bind (descriptor_set, layout, cmd_buffer);

  GulkanVertexBuffer *vb = self->vertex_buffer;
  g_assert (gulkan_vertex_buffer_is_initialized (vb));

  gulkan_vertex_buffer_bind_with_offsets (vb, cmd_buffer);
  GulkanBuffer *index_buffer = gulkan_vertex_buffer_get_index_buffer (vb);
  if (index_buffer)
    {
      gulkan_vertex_buffer_draw_indexed (vb, cmd_buffer);
    }
  else
    {
      gulkan_vertex_buffer_draw (vb, cmd_buffer);
    }
}
