/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-background.h"

#include <stdalign.h>

#include "g3k-renderer.h"

typedef struct
{
  alignas (32) float mvp[2][16];
} G3kBackgroundUniformBuffer;

struct _G3kBackground
{
  G3kObject           parent;
  GulkanVertexBuffer *vertex_buffer;
};

G_DEFINE_TYPE (G3kBackground, g3k_background, G3K_TYPE_OBJECT)

static void
g3k_background_finalize (GObject *gobject);

static void
_draw (G3kObject *self, VkCommandBuffer cmd_buffer);

static void
g3k_background_class_init (G3kBackgroundClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_background_finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
}

static void
g3k_background_init (G3kBackground *self)
{
  (void) self;
}

static gboolean
_initialize (G3kBackground *self, G3kContext *g3k);

G3kBackground *
g3k_background_new (G3kContext *g3k)
{
  G3kBackground *self = (G3kBackground *) g_object_new (G3K_TYPE_BACKGROUND, 0);
  _initialize (self, g3k);
  return self;
}

static void
g3k_background_finalize (GObject *gobject)
{
  G3kBackground *self = G3K_BACKGROUND (gobject);
  g_clear_object (&self->vertex_buffer);
  G_OBJECT_CLASS (g3k_background_parent_class)->finalize (gobject);
}

static void
_append_star (GulkanVertexBuffer *self,
              float               radius,
              float               y,
              uint32_t            sections,
              graphene_vec3_t    *color)
{
  graphene_vec4_t *points = g_malloc (sizeof (graphene_vec4_t) * sections);

  graphene_vec4_init (&points[0], radius, y, 0, 1);
  graphene_vec4_init (&points[1], -radius, y, 0, 1);

  graphene_matrix_t rotation;
  graphene_matrix_init_identity (&rotation);
  graphene_matrix_rotate_y (&rotation, 360.0f / (float) sections);

  for (uint32_t i = 0; i < sections / 2 - 1; i++)
    {
      uint32_t j = i * 2;
      graphene_matrix_transform_vec4 (&rotation, &points[j], &points[j + 2]);
      graphene_matrix_transform_vec4 (&rotation, &points[j + 1],
                                      &points[j + 3]);
    }

  for (uint32_t i = 0; i < sections; i++)
    gulkan_vertex_buffer_append_with_color (self, &points[i], color);

  g_free (points);
}

static void
_append_circle (GulkanVertexBuffer *self,
                float               radius,
                float               y,
                uint32_t            edges,
                graphene_vec3_t    *color)
{
  graphene_vec4_t *points = g_malloc (sizeof (graphene_vec4_t) * edges * 2);

  graphene_vec4_init (&points[0], radius, y, 0, 1);

  graphene_matrix_t rotation;
  graphene_matrix_init_identity (&rotation);
  graphene_matrix_rotate_y (&rotation, 360.0f / (float) edges);

  for (uint32_t i = 0; i < edges; i++)
    {
      uint32_t j = i * 2;
      if (i != 0)
        graphene_vec4_init_from_vec4 (&points[j], &points[j - 1]);
      graphene_matrix_transform_vec4 (&rotation, &points[j], &points[j + 1]);
    }

  for (uint32_t i = 0; i < edges * 2; i++)
    gulkan_vertex_buffer_append_with_color (self, &points[i], color);

  g_free (points);
}

static void
_append_floor (GulkanVertexBuffer *self,
               uint32_t            radius,
               float               y,
               graphene_vec3_t    *color)
{
  _append_star (self, (float) radius, y, 8, color);

  for (uint32_t i = 1; i <= radius; i++)
    _append_circle (self, (float) i, y, 128, color);
}

static gboolean
_initialize (G3kBackground *self, G3kContext *g3k)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  self->vertex_buffer
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
  gulkan_vertex_buffer_reset (self->vertex_buffer);

  graphene_vec3_t color;
  graphene_vec3_init (&color, .6f, .6f, .6f);

  _append_floor (self->vertex_buffer, 20, 0.0f, &color);
  _append_floor (self->vertex_buffer, 20, 4.0f, &color);
  if (!gulkan_vertex_buffer_alloc_empty (self->vertex_buffer,
                                         GXR_DEVICE_INDEX_MAX))
    return FALSE;

  gulkan_vertex_buffer_map_array (self->vertex_buffer);

  G3kObject *obj = G3K_OBJECT (self);

  VkDeviceSize ub_size = sizeof (G3kBackgroundUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "background");

  if (!g3k_object_initialize (obj, g3k, pipeline, ub_size))
    return FALSE;

  return TRUE;
}

static void
_update_ubo (G3kBackground *self)
{
  G3kBackgroundUniformBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *vp = g3k_context_get_vps (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&m_matrix, &vp[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kBackground *self = G3K_BACKGROUND (obj);

  if (!gulkan_vertex_buffer_is_initialized (self->vertex_buffer))
    return;

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);
  gulkan_vertex_buffer_draw (self->vertex_buffer, cmd_buffer);
}
