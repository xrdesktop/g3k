/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_TIP_H_
#define G3K_TIP_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-plane.h"

G_BEGIN_DECLS

#define G3K_TYPE_TIP g3k_tip_get_type ()
G_DECLARE_FINAL_TYPE (G3kTip, g3k_tip, G3K, TIP, G3kPlane)

G3kTip *
g3k_tip_new (G3kContext *g3k);

void
g3k_tip_update_apparent_size (G3kTip *self);

void
g3k_tip_set_active (G3kTip *self, gboolean active);

void
g3k_tip_animate_pulse (G3kTip *self);

G_END_DECLS

#endif /* G3K_TIP_H_ */
