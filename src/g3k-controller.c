/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-controller.h"

#include <graphene-ext.h>

#include "g3k-object-priv.h"

struct _G3kController
{
  G3kObject parent;

  G3kRay       *ray;
  G3kTip       *tip;
  G3kHoverState hover_state;
  G3kGrabState  grab_state;

  GxrAction *haptic_action;

  GxrController *controller;

  gulong controller_move_signal;

  G3kSelection *selection;
};

G_DEFINE_TYPE (G3kController, g3k_controller, G3K_TYPE_OBJECT)

static void
_finalize (GObject *gobject);

static void
g3k_controller_class_init (G3kControllerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;
}

static void
g3k_controller_init (G3kController *self)
{
  self->hover_state.distance = 1.0f;
  self->grab_state.transform_lock = XRD_TRANSFORM_LOCK_NONE;
  self->grab_state.object = NULL;
  self->hover_state.object = NULL;
  self->ray = NULL;
  self->tip = NULL;
  self->selection = NULL;
  self->controller = NULL;
  self->controller_move_signal = 0;
  self->haptic_action = NULL;
}

static void
_controller_move_cb (GxrController *controller,
                     void          *event,
                     G3kController *self);

G3kController *
g3k_controller_new (G3kContext *g3k, GxrController *controller)
{
  G3kController *self = (G3kController *) g_object_new (G3K_TYPE_CONTROLLER, 0);

  self->controller = g_object_ref (controller);

  g3k_object_set_context (G3K_OBJECT (self), g3k);

  self->controller_move_signal = g_signal_connect (self->controller, "move",
                                                   (GCallback)
                                                     _controller_move_cb,
                                                   self);

  return self;
}

static void
_finalize (GObject *gobject)
{
  G3kController *self = G3K_CONTROLLER (gobject);

  g_clear_object (&self->ray);
  g_clear_object (&self->tip);
  g_clear_object (&self->selection);

  g_signal_handler_disconnect (self->controller, self->controller_move_signal);
  g_object_unref (self->controller);

  g_debug ("Destroyed pointer ray, pointer tip, controller");

  G_OBJECT_CLASS (g3k_controller_parent_class)->finalize (gobject);
}

G3kRay *
g3k_controller_get_ray (G3kController *self)
{
  return self->ray;
}

G3kTip *
g3k_controller_get_tip (G3kController *self)
{
  return self->tip;
}

G3kHoverState *
g3k_controller_get_hover_state (G3kController *self)
{
  return &self->hover_state;
}

G3kGrabState *
g3k_controller_get_grab_state (G3kController *self)
{
  return &self->grab_state;
}

static void
_reset_grab_state (G3kController *self)
{
  self->grab_state.object = NULL;
  graphene_point_init (&self->grab_state.offset, 0, 0);
  graphene_quaternion_init_identity (&self->grab_state
                                        .inverse_controller_rotation);
  self->grab_state.transform_lock = XRD_TRANSFORM_LOCK_NONE;
}

void
g3k_controller_reset_hover_state (G3kController *self)
{
  self->hover_state.object = NULL;
  graphene_point_init (&self->hover_state.intersection_2d, 0, 0);
  self->hover_state.distance = 1.0;
}

static void
_controller_move_cb (GxrController *controller,
                     void          *event,
                     G3kController *self)
{
  (void) event;
  (void) self;

  graphene_matrix_t pose;
  gxr_controller_get_pointer_pose (controller, &pose);
  g3k_object_set_matrix (G3K_OBJECT (self->ray), &pose);
}

void
g3k_controller_hide_pointers (G3kController *self)
{
  g3k_object_set_visibility (G3K_OBJECT (self), FALSE);

  g3k_object_set_visibility (G3K_OBJECT (self->ray), FALSE);
  g3k_object_set_visibility (G3K_OBJECT (self->tip), FALSE);
  g3k_object_set_visibility (G3K_OBJECT (self->selection), FALSE);
}

void
g3k_controller_show_pointers (G3kController *self)
{
  g3k_object_set_visibility (G3K_OBJECT (self), TRUE);

  g3k_ray_show (self->ray);
  g3k_object_set_visibility (G3K_OBJECT (self->tip), TRUE);
  g3k_object_set_visibility (G3K_OBJECT (self->selection), TRUE);
}

void
g3k_controller_update_hovered_object (G3kController      *self,
                                      G3kObject          *last_object,
                                      G3kObject          *object,
                                      graphene_point3d_t *global_intersection,
                                      graphene_point_t   *local_intersection,
                                      float               intersection_distance)
{
  if (last_object != object)
    {
      g3k_tip_set_active (self->tip, object != NULL);
      g3k_object_set_visibility (G3K_OBJECT (self->selection),
                                 object != NULL
                                   && g3k_object_is_visible (G3K_OBJECT (self)));
    }

  if (object)
    {
      self->hover_state.object = object;

      graphene_point_init_from_point (&self->hover_state.intersection_2d,
                                      local_intersection);

      self->hover_state.distance = intersection_distance;

      G3kPose new_pose = g3k_object_get_pose (object);
      graphene_point3d_init_from_point (&new_pose.position,
                                        global_intersection);
      g3k_object_set_pose (G3K_OBJECT (self->tip), &new_pose);

      g3k_ray_set_length (self->ray, intersection_distance);

      g3k_object_update_selection (object, self->selection);
    }
  else if (last_object)
    {
      float dist = g3k_ray_get_default_length (self->ray);

      G3kPose tip_pose = g3k_pose_new (NULL, NULL);
      tip_pose.position.z = -dist;
      g3k_object_set_local_pose (G3K_OBJECT (self->tip), &tip_pose);

      g3k_ray_reset_length (self->ray);
      g3k_controller_reset_hover_state (self);
      _reset_grab_state (self);
    }

  /* TODO: Update apparent size in g3k scene graph update_node */
  g3k_tip_update_apparent_size (self->tip);
}

void
g3k_controller_drag_start (G3kController *self, G3kObject *grabbed_object)
{
  self->grab_state.object = grabbed_object;

  graphene_matrix_t pointer_pose;
  gxr_controller_get_pointer_pose (GXR_CONTROLLER (self->controller),
                                   &pointer_pose);

  graphene_quaternion_t controller_rotation;

  graphene_point3d_t unused_scale;
  graphene_ext_matrix_get_rotation_quaternion (&pointer_pose, &unused_scale,
                                               &controller_rotation);

  graphene_matrix_t g_grabbed_object;
  g3k_object_get_matrix (grabbed_object, &g_grabbed_object);

  graphene_ext_matrix_get_rotation_quaternion (&g_grabbed_object, &unused_scale,
                                               &self->grab_state
                                                  .object_rotation);

  graphene_point_init (&self->grab_state.offset,
                       -self->hover_state.intersection_2d.x,
                       -self->hover_state.intersection_2d.y);

  graphene_quaternion_invert (&controller_rotation,
                              &self->grab_state.inverse_controller_rotation);
}

static graphene_matrix_t
_calculate_tip_transform (G3kController *self)
{
  g_assert (self->grab_state.object);

  graphene_matrix_t pointer_pose;
  gxr_controller_get_pointer_pose (GXR_CONTROLLER (self->controller),
                                   &pointer_pose);

  graphene_point3d_t controller_translation_point;
  graphene_ext_matrix_get_translation_point3d (&pointer_pose,
                                               &controller_translation_point);
  graphene_quaternion_t controller_rotation;
  graphene_quaternion_init_from_matrix (&controller_rotation, &pointer_pose);

  graphene_point3d_t distance_translation_point;
  graphene_point3d_init (&distance_translation_point, 0.f, 0.f,
                         -self->hover_state.distance);

  /* Build a new transform for pointer tip in event->pose.
   * Pointer tip is at intersection, in the plane of the window,
   * so we can reuse the tip rotation for the window rotation. */
  graphene_matrix_t tip_transform;
  graphene_matrix_init_identity (&tip_transform);

  /* restore original rotation of the tip */
  graphene_matrix_rotate_quaternion (&tip_transform,
                                     &self->grab_state.object_rotation);

  /* Later the current controller rotation is applied to the overlay, so to
   * keep the later controller rotations relative to the initial controller
   * rotation, rotate the window in the opposite direction of the initial
   * controller rotation.
   * This will initially result in the same window rotation so the window does
   * not change its rotation when being grabbed, and changing the controllers
   * position later will rotate the window with the "diff" of the controller
   * rotation to the initial controller rotation. */
  graphene_matrix_rotate_quaternion (&tip_transform,
                                     &self->grab_state
                                        .inverse_controller_rotation);

  /* then translate the overlay to the controller ray distance */
  graphene_matrix_translate (&tip_transform, &distance_translation_point);

  /* Rotate the translated overlay to where the controller is pointing. */
  graphene_matrix_rotate_quaternion (&tip_transform, &controller_rotation);

  /* Calculation was done for controller in (0,0,0), just move it with
   * controller's offset to real (0,0,0) */
  graphene_matrix_translate (&tip_transform, &controller_translation_point);

  return tip_transform;
}

void
g3k_controller_update_grab (G3kController *self)
{
  graphene_matrix_t tip_transform = _calculate_tip_transform (self);

  graphene_point3d_t grab_offset3d;
  graphene_point3d_init (&grab_offset3d, self->grab_state.offset.x,
                         self->grab_state.offset.y, 0);

  /* translate such that the grab point is pivot point. */
  graphene_matrix_t object_transform;
  graphene_matrix_init_translate (&object_transform, &grab_offset3d);

  /* window has the same rotation as the tip we calculated in event->pose */
  graphene_matrix_multiply (&object_transform, &tip_transform,
                            &object_transform);

  g3k_object_set_matrix (G3K_OBJECT (self->tip), &tip_transform);
  /* update apparent size after pointer has been moved */
  g3k_tip_update_apparent_size (self->tip);

  G3kPose pose = g3k_pose_new_from_matrix (&object_transform);
  g3k_object_set_pose (self->grab_state.object, &pose);

  // TODO: scene graph should move selection with window
  g3k_object_update_selection (self->grab_state.object,
                               g3k_controller_get_selection (self));

  G3kGrabEvent event = {
    .controller = self,
  };
  g3k_object_emit_grab (self->grab_state.object, &event);
}

G3kSelection *
g3k_controller_get_selection (G3kController *self)
{
  return self->selection;
}

GxrController *
g3k_controller_get_controller (G3kController *self)
{
  return self->controller;
}

gboolean
g3k_controller_get_intersection (G3kController      *self,
                                 G3kObject          *object,
                                 float              *distance,
                                 graphene_point3d_t *intersection_point)
{
  G3kRay *ray = g3k_controller_get_ray (self);
  return g3k_ray_get_intersection (ray, object, distance, intersection_point);
}

gboolean
g3k_controller_init_children (G3kController *self)
{
  G3kContext *g3k = g3k_object_get_context (G3K_OBJECT (self));

  self->ray = g3k_ray_new (g3k);
  g3k_object_add_child (G3K_OBJECT (self), G3K_OBJECT (self->ray), 0);

  self->selection = g3k_selection_new (g3k);
  g3k_object_add_child (G3K_OBJECT (self), G3K_OBJECT (self->selection), 0);

  self->tip = g3k_tip_new (g3k);
  g3k_object_add_child (G3K_OBJECT (self->ray), G3K_OBJECT (self->tip), 1);

  /* set intitial tip pose */
  float   dist = g3k_ray_get_default_length (self->ray);
  G3kPose tip_pose = g3k_pose_new (NULL, NULL);
  tip_pose.position.z = -dist;
  g3k_object_set_local_pose (G3K_OBJECT (self->tip), &tip_pose);

  return TRUE;
}

#define ENUM_TO_STR(r)                                                         \
  case r:                                                                      \
    return #r

const gchar *
g3k_transform_lock_string (G3kTransformLock lock)
{
  switch (lock)
    {
      ENUM_TO_STR (XRD_TRANSFORM_LOCK_NONE);
      ENUM_TO_STR (XRD_TRANSFORM_LOCK_PUSH_PULL);
      ENUM_TO_STR (XRD_TRANSFORM_LOCK_SCALE);
      default:
        return "UNKNOWN TYPE";
    }
}

void
g3k_controller_check_grab (G3kController *self)
{
  if (self->hover_state.object == NULL)
    return;

  g3k_object_emit_grab_start (self->hover_state.object, self);
}

void
g3k_controller_check_release (G3kController *self)
{
  if (self->grab_state.object == NULL)
    return;

  g3k_object_emit_release (self->grab_state.object, self);
  _reset_grab_state (self);
}

void
g3k_controller_trigger_haptic (G3kController *self,
                               float          duration_seconds,
                               float          frequency,
                               float          amplitude)
{
  if (!self->haptic_action)
    {
      return;
    }

  guint64 handle = gxr_device_get_handle (GXR_DEVICE (self->controller));
  gxr_action_trigger_haptic (self->haptic_action, 0, duration_seconds,
                             frequency, amplitude, handle);
}

void
g3k_controller_set_haptic_action (G3kController *self, GxrAction *action)
{
  self->haptic_action = action;
}
