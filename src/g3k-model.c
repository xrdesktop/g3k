/*
 * xrdesktop
 * Copyright 2018-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-model.h"

#include <stdalign.h>

#include "g3k-object-priv.h"

typedef struct
{
  alignas (32) float projection[2][16];
  alignas (32) float view[2][16];
  alignas (16) float model[16];
  alignas (8) float eye_position[2][4];
} G3kModelUniformBuffer;

struct _G3kModel
{
  G3kObject   parent;
  G3kMesh    *mesh;
  char       *name;
  GHashTable *descriptor_sets;
};

G_DEFINE_TYPE (G3kModel, g3k_model, G3K_TYPE_OBJECT)

static void
g3k_model_finalize (GObject *gobject);

static void
_draw (G3kObject *self, VkCommandBuffer cmd_buffer);

static void
g3k_model_class_init (G3kModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = g3k_model_finalize;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
}

static void
g3k_model_init (G3kModel *self)
{
  self->mesh = NULL;
  self->descriptor_sets = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                                 NULL, g_object_unref);
}

G3kModel *
g3k_model_new (G3kContext *g3k)
{
  G3kModel *self = (G3kModel *) g_object_new (G3K_TYPE_MODEL, 0);
  g3k_object_set_context (G3K_OBJECT (self), g3k);
  return self;
}

void
g3k_model_set_mesh (G3kModel *self, G3kMesh *mesh)
{
  self->mesh = g_object_ref (mesh);
}

void
g3k_model_set_name (G3kModel *self, char *name)
{
  g_free (self->name);
  self->name = g_strdup (name);
}

char *
g3k_model_get_name (G3kModel *self)
{
  return self->name;
}

static void
g3k_model_finalize (GObject *gobject)
{
  G3kModel *self = G3K_MODEL (gobject);
  g_clear_object (&self->mesh);
  g_hash_table_destroy (self->descriptor_sets);
  G_OBJECT_CLASS (g3k_model_parent_class)->finalize (gobject);
}

static void
_update_ubo (G3kModel *self)
{
  G3kModelUniformBuffer ub = {0};

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *views = g3k_context_get_views (context);
  graphene_matrix_t *projections = g3k_context_get_projections (context);

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);
  graphene_matrix_to_float (&m_matrix, ub.model);

  GxrContext *gxr = g3k_context_get_gxr (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_vec3_t eye_position;
      gxr_context_get_eye_position (gxr, eye, &eye_position);
      graphene_vec3_to_float (&eye_position, ub.eye_position[eye]);
      graphene_matrix_to_float (&projections[eye], ub.projection[eye]);
      graphene_matrix_to_float (&views[eye], ub.view[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  G3kModel *self = G3K_MODEL (obj);

  _update_ubo (self);

  for (guint i = 0; i < self->mesh->primitives->len; i++)
    {
      G3kPrimitive *primitive
        = G3K_PRIMITIVE (g_ptr_array_index (self->mesh->primitives, i));

      GulkanDescriptorSet *descriptor_set
        = g_hash_table_lookup (self->descriptor_sets, primitive);
      g3k_primitive_draw (primitive, descriptor_set, cmd_buffer);
    }
}

bool
g3k_model_initialize (G3kModel *self, GHashTable *pipelines)
{
  G3kObject   *obj = G3K_OBJECT (self);
  G3kContext  *g3k = g3k_object_get_context (G3K_OBJECT (self));
  VkDeviceSize ub_size = sizeof (G3kModelUniformBuffer);

  if (!g3k_object_initialize_no_pipeline (obj, g3k, ub_size))
    return FALSE;

  GulkanUniformBuffer *transformation_ubo
    = g3k_object_get_transformation_ubo (obj);

  for (guint i = 0; i < self->mesh->primitives->len; i++)
    {
      G3kPrimitive *primitive
        = G3K_PRIMITIVE (g_ptr_array_index (self->mesh->primitives, i));
      char        *pipeline_key = g3k_primitive_get_pipeline_key (primitive);
      G3kPipeline *pipeline = NULL;
      if (!g_hash_table_contains (pipelines, pipeline_key))
        {
          pipeline = g3k_primitive_create_pipeline (primitive);
          g_hash_table_insert (pipelines, pipeline_key, pipeline);
          g_debug ("Creating new pipeline for id: %s\n", pipeline_key);
        }
      else
        {
          pipeline = g_hash_table_lookup (pipelines, pipeline_key);
        }
      g_assert (pipeline);

      GulkanDescriptorSet *descriptor_set
        = g3k_pipeline_create_descriptor_set (pipeline);
      g_assert (descriptor_set);
      gulkan_descriptor_set_update_buffer (descriptor_set, 0,
                                           transformation_ubo);
      g_hash_table_insert (self->descriptor_sets, primitive, descriptor_set);

      g3k_material_update_descriptors (primitive->material, descriptor_set);
      // TODO: Do this less often
      g3k_material_update_ubo (primitive->material);

      g3k_primitive_set_pipeline (primitive, pipeline);
    }

  return TRUE;
}
