/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_BACKGROUND_H_
#define G3K_BACKGROUND_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

#include "g3k-context.h"
#include "g3k-object.h"

G_BEGIN_DECLS

#define G3K_TYPE_BACKGROUND g3k_background_get_type ()
G_DECLARE_FINAL_TYPE (G3kBackground, g3k_background, G3K, BACKGROUND, G3kObject)

G3kBackground *
g3k_background_new (G3kContext *g3k);

G_END_DECLS

#endif /* G3K_BACKGROUND_H_ */
