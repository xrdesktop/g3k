/*
 * g3k
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-context.h"

#include <canberra.h>

#include "g3k-controller.h"
#include "g3k-object-manager.h"
#include "g3k-renderer.h"
#include "g3k-settings.h"
#include "g3k-types.h"
#include "g3k-version.h"

#define APP_NAME "g3k"

enum
{
  RENDER_EVENT,
  CONTROLLER_PRIMARY_CHANGED_EVENT,
  CONTROLLER_ACTIVATE_EVENT,
  CONTROLLER_DEACTIVATE_EVENT,
  INPUT_POLL_EVENT,
  SHUTDOWN_EVENT,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0};

struct _G3kContext
{
  GObject parent;

  GxrContext       *gxr;
  G3kRenderer      *renderer;
  G3kObjectManager *manager;

  int           shutdown_wait_frame_thread;
  GThread      *wait_frame_thread;
  GMutex        wait_thread_mutex;
  GCond         wait_thread_cond;
  int           waited_frame_number;
  int           submitted_frame_number;

  float    near;
  float    far;
  gboolean have_projection;

  graphene_matrix_t views[2];
  graphene_matrix_t projections[2];
  graphene_matrix_t view_projections[2];

  VkImageLayout upload_layout;

  bool rendering;
  bool framecycle;

  // GxrController -> G3kController
  GHashTable *controllers;

  G3kController *primary_controller;

  G3kObject *root;

  gint64 last_poll_timestamp;
  guint  poll_input_rate_ms;

  ca_context *canberra_context;

  gboolean enable_sound;

  struct
  {
    guint poll_input;
    guint runtime_event;
    guint render_event;
  } source_ids;
};

G_DEFINE_TYPE (G3kContext, g3k_context, G_TYPE_OBJECT)

static gboolean
_render_cb (gpointer _self)
{
  if (_self == NULL || !G3K_IS_CONTEXT (_self))
    {
      g_printerr ("context: Can't render, context instance does not exist.\n");
      return FALSE;
    }

  G3kContext *self = G3K_CONTEXT (_self);
  g3k_context_render (self);
  return TRUE;
}

static void *
_wait_frame_thread (gpointer _self);

static void
g3k_context_init (G3kContext *self)
{
  self->gxr = NULL;
  self->renderer = NULL;
  self->near = 0.05f;
  self->far = 100.0f;
  self->have_projection = FALSE;
  self->upload_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  self->renderer = FALSE;
  self->framecycle = FALSE;
  self->controllers = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                             NULL, g_object_unref);
  self->primary_controller = NULL;
  self->root = g3k_object_new ();
  self->manager = g3k_object_manager_new (self);

  self->source_ids.runtime_event = 0;
  self->source_ids.poll_input = 0;
  self->last_poll_timestamp = g_get_monotonic_time ();

  self->enable_sound = TRUE;

  ca_context_create (&self->canberra_context);

  self->waited_frame_number = -1;
  self->submitted_frame_number = -1;

  g_mutex_init (&self->wait_thread_mutex);
  g_cond_init (&self->wait_thread_cond);

  self->wait_frame_thread = NULL;

  self->source_ids.render_event = g_timeout_add (1, _render_cb, self);
}

G3kContext *
g3k_context_new (void)
{
  GxrContext *gxr = gxr_context_new_from_vulkan_extensions (NULL, NULL,
                                                            APP_NAME,
                                                            G3K_VERSION_HEX);
  return g3k_context_new_from_gxr (gxr);
}

static void
_device_activate_cb (GxrDeviceManager *device_manager,
                     GxrDevice        *device,
                     gpointer          _self);

static void
_device_deactivate_cb (GxrDeviceManager *dm, GxrDevice *device, gpointer _self);

static gboolean
_poll_runtime_events (G3kContext *self)
{

  gxr_context_poll_events (g3k_context_get_gxr (self));
  return TRUE;
}

static gboolean
_poll_input_events (G3kContext *self)
{
  // TODO: do we want to process input while not rendering?
  if (!self->framecycle || !self->rendering)
    return TRUE;

  g_signal_emit (self, signals[INPUT_POLL_EVENT], 0, NULL);

  g3k_object_manager_step_containers (self->manager);

  self->last_poll_timestamp = g_get_monotonic_time ();
  return TRUE;
}

static void
_update_input_poll_rate (GSettings *settings, gchar *key, gpointer _self)
{
  G3kContext *self = G3K_CONTEXT (_self);

  if (self->source_ids.poll_input != 0)
    g_source_remove (self->source_ids.poll_input);
  self->poll_input_rate_ms = g_settings_get_uint (settings, key);

  self->source_ids.poll_input = g_timeout_add (self->poll_input_rate_ms,
                                               (GSourceFunc) _poll_input_events,
                                               self);
}

static void
_update_enable_sound (GSettings *settings, gchar *key, gpointer _self)
{
  G3kContext *self = G3K_CONTEXT (_self);
  self->enable_sound = g_settings_get_boolean (settings, key);
}

static void
_state_change_cb (GxrContext *gxr, GxrStateChangeEvent *event, gpointer _self)
{
  (void) gxr;

  G3kContext *self = G3K_CONTEXT (_self);

  switch (event->state_change)
    {
      case GXR_STATE_SHUTDOWN:
        self->framecycle = FALSE;
        self->rendering = FALSE;
        g_signal_emit (self, signals[SHUTDOWN_EVENT], 0, NULL);
        break;
      case GXR_STATE_FRAMECYCLE_START:
        self->framecycle = TRUE;
        break;
      case GXR_STATE_FRAMECYCLE_STOP:
        self->framecycle = FALSE;
        break;
      case GXR_STATE_RENDERING_START:
        self->rendering = TRUE;
        break;
      case GXR_STATE_RENDERING_STOP:
        self->rendering = FALSE;
        break;
    }
}

/**
 * g3k_context_new_from_gxr:
 * @gxr: (transfer full): a #GxrContext
 */
G3kContext *
g3k_context_new_from_gxr (GxrContext *gxr)
{
  if (!g3k_settings_is_schema_installed ("org.g3k"))
    {
      g_print ("GSettings schema not found. Check g3k installation!\n");
      return NULL;
    }

  G3kContext *self = (G3kContext *) g_object_new (G3K_TYPE_CONTEXT, 0);
  self->gxr = gxr;

  g_signal_connect (self->gxr, "state-change-event",
                    (GCallback) _state_change_cb, self);

  self->renderer = g3k_renderer_new (self);
  if (!self->renderer)
    {
      g_object_unref (self);
      g_printerr ("Could not init renderer.\n");
      return NULL;
    }

  GxrDeviceManager *dm = gxr_context_get_device_manager (gxr);
  g_signal_connect (dm, "device-activate-event",
                    (GCallback) _device_activate_cb, self);
  g_signal_connect (dm, "device-deactivate-event",
                    (GCallback) _device_deactivate_cb, self);

  self->source_ids.runtime_event = g_timeout_add (20,
                                                  (GSourceFunc)
                                                    _poll_runtime_events,
                                                  self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_input_poll_rate),
                                  "org.g3k", "input-poll-rate-ms", self);

  g3k_settings_connect_and_apply (G_CALLBACK (_update_enable_sound), "org.g3k",
                                  "enable-sound", self);

  return self;
}

G3kContext *
g3k_context_new_from_vulkan_extensions (GSList *instance_ext_list,
                                        GSList *device_ext_list)
{
  GxrContext *gxr = gxr_context_new_from_vulkan_extensions (instance_ext_list,
                                                            device_ext_list,
                                                            APP_NAME,
                                                            G3K_VERSION_HEX);
  return g3k_context_new_from_gxr (gxr);
}

G3kContext *
g3k_context_new_full (GSList  *instance_ext_list,
                      GSList  *device_ext_list,
                      char    *app_name,
                      uint32_t app_version)
{
  GxrContext *gxr = gxr_context_new_from_vulkan_extensions (instance_ext_list,
                                                            device_ext_list,
                                                            app_name,
                                                            app_version);
  if (!gxr)
    {
      g_warning ("Failed to create gxr context, unable to create g3k context");
      return NULL;
    }

  return g3k_context_new_from_gxr (gxr);
}

static void
_finalize (GObject *gobject)
{
  G3kContext *self = G3K_CONTEXT (gobject);

  if (self->wait_frame_thread)
    {
      g_mutex_lock (&self->wait_thread_mutex);
      self->shutdown_wait_frame_thread = 1;
      g_mutex_unlock (&self->wait_thread_mutex);
      g_cond_signal (&self->wait_thread_cond);

      g_thread_join (self->wait_frame_thread);
      self->wait_frame_thread = NULL;
      g_atomic_int_set (&self->shutdown_wait_frame_thread, 0);
      g_mutex_clear (&self->wait_thread_mutex);
      g_cond_clear (&self->wait_thread_cond);
    }

  if (self->source_ids.render_event > 0)
    g_source_remove (self->source_ids.render_event);

  if (self->source_ids.runtime_event > 0)
    g_source_remove (self->source_ids.runtime_event);

  if (self->source_ids.poll_input > 0)
    g_source_remove (self->source_ids.poll_input);

  g_hash_table_unref (self->controllers);
  g_clear_object (&self->root);
  g_clear_object (&self->renderer);
  g_clear_object (&self->gxr);
  g_object_unref (self->manager);

  g3k_settings_destroy ();

  ca_context_destroy (self->canberra_context);

  G_OBJECT_CLASS (g3k_context_parent_class)->finalize (gobject);
}

static void
g3k_context_class_init (G3kContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  signals[RENDER_EVENT] = g_signal_new ("render-event",
                                        G_TYPE_FROM_CLASS (klass),
                                        G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                        G_TYPE_NONE, 1,
                                        G_TYPE_POINTER
                                          | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[CONTROLLER_PRIMARY_CHANGED_EVENT]
    = g_signal_new ("controller-primary-changed", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    G_TYPE_POINTER | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[CONTROLLER_ACTIVATE_EVENT]
    = g_signal_new ("controller-activate", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    G_TYPE_POINTER | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[CONTROLLER_DEACTIVATE_EVENT]
    = g_signal_new ("controller-deactivate", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    G_TYPE_POINTER | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals[INPUT_POLL_EVENT] = g_signal_new ("input-poll",
                                            G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            NULL, G_TYPE_NONE, 0);

  signals[SHUTDOWN_EVENT] = g_signal_new ("shutdown", G_TYPE_FROM_CLASS (klass),
                                          G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                          NULL, G_TYPE_NONE, 0);
}

GxrContext *
g3k_context_get_gxr (G3kContext *self)
{
  return self->gxr;
}

GulkanContext *
g3k_context_get_gulkan (G3kContext *self)
{
  return gxr_context_get_gulkan (self->gxr);
}

G3kRenderer *
g3k_context_get_renderer (G3kContext *self)
{
  return self->renderer;
}

static void *
_wait_frame_thread (gpointer _self)
{
  G3kContext *self = G3K_CONTEXT (_self);

  while (true)
    {
      // note: Only locks the preconditions for xrWaitFrame, not xrWaitFrame
      g_mutex_lock (&self->wait_thread_mutex);

      if (self->shutdown_wait_frame_thread)
        {
          g_mutex_unlock (&self->wait_thread_mutex);
          break;
        }

      // for the last waited frame, no submit has happened yet
      while (self->submitted_frame_number < self->waited_frame_number)
        {
          if (self->shutdown_wait_frame_thread)
            {
              g_mutex_unlock (&self->wait_thread_mutex);
              g_debug ("Stopped G3k wait-frame thread, bye.");
              return NULL;
            }
          g_cond_wait (&self->wait_thread_cond, &self->wait_thread_mutex);
        }

      g_mutex_unlock (&self->wait_thread_mutex);

      if (!gxr_context_wait_frame (self->gxr))
        {
          g_printerr ("Failed to begin frame\n");
        }

      g_mutex_lock (&self->wait_thread_mutex);
      self->waited_frame_number++;
      g_mutex_unlock (&self->wait_thread_mutex);

      g_thread_yield ();
    }

  g_debug ("Stopped G3k wait-frame thread, bye.");

  return NULL;
}

gboolean
g3k_context_start_renderer (G3kContext *self)
{
  if (self->wait_frame_thread != NULL)
    {
      g_printerr ("g3k renderer already started");
      return false;
    }

  GError *error = NULL;
  self->wait_frame_thread = g_thread_try_new ("g3k-wait-frame",
                                              (GThreadFunc) _wait_frame_thread,
                                              self, &error);

  if (error != NULL)
    {
      g_printerr ("Unable to start wait frame thread: %s\n", error->message);
      g_error_free (error);
      return FALSE;
    }

  return TRUE;
}

void
g3k_context_render (G3kContext *self)
{
  g_mutex_lock (&self->wait_thread_mutex);

  // not waited for a frame yet
  if (self->waited_frame_number < 0)
    {
      g_mutex_unlock (&self->wait_thread_mutex);
      return;
    }

  // already submitted a frame matching the last waited frame
  if (self->submitted_frame_number >= self->waited_frame_number)
    {
      g_mutex_unlock (&self->wait_thread_mutex);
      return;
    }

  g_mutex_unlock (&self->wait_thread_mutex);

  if (!self->framecycle)
    return;

  if (!gxr_context_begin_frame (self->gxr))
    {
      g_printerr ("Failed to begin frame\n");
      return;
    }

  G3kRenderEvent render_start_event = {
    .type = G3K_RENDER_EVENT_FRAME_START,
  };
  g_signal_emit (self, signals[RENDER_EVENT], 0, &render_start_event);

  for (uint32_t eye = 0; eye < 2; eye++)
    gxr_context_get_view (self->gxr, eye, &self->views[eye]);

  if (!self->have_projection)
    {
      for (uint32_t eye = 0; eye < 2; eye++)
        gxr_context_get_projection (self->gxr, eye, self->near, self->far,
                                    &self->projections[eye]);
      self->have_projection = TRUE;
    }

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_multiply (&self->views[eye], &self->projections[eye],
                                &self->view_projections[eye]);
    }

  if (self->rendering)
    {
      if (!g3k_renderer_draw (self->renderer))
        {
          g_printerr ("Failed to draw frame\n");
        }
    }

  G3kRenderEvent render_end_event = {
    .type = G3K_RENDER_EVENT_FRAME_END,
  };
  g_signal_emit (self, signals[RENDER_EVENT], 0, &render_end_event);

  if (!gxr_context_end_frame (self->gxr, self->near, self->far, 0.0, 1.0))
    {
      g_printerr ("Failed to end frame\n");
      return;
    }

  g_mutex_lock (&self->wait_thread_mutex);
  self->submitted_frame_number++;
  g_cond_signal (&self->wait_thread_cond);
  g_mutex_unlock (&self->wait_thread_mutex);
}

graphene_matrix_t *
g3k_context_get_views (G3kContext *self)
{
  return self->views;
}

graphene_matrix_t *
g3k_context_get_projections (G3kContext *self)
{
  return self->projections;
}

graphene_matrix_t *
g3k_context_get_vps (G3kContext *self)
{
  return self->view_projections;
}

/**
 * g3k_context_get_upload_layout:
 * @self: The #G3kContext
 *
 * Returns: (transfer none): an #VkImageLayout
 */
VkImageLayout
g3k_context_get_upload_layout (G3kContext *self)
{
  return self->upload_layout;
}

G3kController *
g3k_context_get_controller (G3kContext *self, GxrController *gxr_controller)
{
  return g_hash_table_lookup (self->controllers, gxr_controller);
}

GList *
g3k_context_get_controllers (G3kContext *self)
{
  return g_hash_table_get_values (self->controllers);
}

static G3kController *
_init_controller (G3kContext *self, GxrController *gxr_controller)
{
  g_debug ("g3k: Controller %lu %p activated.",
           gxr_device_get_handle (GXR_DEVICE (gxr_controller)),
           (void *) gxr_controller);

  G3kController *controller = g3k_controller_new (self, gxr_controller);

  g_hash_table_insert (self->controllers, gxr_controller, controller);
  g_debug ("shell: Created G3kController %p for GxrController %p",
           (void *) controller, (void *) gxr_controller);

  g3k_controller_init_children (controller);

  if (self->primary_controller == NULL)
    {
      self->primary_controller = controller;
    }

  g3k_object_add_child (self->root, G3K_OBJECT (controller), UINT32_MAX);

  return controller;
}

static void
_device_activate_cb (GxrDeviceManager *dm, GxrDevice *device, gpointer _self)
{
  (void) dm;

  G3kContext *self = G3K_CONTEXT (_self);

  if (!gxr_device_is_controller (device))
    return;

  GxrController *gxr_controller = GXR_CONTROLLER (device);
  G3kController *controller = _init_controller (self, gxr_controller);

  g_signal_emit (self, signals[CONTROLLER_ACTIVATE_EVENT], 0, controller);
}

static void
_device_deactivate_cb (GxrDeviceManager *dm, GxrDevice *device, gpointer _self)
{
  (void) dm;

  if (!gxr_device_is_controller (device))
    return;

  G3kContext    *self = _self;
  GxrController *gxr_controller = GXR_CONTROLLER (device);
  G3kController *controller = g_hash_table_lookup (self->controllers,
                                                   gxr_controller);

  g_debug ("g3k: Controller %lu deactivated.",
           gxr_device_get_handle (GXR_DEVICE (controller)));

  g3k_object_remove_child (self->root, G3K_OBJECT (controller));

  g_hash_table_remove (self->controllers, gxr_controller);

  GList *controllers = g_hash_table_get_values (self->controllers);
  if (g_list_length (controllers) > 0)
    {
      self->primary_controller = G3K_CONTROLLER (g_list_nth_data (controllers,
                                                                  0));
    }
  else
    {
      self->primary_controller = NULL;
    }

  g_signal_emit (self, signals[CONTROLLER_DEACTIVATE_EVENT], 0, controller);
}

G3kController *
g3k_context_init_dummy_controller (G3kContext *self)
{
  GxrController *gxr_controller = gxr_controller_new (1);
  _init_controller (self, gxr_controller);
  return G3K_CONTROLLER (g_hash_table_lookup (self->controllers,
                                              gxr_controller));
}

/**
 * g3k_context_get_primary_controller:
 * @self: The #G3kContext
 *
 * Returns: (transfer none): The G3kController that is used for input synth.
 */
G3kController *
g3k_context_get_primary_controller (G3kContext *self)
{
  return self->primary_controller;
}

/**
 * g3k_context_make_controller_primary:
 * @self: The #G3kContext
 * @controller: The index of the controller that will be used for input synth.
 */
void
g3k_context_make_controller_primary (G3kContext    *self,
                                     G3kController *controller)
{
  self->primary_controller = controller;
  g_signal_emit (self, signals[CONTROLLER_PRIMARY_CHANGED_EVENT], 0,
                 controller);
}

gboolean
g3k_context_is_controller_primary (G3kContext *self, G3kController *controller)
{
  return self->primary_controller == controller;
}

G3kObject *
g3k_context_get_root (G3kContext *self)
{
  return self->root;
}

G3kObjectManager *
g3k_context_get_manager (G3kContext *self)
{
  return self->manager;
}

float
g3k_context_get_ms_since_last_poll (G3kContext *self)
{
  return (float) (g_get_monotonic_time () - self->last_poll_timestamp) / 1000.f;
}

static uint32_t sound_id = 0;

void
g3k_context_play_sound (G3kContext *self, const char *event_id)
{
  if (!self->enable_sound)
    {
      return;
    }
  ca_context_play (self->canberra_context, sound_id, CA_PROP_EVENT_ID, event_id,
                   CA_PROP_EVENT_DESCRIPTION, "G3k Sound", NULL);
  sound_id += 1;
}

void
g3k_context_play_sound_path (G3kContext *self, const char *path)
{
  if (!self->enable_sound)
    {
      return;
    }
  ca_context_play (self->canberra_context, sound_id, CA_PROP_MEDIA_FILENAME,
                   path, CA_PROP_EVENT_DESCRIPTION, "G3k Sound", NULL);
  sound_id += 1;
}
