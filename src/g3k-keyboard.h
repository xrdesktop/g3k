/*
 * g3k
 * Copyright 2021 Remco Kranenburg <remco@burgsoft.nl>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_KEYBOARD_H_
#define G3K_KEYBOARD_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>

#include "g3k-keyboard-button.h"

G_BEGIN_DECLS

#define G3K_TYPE_KEYBOARD g3k_keyboard_get_type ()
G_DECLARE_FINAL_TYPE (G3kKeyboard, g3k_keyboard, G3K, KEYBOARD, GObject)

G3kKeyboard *
g3k_keyboard_new (G3kContext *context);

GHashTable *
g3k_keyboard_get_modes (G3kKeyboard *self);

void
g3k_keyboard_click_cb (G3kKeyboardButton *button,
                       GxrController     *controller,
                       gpointer           user_data);

void
g3k_keyboard_show (G3kKeyboard *self);

void
g3k_keyboard_hide (G3kKeyboard *self);

gboolean
g3k_keyboard_is_visible (G3kKeyboard *self);

G_END_DECLS

#endif /* G3K_KEYBOARD_H_ */
