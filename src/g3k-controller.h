/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_CONTROLLER_H_
#define G3K_CONTROLLER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include <glib-object.h>
#include <graphene.h>

#include "g3k-ray.h"
#include "g3k-selection.h"
#include "g3k-tip.h"

G_BEGIN_DECLS

#define G3K_TYPE_CONTROLLER g3k_controller_get_type ()
G_DECLARE_FINAL_TYPE (G3kController, g3k_controller, G3K, CONTROLLER, G3kObject)

/**
 * G3kTransformLock:
 * @XRD_TRANSFORM_LOCK_NONE: The grab action does not currently have a
 *transformation it is locked to.
 * @XRD_TRANSFORM_LOCK_PUSH_PULL: Only push pull transformation can be
 *performed.
 * @XRD_TRANSFORM_LOCK_SCALE: Only a scale transformation can be performed.
 *
 * The type of transformation the grab action is currently locked to.
 * This will be detected at the begginging of a grab transformation
 * and reset after the transformation is done.
 *
 **/
typedef enum
{
  XRD_TRANSFORM_LOCK_NONE,
  XRD_TRANSFORM_LOCK_PUSH_PULL,
  XRD_TRANSFORM_LOCK_SCALE
} G3kTransformLock;

typedef struct
{
  G3kObject       *object;
  float            distance;
  graphene_point_t intersection_2d;
} G3kHoverState;

typedef struct
{
  G3kObject            *object;
  graphene_quaternion_t object_rotation;
  graphene_quaternion_t inverse_controller_rotation;
  graphene_point_t      offset;
  G3kTransformLock      transform_lock;
} G3kGrabState;

G3kController *
g3k_controller_new (G3kContext *g3k, GxrController *controller);

G3kRay *
g3k_controller_get_ray (G3kController *self);

G3kTip *
g3k_controller_get_tip (G3kController *self);

G3kHoverState *
g3k_controller_get_hover_state (G3kController *self);

G3kGrabState *
g3k_controller_get_grab_state (G3kController *self);

void
g3k_controller_update_grab (G3kController *self);

void
g3k_controller_reset_hover_state (G3kController *self);

void
g3k_controller_hide_pointers (G3kController *self);

void
g3k_controller_show_pointers (G3kController *self);

void
g3k_controller_update_hovered_object (G3kController      *self,
                                      G3kObject          *last_object,
                                      G3kObject          *object,
                                      graphene_point3d_t *global_intersection,
                                      graphene_point_t   *local_intersection,
                                      float intersection_distance);

void
g3k_controller_drag_start (G3kController *self, G3kObject *grabbed_object);

G3kSelection *
g3k_controller_get_selection (G3kController *self);

GxrController *
g3k_controller_get_controller (G3kController *self);

gboolean
g3k_controller_get_intersection (G3kController      *self,
                                 G3kObject          *object,
                                 float              *distance,
                                 graphene_point3d_t *intersection_point);

gboolean
g3k_controller_init_children (G3kController *self);

const gchar *
g3k_transform_lock_string (G3kTransformLock lock);

void
g3k_controller_check_grab (G3kController *self);

void
g3k_controller_check_release (G3kController *self);

void
g3k_controller_trigger_haptic (G3kController *self,
                               float          duration_seconds,
                               float          frequency,
                               float          amplitude);

void
g3k_controller_set_haptic_action (G3kController *self, GxrAction *action);

G_END_DECLS

#endif /* G3K_CONTROLLER_H_ */
