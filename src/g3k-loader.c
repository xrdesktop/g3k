/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Copyright 2010-2014 The three.js authors
 * Copyright 2014-2020 The gthree authors
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-loader.h"

#include <json-glib/json-glib.h>

#include "g3k-attribute.h"
#include "g3k-mesh.h"
#include "g3k-model.h"
#include "g3k-sampler.h"

typedef struct _G3kLoaderPrivate
{
  G3kContext *context;

  GPtrArray *buffers;
  GPtrArray *buffer_views;
  GPtrArray *images;
  GPtrArray *accessors;
  GPtrArray *nodes;
  GPtrArray *meshes;
  GPtrArray *scenes;
  GPtrArray *samplers;
  GPtrArray *textures;
  GPtrArray *materials;
  GPtrArray *root_nodes;

  G3kMaterial *default_material;
  int          scene;

  G3kObject *root;

  GHashTable *pipelines;

} G3kLoaderPrivate;

G_DEFINE_QUARK (g3k - loader - error - quark, g3k_loader_error)
G_DEFINE_TYPE_WITH_PRIVATE (G3kLoader, g3k_loader, G_TYPE_OBJECT)

typedef struct
{
  guint   buffer;
  gsize   byte_offset;
  GBytes *bytes; /* combines buffer + byte offset/length +  the actual buffer
                    GBytes */
  gsize byte_length;
  gsize byte_stride;
  guint target;

  /* Set later for reuse when we know the attribute type to user */
  G3kAttributeArray *array;

} G3kBufferView;

static void
buffer_view_free (G3kBufferView *view)
{
  if (view->array)
    g3k_attribute_array_unref (view->array);
  if (view->bytes)
    g_bytes_unref (view->bytes);
  g_free (view);
}

static void
accessor_free (G3kAccessor *accessor)
{
  if (accessor->array)
    g3k_attribute_array_unref (accessor->array);
  g_free (accessor);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (G3kBufferView, buffer_view_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (G3kAccessor, accessor_free)

static G3kBufferView *
buffer_view_new (G3kLoader *loader, JsonObject *json, GError **error)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  g_autoptr (G3kBufferView) view = g_new0 (G3kBufferView, 1);

  view->array = NULL;

  if (!json_object_has_member (json, "buffer"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "BufferView lacks buffer");
      return NULL;
    }
  view->buffer = (guint) json_object_get_int_member (json, "buffer");
  if (view->buffer >= priv->buffers->len)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "BufferView refers to non-existing buffer");
      return NULL;
    }
  view->byte_offset = 0;
  if (json_object_has_member (json, "byteOffset"))
    view->byte_offset = (guint) json_object_get_int_member (json, "byteOffset");

  if (!json_object_has_member (json, "byteLength"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "BufferView lacks byteLength");
      return NULL;
    }
  view->byte_length = (guint) json_object_get_int_member (json, "byteLength");

  view->byte_stride = 0;
  if (json_object_has_member (json, "byteStride"))
    view->byte_stride = (guint) json_object_get_int_member (json, "byteStride");

  view->target = 0;
  if (json_object_has_member (json, "target"))
    view->target = (guint) json_object_get_int_member (json, "target");

  view->bytes = g_bytes_new_from_bytes (g_ptr_array_index (priv->buffers,
                                                           view->buffer),
                                        view->byte_offset, view->byte_length);

  return g_steal_pointer (&view);
}

static gsize
g3k_loader_gltf_type_size (const char *type)
{
  if (strcmp (type, "SCALAR") == 0)
    return 1;
  else if (strcmp (type, "VEC2") == 0)
    return 2;
  else if (strcmp (type, "VEC3") == 0)
    return 3;
  else if (strcmp (type, "VEC4") == 0)
    return 4;
  else if (strcmp (type, "MAT2") == 0)
    return 4;
  else if (strcmp (type, "MAT3") == 0)
    return 9;
  else if (strcmp (type, "MAT4") == 0)
    return 16;
  else
    {
      // Unreachable
      g_error ("Unknown size for GLTF type %s", type);
      return 0;
    }
}

static void
g3k_loader_finalize (GObject *gobject);

static void
g3k_loader_class_init (G3kLoaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = g3k_loader_finalize;
}

static char *
sanitize_name (const char *name)
{
  GString *s = g_string_new ("");

  while (*name)
    {
      char c = *name++;
      if (c != '[' && c != ']' && c != '.' && c != ':' && c != '/')
        g_string_append_c (s, c);
    }

  return g_string_free (s, FALSE);
}

static void
parse_point3d (JsonArray *point_j, graphene_point3d_t *p)
{
  p->x = (float) json_array_get_double_element (point_j, 0);
  p->y = (float) json_array_get_double_element (point_j, 1);
  p->z = (float) json_array_get_double_element (point_j, 2);
}

static void
parse_quaternion (JsonArray *quat_j, graphene_quaternion_t *q)
{
  graphene_quaternion_init (q,
                            (float) json_array_get_double_element (quat_j, 0),
                            (float) json_array_get_double_element (quat_j, 1),
                            (float) json_array_get_double_element (quat_j, 2),
                            (float) json_array_get_double_element (quat_j, 3));
}

static void
parse_matrix (JsonArray *matrix_j, graphene_matrix_t *m)
{
  float floats[16];
  int   i;

  for (i = 0; i < 16; i++)
    floats[i] = (float) json_array_get_double_element (matrix_j, (guint) i);

  graphene_matrix_init_from_float (m, floats);
}

static void
g3k_loader_init (G3kLoader *self)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (self);
  graphene_vec3_t   magenta;
  graphene_vec3_init (&magenta, 1, 0, 1);

  // clang-format off
  priv->buffers =      g_ptr_array_new_with_free_func ((GDestroyNotify) g_bytes_unref);
  priv->buffer_views = g_ptr_array_new_with_free_func ((GDestroyNotify) buffer_view_free);
  priv->images =       g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->accessors =    g_ptr_array_new_with_free_func ((GDestroyNotify) accessor_free);
  priv->nodes =        g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->meshes =       g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->scenes =       g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->samplers =     g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->textures =     g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->materials =    g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  priv->root_nodes =   g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  // clang-format on

  priv->pipelines = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                           g_object_unref);
}

static GBytes *
data_url_parse (const char *url, char **out_mimetype, GError **error)
{
  g_autofree char *mimetype = NULL;
  const char      *parameters_start;
  const char      *data_start;
  gboolean         base64 = FALSE;
  gpointer         bdata;
  gsize            bsize;

  /* url must be an URI as defined in RFC 2397
   * data:[<mediatype>][;base64],<data>
   */
  if (g_ascii_strncasecmp ("data:", url, 5) != 0)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "Not a data: URL");
      return NULL;
    }

  url += 5;

  parameters_start = strchr (url, ';');
  data_start = strchr (url, ',');
  if (data_start == NULL)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "Malformed data: URL");
      return NULL;
    }

  if (parameters_start > data_start)
    parameters_start = NULL;

  if (data_start != url && parameters_start != url)
    {
      mimetype = g_strndup (url, (gsize) ((parameters_start ? parameters_start
                                                            : data_start)
                                          - url));
    }
  else
    {
      mimetype = NULL;
    }

  if (parameters_start != NULL)
    {
      char  *parameters_str;
      char **parameters;
      guint  i;

      parameters_str = g_strndup (parameters_start + 1,
                                  (gsize) (data_start - parameters_start - 1));
      parameters = g_strsplit (parameters_str, ";", -1);

      for (i = 0; parameters[i] != NULL; i++)
        {
          if (g_ascii_strcasecmp ("base64", parameters[i]) == 0)
            {
              base64 = TRUE;
            }
        }
      g_free (parameters_str);
      g_strfreev (parameters);
    }

  /* Skip comma */
  data_start += 1;
  if (base64)
    {
      bdata = g_base64_decode (data_start, &bsize);
    }
  else
    {
      /* URI encoded, i.e. "percent" encoding */
      /* XXX: This doesn't allow nul bytes */
      bdata = g_uri_unescape_string (data_start, NULL);
      if (bdata == NULL)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Could not unescape string");
          g_free (mimetype);
          return NULL;
        }
      bsize = strlen (bdata);
    }

  if (out_mimetype)
    *out_mimetype = g_steal_pointer (&mimetype);

  return g_bytes_new_take (bdata, bsize);
}

static gboolean
supports_extension (const char *extension)
{
  if (strcmp (extension, "KHR_materials_pbrSpecularGlossiness") == 0)
    return TRUE;
  return FALSE;
}

static gboolean
parse_extensions_required (JsonObject *root)
{
  if (!json_object_has_member (root, "extensionsRequired"))
    return TRUE;

  JsonArray *exts = json_object_get_array_member (root, "extensionsRequired");
  for (guint i = 0; i < json_array_get_length (exts); i++)
    {
      const char *ext = json_array_get_string_element (exts, i);
      if (!supports_extension (ext))
        {
          g_warning ("Unsupported required GLTF extension %s", ext);
        }
    }

  return TRUE;
}

static gboolean
parse_extensions_used (JsonObject *root)
{
  if (!json_object_has_member (root, "extensionsUsed"))
    return TRUE;

  JsonArray *exts = json_object_get_array_member (root, "extensionsUsed");
  for (guint i = 0; i < json_array_get_length (exts); i++)
    {
      const char *ext = json_array_get_string_element (exts, i);
      if (!supports_extension (ext))
        {
          g_warning ("Unsupported used GLTF extension %s", ext);
        }
    }

  return TRUE;
}

static gboolean
parse_asset (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) loader;
  JsonObject *asset = NULL;
  const char *version;

  if (!json_object_has_member (root, "asset"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "No asset field, is this really a GLTF file?");
      return FALSE;
    }

  asset = json_object_get_object_member (root, "asset");

  if (!json_object_has_member (asset, "version"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "No GLTF version specified");
      return FALSE;
    }

  version = json_object_get_string_member (asset, "version");
  if (strcmp (version, "2.0") != 0)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "Unsupported GLTF version %s", version);
      return FALSE;
    }

  return TRUE;
}

static GBytes *
load_uri (const char *uri, gint64 byte_length, GFile *base_path, GError **error)
{
  if (g_ascii_strncasecmp ("data:", uri, 5) == 0)
    {
      return data_url_parse (uri, NULL, error);
    }
  else
    {
      g_autoptr (GFile) file = NULL;
      g_autoptr (GBytes) file_bytes = NULL;

      char *unescaped = g_uri_unescape_string (uri, NULL);
      if (base_path)
        file = g_file_resolve_relative_path (base_path, unescaped);
      else
        file = g_file_new_for_commandline_arg (unescaped);
      g_free (unescaped);

      file_bytes = g_file_load_bytes (file, NULL, NULL, error);
      if (file_bytes == NULL)
        return NULL;

      if (byte_length > 0)
        return g_bytes_new_from_bytes (file_bytes, 0, (gsize) byte_length);
      else
        return g_bytes_ref (file_bytes);
    }
}

static gboolean
parse_buffers (G3kLoader  *loader,
               JsonObject *root,
               GBytes     *bin_chunk,
               GFile      *base_path,
               GError    **error)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *buffers_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "buffers"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "No buffers specified");
      return FALSE;
    }
  buffers_j = json_object_get_array_member (root, "buffers");
  len = json_array_get_length (buffers_j);

  for (i = 0; i < len; i++)
    {
      JsonObject *buffer_j = json_array_get_object_element (buffers_j, i);
      gint64      byte_length = -1;
      const char *uri = NULL;
      g_autoptr (GBytes) bytes = NULL;

      if (json_object_has_member (buffer_j, "byteLength"))
        byte_length = json_object_get_int_member (buffer_j, "byteLength");

      if (json_object_has_member (buffer_j, "uri"))
        uri = json_object_get_string_member (buffer_j, "uri");

      if (uri == NULL)
        {
          if (i == 0 && bin_chunk != NULL)
            {
              if (byte_length == -1)
                {
                  g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                               "Binary chunk size missing");
                  return FALSE;
                }
              if (g_bytes_get_size (bin_chunk) < (gsize) byte_length)
                {
                  g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                               "Binary chunk too short");
                  return FALSE;
                }
              bytes = g_bytes_new_from_bytes (bin_chunk, 0,
                                              (gsize) byte_length);
            }
          else
            {
              g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                           "Missing url from buffer %d", i);
              return FALSE;
            }
        }
      else
        {
          bytes = load_uri (uri, byte_length, base_path, error);
          if (bytes == NULL)
            return FALSE;
        }

      g_ptr_array_add (priv->buffers, g_steal_pointer (&bytes));
    }

  return TRUE;
}

static gboolean
parse_buffer_views (G3kLoader *loader, JsonObject *root, GError **error)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *buffer_views_j;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "bufferViews"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "No bufferViews specified");
      return FALSE;
    }
  buffer_views_j = json_object_get_array_member (root, "bufferViews");
  len = json_array_get_length (buffer_views_j);

  for (i = 0; i < len; i++)
    {
      JsonObject *buffer_view_j = json_array_get_object_element (buffer_views_j,
                                                                 i);
      g_autoptr (G3kBufferView) buffer_view = NULL;

      buffer_view = buffer_view_new (loader, buffer_view_j, error);
      if (buffer_view == NULL)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Could not create buffer view.");
          return FALSE;
        }

      g_ptr_array_add (priv->buffer_views, g_steal_pointer (&buffer_view));
    }

  return TRUE;
}

static gboolean
parse_accessors (G3kLoader *loader, JsonObject *root, GError **error)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);

  if (!json_object_has_member (root, "accessors"))
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "No accessors specified");
      return FALSE;
    }
  JsonArray *accessors_j = json_object_get_array_member (root, "accessors");
  for (guint i = 0; i < json_array_get_length (accessors_j); i++)
    {
      JsonObject *accessor_j = json_array_get_object_element (accessors_j, i);

      // bufferView
      if (!json_object_has_member (accessor_j, "bufferView"))
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "No valid bufferView element.");
          return FALSE;
        }
      gint64 buffer_view = json_object_get_int_member (accessor_j,
                                                       "bufferView");
      g_assert (buffer_view >= 0);

      if (buffer_view >= priv->buffer_views->len)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Buffer view %ld is outside of length %d", buffer_view,
                       priv->buffer_views->len);
          return FALSE;
        }

      // byteOffset
      gsize byte_offset = 0;
      if (json_object_has_member (accessor_j, "byteOffset"))
        byte_offset = (gsize) json_object_get_int_member (accessor_j,
                                                          "byteOffset");

      // componentType
      if (!json_object_has_member (accessor_j, "componentType"))
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "No valid componentType element.");
          return FALSE;
        }
      gint64 component_type = json_object_get_int_member (accessor_j,
                                                          "componentType");

      // normalized
      gboolean normalized = FALSE;
      if (json_object_has_member (accessor_j, "normalized"))
        normalized = json_object_get_boolean_member (accessor_j, "normalized");

      gsize count = (gsize) json_object_get_int_member (accessor_j, "count");
      const char *type = json_object_get_string_member (accessor_j, "type");

      g_autoptr (G3kAccessor) accessor = g_new0 (G3kAccessor, 1);
      accessor->normalized = normalized;

      G3kAttributeType attribute_type
        = g3k_attribute_type_from_component_type (component_type);

      G3kBufferView *view = g_ptr_array_index (priv->buffer_views, buffer_view);

      gsize attribute_type_size = g3k_attribute_type_size (attribute_type);
      gsize item_size = g3k_loader_gltf_type_size (type);

      if (view->array && view->array->type != attribute_type)
        {
          g_warning ("Array Type %s != Attribute type %s\n",
                     g3k_attribute_type_string (view->array->type),
                     g3k_attribute_type_string (attribute_type));
        }

      if (view->array && view->array->type == attribute_type)
        {
          /* We already have an array for this buffer view, and it matches the
             type, its either a pure re-use, or an interleaved array we can
             reuse. */
          accessor->array = g3k_attribute_array_ref (view->array);
          accessor->item_size = item_size;
          accessor->item_offset = byte_offset / attribute_type_size;
          accessor->count = count;
        }
      else
        {
          gsize array_stride;
          if (view->byte_stride == 0) // 0 == densely packed
            array_stride = item_size;
          else
            array_stride = view->byte_stride / attribute_type_size;

          gsize array_count = view->byte_length
                              / (attribute_type_size * array_stride);

          /* Create an array for the entire bufferview now that we know the
             type, then store that for later use and use a subset of it here. */
          accessor->array = g3k_attribute_array_new (attribute_type,
                                                     array_count, array_stride);
          accessor->item_size = item_size;
          accessor->item_offset = byte_offset / attribute_type_size;
          accessor->count = count;
          accessor->array->bytes = g_bytes_ref (view->bytes);

          if (view->array == NULL)
            view->array = g3k_attribute_array_ref (accessor->array);
        }

      g_ptr_array_add (priv->accessors, g_steal_pointer (&accessor));
    }

  return TRUE;
}

static void
convert_wrapping (int wrapping_gltf, VkSamplerAddressMode *wrapping_vk)
{
  switch (wrapping_gltf)
    {
      case 33071:
        *wrapping_vk = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        break;
      case 33648:
        *wrapping_vk = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
        break;
      case 10497:
        *wrapping_vk = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        break;
      default:
        g_warning ("unknown wrapping %d", wrapping_gltf);
        *wrapping_vk = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        break;
    }
}

static void
convert_filter (int                  filter_gltf,
                VkFilter            *filter_vk,
                VkSamplerMipmapMode *mipmap_vk)
{
  switch (filter_gltf)
    {
      case 9728:
        *filter_vk = VK_FILTER_NEAREST;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;
      case 9729:
        *filter_vk = VK_FILTER_LINEAR;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        break;
      case 9984:
        *filter_vk = VK_FILTER_NEAREST;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;
      case 9985:
        *filter_vk = VK_FILTER_LINEAR;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;
      case 9986:
        *filter_vk = VK_FILTER_NEAREST;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        break;
      case 9987:
        *filter_vk = VK_FILTER_LINEAR;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        break;

      default:
        g_warning ("unknown filter %d", filter_gltf);
        *filter_vk = VK_FILTER_NEAREST;
        *mipmap_vk = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;
    }
}

static gboolean
parse_samplers (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *samplers_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "samplers"))
    return TRUE;

  samplers_j = json_object_get_array_member (root, "samplers");
  len = json_array_get_length (samplers_j);
  for (i = 0; i < len; i++)
    {
      JsonObject *sampler_j = json_array_get_object_element (samplers_j, i);
      g_autoptr (G3kSampler) sampler = g3k_sampler_new ();

      if (json_object_has_member (sampler_j, "magFilter"))
        convert_filter ((int) json_object_get_int_member (sampler_j,
                                                          "magFilter"),
                        &sampler->mag_filter, &sampler->mipmap_mode);
      if (json_object_has_member (sampler_j, "minFilter"))
        convert_filter ((int) json_object_get_int_member (sampler_j,
                                                          "minFilter"),
                        &sampler->min_filter, &sampler->mipmap_mode);
      if (json_object_has_member (sampler_j, "wrapS"))
        convert_wrapping ((int) json_object_get_int_member (sampler_j, "wrapS"),
                          &sampler->wrap_s);
      if (json_object_has_member (sampler_j, "wrapT"))
        convert_wrapping ((int) json_object_get_int_member (sampler_j, "wrapT"),
                          &sampler->wrap_t);

      g_ptr_array_add (priv->samplers, g_steal_pointer (&sampler));
    }

  return TRUE;
}

static gboolean
parse_images (G3kLoader  *loader,
              JsonObject *root,
              GFile      *base_path,
              GError    **error)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *images_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "images"))
    return TRUE;

  images_j = json_object_get_array_member (root, "images");
  len = json_array_get_length (images_j);
  for (i = 0; i < len; i++)
    {
      JsonObject *image_j = json_array_get_object_element (images_j, i);
      g_autoptr (GdkPixbuf) pixbuf = NULL;
      g_autoptr (GBytes) bytes = NULL;
      g_autoptr (GInputStream) in = NULL;

      if (json_object_has_member (image_j, "uri"))
        {
          const char *uri;

          uri = json_object_get_string_member (image_j, "uri");

          bytes = load_uri (uri, -1, base_path, error);
          if (bytes == NULL)
            return FALSE;
        }
      else if (json_object_has_member (image_j, "bufferView"))
        {
          G3kBufferView *view;
          gint64         v = json_object_get_int_member (image_j, "bufferView");

          if (v >= priv->buffer_views->len)
            {
              g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                           "No buffer view %d in image %d", (int) v, (int) i);
              return FALSE;
            }
          view = g_ptr_array_index (priv->buffer_views, v);
          bytes = g_bytes_ref (view->bytes);
        }
      else
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Missing url or bufferView from image buffer %d", i);
          return FALSE;
        }

      in = g_memory_input_stream_new_from_bytes (bytes);
      pixbuf = gdk_pixbuf_new_from_stream (in, NULL, error);
      if (pixbuf == NULL)
        return FALSE;

      g_ptr_array_add (priv->images, g_steal_pointer (&pixbuf));
    }

  return TRUE;
}

static GulkanTexture *
_init_texture_with_sampler (G3kContext *context,
                            GdkPixbuf  *pixbuf,
                            G3kSampler *sampler)
{
  GulkanContext *gulkan = g3k_context_get_gulkan (context);
  VkDevice       device = gulkan_context_get_device_handle (gulkan);

  GdkPixbuf *pixbuf_rgba = gdk_pixbuf_add_alpha (pixbuf, FALSE, 0, 0, 0);

  GulkanTexture *texture
    = gulkan_texture_new_from_pixbuf (gulkan, pixbuf_rgba,
                                      VK_FORMAT_R8G8B8A8_SRGB,
                                      g3k_context_get_upload_layout (context),
                                      false);

  g_object_unref (pixbuf_rgba);

  guint mip_levels = gulkan_texture_get_mip_levels (texture);

  g_assert (GULKAN_IS_TEXTURE (texture));

  VkSamplerCreateInfo sampler_info = {
    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    .magFilter = sampler->mag_filter,
    .minFilter = sampler->min_filter,
    .mipmapMode = sampler->mipmap_mode,
    .addressModeU = sampler->wrap_s,
    .addressModeV = sampler->wrap_t,
    .anisotropyEnable = VK_TRUE,
    .maxAnisotropy = 16.0f,
    .minLod = 0.0f,
    .maxLod = (float) mip_levels,
  };

  VkSampler vk_sampler;
  vkCreateSampler (device, &sampler_info, NULL, &vk_sampler);

  gulkan_texture_set_sampler (texture, vk_sampler);

  return texture;
}

static gboolean
parse_textures (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *textures_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "textures"))
    return TRUE;

  textures_j = json_object_get_array_member (root, "textures");
  len = json_array_get_length (textures_j);
  for (i = 0; i < len; i++)
    {
      JsonObject *texture_j = json_array_get_object_element (textures_j, i);

      int         sampler_idx, source_idx;
      G3kSampler *sampler;
      GdkPixbuf  *image;

      if (json_object_has_member (texture_j, "sampler"))
        {
          sampler_idx = (int) json_object_get_int_member (texture_j, "sampler");
          sampler = g_ptr_array_index (priv->samplers, sampler_idx);
        }
      else
        {
          sampler = g3k_sampler_new ();
        }

      source_idx = (int) json_object_get_int_member (texture_j, "source");

      image = g_ptr_array_index (priv->images, source_idx);

      GulkanTexture *texture = _init_texture_with_sampler (priv->context, image,
                                                           sampler);

      g_ptr_array_add (priv->textures, g_steal_pointer (&texture));
    }

  return TRUE;
}

static gboolean
parse_meshes (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *meshes_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "meshes"))
    return TRUE;

  meshes_j = json_object_get_array_member (root, "meshes");
  len = json_array_get_length (meshes_j);
  for (i = 0; i < len; i++)
    {
      JsonObject *mesh_j = json_array_get_object_element (meshes_j, i);
      JsonArray  *primitives;
      int         primitives_len, j;
      g_autoptr (G3kMesh) mesh = g3k_mesh_new ();

      if (json_object_has_member (mesh_j, "name"))
        mesh->name = sanitize_name (json_object_get_string_member (mesh_j,
                                                                   "name"));

      primitives = json_object_get_array_member (mesh_j, "primitives");
      primitives_len = (int) json_array_get_length (primitives);

      for (j = 0; j < primitives_len; j++)
        {
          g_autoptr (G3kPrimitive) primitive = g3k_primitive_new (priv
                                                                    ->context);

          JsonObject *primitive_j = json_array_get_object_element (primitives,
                                                                   (guint) j);
          JsonObject *attributes = json_object_get_object_member (primitive_j,
                                                                  "attributes");
          int         mode = 4;
          int         material = -1;
          g_autoptr (GList) members = json_object_get_members (attributes);

          for (GList *l = members; l != NULL; l = l->next)
            {
              const char *attr_name = l->data;
              gint64 accessor_index = json_object_get_int_member (attributes,
                                                                  attr_name);

              G3kAccessor *accessor = g_ptr_array_index (priv->accessors,
                                                         accessor_index);

              g3k_primitive_add_attribute (primitive, attr_name, accessor);
            }

          if (json_object_has_member (primitive_j, "indices"))
            {
              int index_index = (int) json_object_get_int_member (primitive_j,
                                                                  "indices");
              G3kAccessor *accessor = g_ptr_array_index (priv->accessors,
                                                         index_index);
              g3k_primitive_add_index (primitive, accessor);
            }

          if (json_object_has_member (primitive_j, "mode"))
            {
              mode = (int) json_object_get_int_member (primitive_j, "mode");
              if (mode != 4)
                {
                  // TODO: Support modes other than Triangle List
                  g_warning ("Unsupported pimitive mode %d.", mode);
                  return FALSE;
                }
            }

          if (json_object_has_member (primitive_j, "material"))
            material = (int) json_object_get_int_member (primitive_j,
                                                         "material");

          if (material != -1)
            primitive->material
              = g_object_ref (g_ptr_array_index (priv->materials, material));
          else
            primitive->material = g_object_ref (priv->default_material);

          g3k_primitive_init_vertex_buffer (primitive, priv->context);

          g_ptr_array_add (mesh->primitives, g_steal_pointer (&primitive));
        }

      g_ptr_array_add (priv->meshes, g_steal_pointer (&mesh));
    }

  return TRUE;
}

static gboolean
parse_nodes (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *nodes_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "nodes"))
    return TRUE;

  nodes_j = json_object_get_array_member (root, "nodes");
  len = json_array_get_length (nodes_j);

  /* First create all all base nodes with the right type and local transform */
  for (i = 0; i < len; i++)
    {
      JsonObject *node_j = json_array_get_object_element (nodes_j, i);
      g_autoptr (G3kModel) node = g3k_model_new (priv->context);
      graphene_point3d_t    scale = {1.f, 1.f, 1.f}, translate = {0, 0, 0};
      graphene_quaternion_t rotate;
      graphene_matrix_t     m;

      graphene_matrix_init_identity (&m);

      graphene_quaternion_init_identity (&rotate);

      if (json_object_has_member (node_j, "matrix"))
        {
          JsonArray *matrix_j = json_object_get_array_member (node_j, "matrix");

          parse_matrix (matrix_j, &m);
        }
      else
        {
          if (json_object_has_member (node_j, "translation"))
            {
              JsonArray *translation_j
                = json_object_get_array_member (node_j, "translation");
              parse_point3d (translation_j, &translate);
            }
          if (json_object_has_member (node_j, "scale"))
            {
              JsonArray *scale_j = json_object_get_array_member (node_j,
                                                                 "scale");
              parse_point3d (scale_j, &scale);
            }
          if (json_object_has_member (node_j, "rotation"))
            {
              JsonArray *rotation_j = json_object_get_array_member (node_j,
                                                                    "rotation");
              parse_quaternion (rotation_j, &rotate);
            }

          graphene_matrix_scale (&m, scale.x, scale.y, scale.z);
          graphene_matrix_rotate_quaternion (&m, &rotate);
          graphene_matrix_translate (&m, &translate);
        }
      g3k_object_set_local_matrix (G3K_OBJECT (node), &m);

      if (json_object_has_member (node_j, "name"))
        {
          g_autofree char *name
            = sanitize_name (json_object_get_string_member (node_j, "name"));
          g3k_model_set_name (node, name);
        }

      g_ptr_array_add (priv->nodes, g_steal_pointer (&node));
    }

  /* Then apply the hierarchy */
  for (i = 0; i < len; i++)
    {
      JsonObject *node_j = json_array_get_object_element (nodes_j, i);
      G3kModel   *parent = g_ptr_array_index (priv->nodes, i);

      if (json_object_has_member (node_j, "children"))
        {
          JsonArray *children = json_object_get_array_member (node_j,
                                                              "children");
          int        children_len = (int) json_array_get_length (children);
          int        j;
          for (j = 0; j < children_len; j++)
            {
              gint64 index = json_array_get_int_element (children, (guint) j);
              G3kModel *child = g_ptr_array_index (priv->nodes, index);

              g3k_object_add_child (G3K_OBJECT (parent), G3K_OBJECT (child), 0);
            }
        }
    }

  /* Associate mesh */
  for (i = 0; i < len; i++)
    {
      JsonObject *node_j = json_array_get_object_element (nodes_j, i);
      G3kModel   *node = g_ptr_array_index (priv->nodes, i);

      if (json_object_has_member (node_j, "mesh"))
        {
          gint64   mesh_id = json_object_get_int_member (node_j, "mesh");
          G3kMesh *mesh = G3K_MESH (g_ptr_array_index (priv->meshes, mesh_id));

          g3k_model_set_mesh (node, mesh);

          g3k_model_initialize (node, priv->pipelines);
        }
    }

  return TRUE;
}

static gboolean
parse_scenes (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *scenes_j = NULL;

  if (!json_object_has_member (root, "scenes"))
    return TRUE;

  scenes_j = json_object_get_array_member (root, "scenes");

  JsonObject *scene_j = json_array_get_object_element (scenes_j, 0);

  if (json_object_has_member (scene_j, "nodes"))
    {
      JsonArray *children = json_object_get_array_member (scene_j, "nodes");
      int        children_len = (int) json_array_get_length (children);
      int        j;

      for (j = 0; j < children_len; j++)
        {
          gint64    index = json_array_get_int_element (children, (guint) j);
          G3kModel *child = g_ptr_array_index (priv->nodes, index);
          g_ptr_array_add (priv->root_nodes, g_object_ref (child));
        }
    }

  return TRUE;
}

static GulkanTexture *
parse_texture_ref (G3kLoader *loader, JsonObject *texture_def)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  gint64            index = json_object_get_int_member (texture_def, "index");

  return g_ptr_array_index (priv->textures, index);
}

static void
parse_color (JsonArray *color_j, graphene_vec3_t *c, float *alpha_out)
{
  float red, green, blue, alpha;

  red = (float) json_array_get_double_element (color_j, 0);
  green = (float) json_array_get_double_element (color_j, 1);
  blue = (float) json_array_get_double_element (color_j, 2);
  if (json_array_get_length (color_j) > 3)
    alpha = (float) json_array_get_double_element (color_j, 3);
  else
    alpha = 1.0;

  if (alpha_out)
    *alpha_out = alpha;
  graphene_vec3_init (c, red, green, blue);
}

static gboolean
parse_materials (G3kLoader *loader, JsonObject *root, GError **error)
{
  (void) error;

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  JsonArray        *materials_j = NULL;
  guint             len;
  guint             i;

  if (!json_object_has_member (root, "materials"))
    return TRUE;

  materials_j = json_object_get_array_member (root, "materials");
  len = json_array_get_length (materials_j);
  for (i = 0; i < len; i++)
    {
      JsonObject *material_j = json_array_get_object_element (materials_j, i);
      g_autoptr (G3kMaterial) material = g3k_material_new (priv->context);
      JsonObject      *extensions = NULL;
      g_autofree char *name = NULL;

      if (json_object_has_member (material_j, "name"))
        name = g_strdup (json_object_get_string_member (material_j, "name"));
      else
        name = g_strdup_printf ("material_%d", i);

      if (json_object_has_member (material_j, "extensions"))
        extensions = json_object_get_object_member (material_j, "extensions");

      if (extensions
          && json_object_has_member (extensions,
                                     "KHR_materials_pbrSpecularGlossiness"))
        {
          JsonObject *specglos
            = json_object_get_object_member (extensions,
                                             "KHR_materials_"
                                             "pbrSpecularGlossiness");

          if (json_object_has_member (specglos, "diffuseTexture"))
            {
              JsonObject *texture_j
                = json_object_get_object_member (specglos, "diffuseTexture");
              GulkanTexture *texture = parse_texture_ref (loader, texture_j);
              g3k_material_add_texture (material, "DIFFUSE", texture);
            }
        }
      else if (json_object_has_member (material_j, "pbrMetallicRoughness"))
        {
          JsonObject *pbr
            = json_object_get_object_member (material_j,
                                             "pbrMetallicRoughness");

          material = g3k_material_new (priv->context);

          double metallic_factor = 0.5, roughness_factor = 0.5;

          if (json_object_has_member (pbr, "baseColorFactor"))
            parse_color (json_object_get_array_member (pbr, "baseColorFactor"),
                         &material->color, &material->color_alpha);

          if (json_object_has_member (pbr, "metallicFactor"))
            metallic_factor = json_object_get_double_member (pbr,
                                                             "metallicFactor");
          material->metalness = (float) metallic_factor;

          if (json_object_has_member (pbr, "roughnessFactor"))
            roughness_factor
              = json_object_get_double_member (pbr, "roughnessFactor");
          material->roughness = (float) roughness_factor;

          if (json_object_has_member (pbr, "baseColorTexture"))
            {
              JsonObject *texture_j
                = json_object_get_object_member (pbr, "baseColorTexture");
              GulkanTexture *texture = parse_texture_ref (loader, texture_j);
              g3k_material_add_texture (material, "DIFFUSE", texture);
            }

          if (json_object_has_member (pbr, "metallicRoughnessTexture"))
            {
              JsonObject *texture_j
                = json_object_get_object_member (pbr,
                                                 "metallicRoughnessTexture");
              GulkanTexture *texture = parse_texture_ref (loader, texture_j);
              g3k_material_add_texture (material, "METALLIC_ROUGHNESS",
                                        texture);
            }
        }

      if (json_object_has_member (material_j, "normalTexture"))
        {
          JsonObject *texture_j
            = json_object_get_object_member (material_j, "normalTexture");
          GulkanTexture *texture = parse_texture_ref (loader, texture_j);
          float          scale = 1.0;

          g3k_material_add_texture (material, "NORMAL", texture);

          if (json_object_has_member (texture_j, "scale"))
            scale = (float) json_object_get_double_member (texture_j, "scale");
          graphene_vec2_init (&material->normal_scale, scale, scale);
        }

      if (json_object_has_member (material_j, "occlusionTexture"))
        {
          JsonObject *texture_j
            = json_object_get_object_member (material_j, "occlusionTexture");
          GulkanTexture *texture = parse_texture_ref (loader, texture_j);
          double         intensity = 1.0;

          g3k_material_add_texture (material, "AO", texture);

          if (json_object_get_member (texture_j, "strength"))
            intensity = json_object_get_double_member (texture_j, "strength");
          material->ao_map_intensity = (float) intensity;
        }

      if (json_object_has_member (material_j, "emissiveFactor"))
        {
          parse_color (json_object_get_array_member (material_j,
                                                     "emissiveFactor"),
                       &material->emissive, NULL);
        }

      if (json_object_has_member (material_j, "emissiveTexture"))
        {
          JsonObject *texture_j
            = json_object_get_object_member (material_j, "emissiveTexture");
          GulkanTexture *texture = parse_texture_ref (loader, texture_j);
          g3k_material_add_texture (material, "EMISSIVE", texture);
        }

      if (json_object_has_member (material_j, "doubleSided")
          && json_object_get_boolean_member (material_j, "doubleSided"))
        material->double_sided = TRUE;

      const char *alpha_mode = "OPAQUE";
      if (json_object_has_member (material_j, "alphaMode"))
        alpha_mode = json_object_get_string_member (material_j, "alphaMode");

      if (g_strcmp0 (alpha_mode, "BLEND") == 0)
        {
          material->transparent = TRUE;
        }
      else
        {
          material->transparent = FALSE;
          if (g_strcmp0 (alpha_mode, "MASK") == 0)
            {
              double alpha_test = 0.5;
              if (json_object_has_member (material_j, "alphaCutoff"))
                alpha_test = json_object_get_double_member (material_j,
                                                            "alphaCutoff");
              material->alpha_test = (float) alpha_test;
            }
        }

      if (!material)
        {
          // material = g_object_ref (priv->default_material);
        }
      else
        {
          material->name = g_strdup (name);
        }

      g_ptr_array_add (priv->materials, g_steal_pointer (&material));
    }

  return TRUE;
}

static gboolean
decode_is_glb_header (GBytes *data, guint32 *version, guint32 *length)
{
  gsize         data_len;
  gconstpointer data_ptr = g_bytes_get_data (data, &data_len);

  if (data_len < 12)
    return FALSE;

  const guint8 *uint8_ptr = (const guint8 *) data_ptr;

  // clang-format off
  if (uint8_ptr[0] != 'g' ||
      uint8_ptr[1] != 'l' ||
      uint8_ptr[2] != 'T' ||
      uint8_ptr[3] != 'F')
    return FALSE;
  // clang-format on

  const guint32 *uint32_ptr = (const guint32 *) data_ptr;
  *version = GUINT32_FROM_LE (uint32_ptr[1]);
  *length = GUINT32_FROM_LE (uint32_ptr[2]);

  return TRUE;
}

static gboolean
get_glb_chunks (GBytes  *data,
                GBytes **json_out,
                GBytes **bin_out,
                GError **error)
{
  gsize          data_len;
  const guint8  *data_ptr = g_bytes_get_data (data, &data_len);
  const guint32 *data_ptr32;
  guint32        chunk_length, chunk_type, bin_chunk_offset;
  const guint32  json_chunk_offset = 12;
  g_autoptr (GBytes) json = NULL;
  g_autoptr (GBytes) bin = NULL;

  if (data_len < 12 + 8)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "Short file read finding json chunk");
      return FALSE;
    }

  data_ptr32 = (guint32 *) ((void *) (data_ptr + json_chunk_offset));
  chunk_length = GUINT32_FROM_LE (data_ptr32[0]);
  chunk_type = GUINT32_FROM_LE (data_ptr32[1]);

  if (chunk_type != 0x4E4F534A)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "First GLB chunk is not json");
      return FALSE;
    }

  if (data_len < json_chunk_offset + 8 + chunk_length)
    {
      g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                   "Short file read in json chunk");
      return FALSE;
    }

  json = g_bytes_new_from_bytes (data, json_chunk_offset + 8, chunk_length);

  bin_chunk_offset = json_chunk_offset + 8 + chunk_length;

  if (data_len > bin_chunk_offset)
    {
      if (data_len < bin_chunk_offset + 8)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Short file read finding binary chunk");
          return FALSE;
        }

      data_ptr32 = (guint32 *) ((void *) (data_ptr + bin_chunk_offset));
      chunk_length = GUINT32_FROM_LE (data_ptr32[0]);
      chunk_type = GUINT32_FROM_LE (data_ptr32[1]);

      if (data_len < bin_chunk_offset + 8 + chunk_length)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Short file read in bin chunk");
          return FALSE;
        }

      bin = g_bytes_new_from_bytes (data, bin_chunk_offset + 8, chunk_length);
    }

  *json_out = g_steal_pointer (&json);
  *bin_out = g_steal_pointer (&bin);
  return TRUE;
}

G3kObject *
g3k_loader_get_root (G3kLoader *self)
{
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (self);
  return priv->root;
}

// possible memory leak in return value,
// but that's how they did in gthree as well :p
G3kLoader *
g3k_loader_new_from_file (G3kContext *context,
                          const char *file_path,
                          GError    **error)
{
  GFile *file = g_file_new_for_path (file_path);
  GFile *base_path = g_file_get_parent (file);

  char *char_data;
  gsize size;
  g_autoptr (GBytes) data = NULL;

  g_file_get_contents (file_path, &char_data, &size, error);
  if (*error != NULL)
    return NULL;

  data = g_bytes_new_take (char_data, size);
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (JsonNode) root_node = NULL;
  JsonObject *root;
  g_autoptr (G3kLoader) loader = NULL;
  guint32 glb_version;
  guint32 json_length;
  g_autoptr (GBytes) json = NULL;
  g_autoptr (GBytes) bin = NULL;

  if (decode_is_glb_header (data, &glb_version, &json_length))
    {
      if (glb_version != 2)
        {
          g_set_error (error, G3K_LOADER_ERROR, G3K_LOADER_ERROR_FAIL,
                       "Unsupported glTL GLB version %d", glb_version);
          return NULL;
        }

      if (!get_glb_chunks (data, &json, &bin, error))
        return NULL;
    }
  else /* Assume json format */
    {
      json = g_bytes_ref (data);
    }

  parser = json_parser_new ();

  if (!json_parser_load_from_data (parser, g_bytes_get_data (json, NULL),
                                   (gssize) g_bytes_get_size (json), error))
    return NULL;

  root_node = json_parser_steal_root (parser);
  root = json_node_get_object (root_node);

  loader = g_object_new (g3k_loader_get_type (), NULL);

  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);
  priv->default_material = g3k_material_new_default (context);
  priv->context = g_object_ref (context);
  priv->root = g3k_object_new ();

  // only supports PBR materials
  if (!parse_extensions_required (root))
    return NULL;

  if (!parse_extensions_used (root))
    return NULL;

  if (!parse_asset (loader, root, error))
    return NULL;

  if (!parse_buffers (loader, root, bin, base_path, error))
    return NULL;

  if (!parse_buffer_views (loader, root, error))
    return NULL;

  if (!parse_accessors (loader, root, error))
    return NULL;

  if (!parse_samplers (loader, root, error))
    return NULL;

  if (!parse_images (loader, root, base_path, error))
    return NULL;

  if (!parse_textures (loader, root, error))
    return NULL;

  // only loads diffuse textures
  if (!parse_materials (loader, root, error))
    return NULL;

  // loads only POSITION, NORMAL, TEXCOORD_0, INDEX
  if (!parse_meshes (loader, root, error))
    return NULL;

  if (!parse_nodes (loader, root, error))
    return NULL;

  if (!parse_scenes (loader, root, error))
    return NULL;

  for (guint i = 0; i < priv->root_nodes->len; i++)
    {
      G3kModel *model = G3K_MODEL (g_ptr_array_index (priv->root_nodes, i));
      g3k_object_add_child (priv->root, G3K_OBJECT (model), 0);
    }

  return g_steal_pointer (&loader);
}

static void
g3k_loader_finalize (GObject *obj)
{
  G3kLoader        *loader = G3K_LOADER (obj);
  G3kLoaderPrivate *priv = g3k_loader_get_instance_private (loader);

  g_ptr_array_unref (priv->buffers);
  g_ptr_array_unref (priv->buffer_views);
  g_ptr_array_unref (priv->images);
  g_ptr_array_unref (priv->accessors);
  g_ptr_array_unref (priv->nodes);
  g_ptr_array_unref (priv->meshes);
  g_ptr_array_unref (priv->scenes);
  g_ptr_array_unref (priv->samplers);
  g_ptr_array_unref (priv->textures);
  g_ptr_array_unref (priv->materials);
  g_ptr_array_unref (priv->root_nodes);

  g_object_unref (priv->default_material);

  g_hash_table_destroy (priv->pipelines);

  g_object_unref (priv->root);
  g_object_unref (priv->context);

  G_OBJECT_CLASS (g3k_loader_parent_class)->finalize (obj);
}
