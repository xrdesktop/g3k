/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef G3K_OBJECT_MANAGER_H_
#define G3K_OBJECT_MANAGER_H_

#if !defined(G3K_INSIDE) && !defined(G3K_COMPILATION)
#error "Only <g3k.h> can be included directly."
#endif

#include "g3k-container.h"
#include "g3k-controller.h"

G_BEGIN_DECLS

#define G3K_TYPE_OBJECT_MANAGER g3k_object_manager_get_type ()
G_DECLARE_FINAL_TYPE (G3kObjectManager,
                      g3k_object_manager,
                      G3K,
                      OBJECT_MANAGER,
                      GObject)

/**
 * G3kNoHoverEvent:
 * @controller: A #GxrController.
 *
 * An event that gets emitted when a window is not being hovered anymore.
 **/
typedef struct
{
  G3kController *controller;
} G3kNoHoverEvent;

/**
 * G3kTransformTransition:
 * @window: A #XrdWindow.
 * @from: The initial #graphene_matrix_t of the transiton.
 * @to: The final #graphene_matrix_t of the transition.
 * @from_scaling: The initial scale of the transition.
 * @to_scaling: The final scale of the transition.
 * @interpolate: The current state in the range [0-1] of the transition.
 * @last_timestamp: The last timestamp the transition was updated.
 *
 * A transition between two #XrdWindow transformation states.
 **/
typedef struct
{
  G3kObject        *object;
  graphene_matrix_t from;
  graphene_matrix_t to;
  float             interpolate;
  gint64            last_timestamp;
} G3kTransformTransition;

/**
 * G3kInteractionFlags:
 * @XRD_WINDOW_HOVERABLE: Set if hover events should be generated.
 * @XRD_WINDOW_DRAGGABLE: Set if the window should be draggable.
 * @XRD_WINDOW_MANAGED: Set if window should be manipulated by window manager
 *auto alignment.
 * @XRD_WINDOW_DESTROY_WITH_PARENT: Set if window should be destroyed with the
 *window manager.
 * @XRD_WINDOW_BUTTON: Set if window is a button.
 *
 * Flags for the window manager.
 *
 **/

// clang-format off
typedef enum
{
  G3K_INTERACTION_HOVERABLE           = 1 << 0,
  G3K_INTERACTION_DRAGGABLE           = 1 << 1,
  G3K_INTERACTION_MANAGED             = 1 << 2,
  G3K_INTERACTION_DESTROY_WITH_PARENT = 1 << 3,
  G3K_INTERACTION_BUTTON              = 1 << 4,
} G3kInteractionFlags;
// clang-format on

/**
 * G3kHoverMode:
 * @G3K_HOVER_MODE_EVERYTHING: Buttons and windows should receive events.
 * @G3K_HOVER_MODE_BUTTONS: Only buttons should receive events.
 *
 * A mode where input events can be ignored for certain widgets.
 *
 **/
typedef enum
{
  G3K_HOVER_MODE_EVERYTHING,
  G3K_HOVER_MODE_BUTTONS
} G3kHoverMode;

G3kObjectManager *
g3k_object_manager_new (G3kContext *context);

void
g3k_object_manager_arrange_reset (G3kObjectManager *self);

gboolean
g3k_object_manager_arrange_sphere (G3kObjectManager *self);

void
g3k_object_manager_add_container (G3kObjectManager *self,
                                  G3kContainer     *container);

void
g3k_object_manager_remove_container (G3kObjectManager *self,
                                     G3kContainer     *container);

void
g3k_object_manager_add_object (G3kObjectManager   *self,
                               G3kObject          *object,
                               G3kInteractionFlags flags);

void
g3k_object_manager_remove_object (G3kObjectManager *self, G3kObject *object);

void
g3k_object_manager_drag_start (G3kObjectManager *self,
                               G3kController    *controller);

void
g3k_object_manager_update_controller (G3kObjectManager *self,
                                      G3kController    *controller);

void
g3k_object_manager_step_containers (G3kObjectManager *self);

GSList *
g3k_object_manager_get_objects (G3kObjectManager *self);

GSList *
g3k_object_manager_get_buttons (G3kObjectManager *self);

void
g3k_object_manager_set_hover_mode (G3kObjectManager *self, G3kHoverMode mode);

G3kHoverMode
g3k_object_manager_get_hover_mode (G3kObjectManager *self);

void
g3k_object_manager_save_reset_transformation (G3kObjectManager *self,
                                              G3kObject        *object);

G_END_DECLS

#endif /* G3K_OBJECT_MANAGER_H_ */
