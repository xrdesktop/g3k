/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-object-manager.h"

#include <gdk/gdk.h>
#include <graphene-ext.h>

#include "g3k-math.h"
#include "g3k-object-priv.h"
#include "g3k-settings.h"

struct _G3kObjectManager
{
  GObject parent;

  G3kContext *context;

  GSList *draggable_objects;
  GSList *managed_objects;
  GSList *hoverable_objects;
  GSList *destroy_objects;
  GSList *containers;

  /* all objects except G3K_INTERACTION_BUTTON */
  GSList *all_objects;

  /* G3K_INTERACTION_BUTTON */
  GSList *buttons;

  gboolean controls_shown;

  // G3kObject -> graphene_matrix_t
  GHashTable *reset_transforms;

  G3kHoverMode hover_mode;
};

G_DEFINE_TYPE (G3kObjectManager, g3k_object_manager, G_TYPE_OBJECT)

enum
{
  NO_HOVER_EVENT,
  LAST_SIGNAL
};
static guint manager_signals[LAST_SIGNAL] = {0};

static void
_finalize (GObject *gobject);

static void
g3k_object_manager_class_init (G3kObjectManagerClass *klass)
{
  manager_signals[NO_HOVER_EVENT]
    = g_signal_new ("no-hover-event", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;
}

static void
_free_matrix (gpointer data)
{
  graphene_matrix_free (data);
}

static void
g3k_object_manager_init (G3kObjectManager *self)
{
  self->all_objects = NULL;
  self->buttons = NULL;
  self->draggable_objects = NULL;
  self->managed_objects = NULL;
  self->destroy_objects = NULL;
  self->hoverable_objects = NULL;
  self->hover_mode = G3K_HOVER_MODE_EVERYTHING;

  /* TODO: possible steamvr issue: When input poll rate is high and buttons are
   * immediately hidden after creation, they may not reappear on show().
   * For, show buttons when starting overlay client to avoid this issue . */
  self->controls_shown = TRUE;

  self->reset_transforms = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                                  NULL, _free_matrix);
}

G3kObjectManager *
g3k_object_manager_new (G3kContext *context)
{
  G3kObjectManager *self = (G3kObjectManager *)
    g_object_new (G3K_TYPE_OBJECT_MANAGER, 0);
  self->context = context;
  return self;
}

static void
_finalize (GObject *gobject)
{
  G3kObjectManager *self = G3K_OBJECT_MANAGER (gobject);

  /* remove the window manager's reference to all windows */
  g_slist_free_full (self->all_objects, g_object_unref);
  g_slist_free_full (self->buttons, g_object_unref);

  g_slist_free (self->hoverable_objects);
  g_slist_free (self->containers);
  g_slist_free (self->draggable_objects);
  g_slist_free (self->managed_objects);

  g_hash_table_unref (self->reset_transforms);

  g_slist_free_full (self->destroy_objects, g_object_unref);
}

/**
 * g3k_object_manager_save_reset_transformation:
 * @self: The #G3kObjectManager
 * Saves the current transformation as the reset transformation.
 */
void
g3k_object_manager_save_reset_transformation (G3kObjectManager *self,
                                              G3kObject        *object)
{
  graphene_matrix_t *g = graphene_matrix_alloc ();
  g3k_object_get_matrix (object, g);
  g_hash_table_insert (self->reset_transforms, object, g);
}

static gboolean
_interpolate_cb (gpointer _transition)
{
  G3kTransformTransition *transition = (G3kTransformTransition *) _transition;

  G3kObject *object = transition->object;

  float curve = -(float) pow ((double) transition->interpolate - 1.0, 4) + 1;

  graphene_matrix_t interpolated;
  graphene_ext_matrix_interpolate_simple (&transition->from, &transition->to,
                                          curve, &interpolated);

  g3k_object_set_matrix (object, &interpolated);

  gint64 now = g_get_monotonic_time ();
  float  ms_since_last = (float) (now - transition->last_timestamp) / 1000.f;
  transition->last_timestamp = now;

  /* in seconds */
  const float transition_duration = 0.75;

  transition->interpolate += ms_since_last / 1000.f / transition_duration;

  if (transition->interpolate > 1)
    {
      g3k_object_set_matrix (object, &transition->to);

      g_object_unref (transition->object);
      g_free (transition);

      return FALSE;
    }

  return TRUE;
}

static gboolean
_is_in_list (GSList *list, G3kObject *object)
{
  GSList *l;
  for (l = list; l != NULL; l = l->next)
    {
      if (l->data == object)
        return TRUE;
    }
  return FALSE;
}

void
g3k_object_manager_arrange_reset (G3kObjectManager *self)
{
  GSList *l;
  for (l = self->managed_objects; l != NULL; l = l->next)
    {
      G3kObject *object = (G3kObject *) l->data;

      G3kTransformTransition *transition = g_malloc (sizeof *transition);
      transition->last_timestamp = g_get_monotonic_time ();

      g3k_object_get_matrix (object, &transition->from);

      graphene_matrix_t *reset_transform
        = g_hash_table_lookup (self->reset_transforms, object);

      if (reset_transform != NULL
          && !graphene_matrix_equal (&transition->from, reset_transform))
        {
          transition->interpolate = 0;
          transition->object = object;
          g_object_ref (object);

          graphene_matrix_init_from_matrix (&transition->to, reset_transform);

          g_timeout_add (20, _interpolate_cb, transition);
        }
      else
        {
          g_free (transition);
        }
    }
}

static float
_azimuth_from_pose (graphene_matrix_t *mat)
{
  graphene_matrix_t rotation_matrix;

  graphene_point3d_t unused_scale;
  graphene_ext_matrix_get_rotation_matrix (mat, &unused_scale,
                                           &rotation_matrix);

  graphene_vec3_t start;
  graphene_vec3_init (&start, 0, 0, -1);
  graphene_vec3_t direction;
  graphene_matrix_transform_vec3 (&rotation_matrix, &start, &direction);

  return atan2f (graphene_vec3_get_x (&direction),
                 -graphene_vec3_get_z (&direction));
}

gboolean
g3k_object_manager_arrange_sphere (G3kObjectManager *self)
{
  guint num_overlays = g_slist_length (self->managed_objects);

  double root_num_overlays = sqrt ((double) num_overlays);

  uint32_t grid_height = (uint32_t) root_num_overlays;
  uint32_t grid_width = (uint32_t) ((float) num_overlays / (float) grid_height);

  while (grid_width * grid_height < num_overlays)
    grid_width++;

  GxrContext       *gxr = g3k_context_get_gxr (self->context);
  graphene_matrix_t hmd_pose;
  if (!gxr_context_get_head_pose (gxr, &hmd_pose))
    {
      g_printerr ("Could not get head pose./n");
      return FALSE;
    }

  graphene_vec3_t hmd_vec;
  graphene_ext_matrix_get_translation_vec3 (&hmd_pose, &hmd_vec);

  graphene_vec3_t hmd_vec_neg;
  graphene_vec3_negate (&hmd_vec, &hmd_vec_neg);

  float theta_fov = (float) M_PI / 2.5f;
  float theta_center = (float) M_PI / 2.0f;
  float theta_start = theta_center + theta_fov / 2.0f;
  float theta_end = theta_center - theta_fov / 2.0f;
  float theta_range = fabsf (theta_end - theta_start);
  float theta_step = theta_range / (float) (grid_height - 1);

  float phi_fov = (float) M_PI / 2.5f;
  float phi_center = -(float) M_PI / 2.0f + _azimuth_from_pose (&hmd_pose);
  float phi_start = phi_center - phi_fov / 2.0f;
  float phi_end = phi_center + phi_fov / 2.0f;
  float phi_range = fabsf (phi_end - phi_start);
  float phi_step = phi_range / (float) (grid_width - 1);

  float radius = 5.0f;

  guint i = 0;
  for (float theta = theta_start; theta > theta_end - 0.01f;
       theta -= theta_step)
    {
      for (float phi = phi_start; phi < phi_end + 0.01f; phi += phi_step)
        {
          G3kTransformTransition *transition = g_malloc (sizeof *transition);
          transition->last_timestamp = g_get_monotonic_time ();

          float const x = sinf (theta) * cosf (phi);
          float const y = cosf (theta);
          float const z = sinf (phi) * sinf (theta);

          graphene_matrix_t transform;

          graphene_vec3_t position;
          graphene_vec3_init (&position, x * radius, y * radius, z * radius);

          graphene_vec3_add (&position, &hmd_vec, &position);

          graphene_vec3_negate (&position, &position);

          graphene_matrix_init_look_at (&transform, &position, &hmd_vec_neg,
                                        graphene_vec3_y_axis ());

          G3kObject *object = (G3kObject *)
            g_slist_nth_data (self->managed_objects, i);

          if (object == NULL)
            {
              g_printerr ("Object %d does not exist!\n", i);
              return FALSE;
            }

          g3k_object_get_matrix (object, &transition->from);

          if (!graphene_matrix_equal (&transition->from, &transform))
            {
              transition->interpolate = 0;
              transition->object = object;
              g_object_ref (object);

              graphene_matrix_init_from_matrix (&transition->to, &transform);

              g_timeout_add (20, _interpolate_cb, transition);
            }
          else
            {
              g_free (transition);
            }

          i++;
          if (i > num_overlays)
            return TRUE;
        }
    }

  return TRUE;
}

void
g3k_object_manager_add_container (G3kObjectManager *self,
                                  G3kContainer     *container)
{
  self->containers = g_slist_append (self->containers, container);
}

void
g3k_object_manager_remove_container (G3kObjectManager *self,
                                     G3kContainer     *container)
{
  self->containers = g_slist_remove (self->containers, container);
}

void
g3k_object_manager_add_object (G3kObjectManager   *self,
                               G3kObject          *object,
                               G3kInteractionFlags flags)
{
  /* any window must be either in all_objects or buttons */
  if (flags & G3K_INTERACTION_BUTTON)
    {
      self->buttons = g_slist_append (self->buttons, object);
      if (!self->controls_shown)
        g3k_object_set_visibility (object, FALSE);
    }
  else
    {
      self->all_objects = g_slist_append (self->all_objects, object);
    }

  /* Freed with manager */
  if (flags & G3K_INTERACTION_DESTROY_WITH_PARENT)
    self->destroy_objects = g_slist_append (self->destroy_objects, object);

  /* Movable overlays (user can move them) */
  if (flags & G3K_INTERACTION_DRAGGABLE)
    self->draggable_objects = g_slist_append (self->draggable_objects, object);

  /* Managed overlays (window manager can move them) */
  if (flags & G3K_INTERACTION_MANAGED)
    self->managed_objects = g_slist_append (self->managed_objects, object);

  /* All windows that can be hovered, includes button windows */
  if (flags & G3K_INTERACTION_HOVERABLE)
    self->hoverable_objects = g_slist_append (self->hoverable_objects, object);

  /* keep the window referenced as long as the window manages this window */
  g_object_ref (object);
}

void
g3k_object_manager_step_containers (G3kObjectManager *self)
{
  for (GSList *l = self->containers; l != NULL; l = l->next)
    {
      G3kContainer *wc = (G3kContainer *) l->data;
      g3k_container_step (wc);
    }
}

void
g3k_object_manager_remove_object (G3kObjectManager *self, G3kObject *object)
{
  self->all_objects = g_slist_remove (self->all_objects, object);
  self->buttons = g_slist_remove (self->buttons, object);
  self->destroy_objects = g_slist_remove (self->destroy_objects, object);
  self->draggable_objects = g_slist_remove (self->draggable_objects, object);
  self->managed_objects = g_slist_remove (self->managed_objects, object);
  self->hoverable_objects = g_slist_remove (self->hoverable_objects, object);

  for (GSList *l = self->containers; l != NULL; l = l->next)
    {
      G3kContainer *wc = (G3kContainer *) l->data;
      g3k_container_remove_child (wc, object);
    }

  /* remove the window manager's reference to the window */
  g_object_unref (object);
}

static G3kObject *
_get_hover_target (G3kObjectManager   *self,
                   G3kController      *controller,
                   graphene_point3d_t *intersection_point,
                   float              *intersection_distance)
{
  G3kObject         *closest_obj = NULL;
  float              closest_distance = FLT_MAX;
  graphene_point3d_t closest_point;

  for (GSList *l = self->hoverable_objects; l != NULL; l = l->next)
    {
      G3kObject *object = (G3kObject *) l->data;

      if (!g3k_object_is_visible (object))
        continue;

      if (self->hover_mode == G3K_HOVER_MODE_BUTTONS)
        if (g_slist_find (self->buttons, object) == NULL)
          continue;

      graphene_point3d_t point;
      float              distance;
      gboolean intersects = g3k_controller_get_intersection (controller, object,
                                                             &distance, &point);

      if (intersects && distance < closest_distance)
        {
          closest_obj = object;
          closest_distance = distance;
          graphene_point3d_init_from_point (&closest_point, &point);
        }
    }

  *intersection_distance = closest_distance;
  graphene_point3d_init_from_point (intersection_point, &closest_point);
  return closest_obj;
}

static void
_emit_hover (G3kObjectManager   *self,
             G3kController      *controller,
             G3kObject          *last_object,
             G3kObject          *object,
             graphene_point3d_t *intersection_point,
             float               intersection_distance)
{
  if (object)
    {
      /* Emit hover event every time when hovering a window */
      G3kHoverEvent hover_event = {
        .distance = intersection_distance,
        .controller = controller,
      };
      graphene_point3d_init_from_point (&hover_event.point, intersection_point);
      g3k_object_emit_hover (object, &hover_event);

      /* Emit hover start event when starting to hover a new window */
      if (object != last_object)
        g3k_object_emit_hover_start (object, controller);

      /* Emit hover end event when hovering over a new window */
      if (last_object && object != last_object)
        g3k_object_emit_hover_end (last_object, controller);
    }
  else
    {
      /* Emit no hover event every time when hovering nothing */
      G3kNoHoverEvent no_hover_event = {
        .controller = controller,
      };
      g_signal_emit (self, manager_signals[NO_HOVER_EVENT], 0, &no_hover_event);

      /* Emit hover end event only if we had hovered something earlier */
      if (last_object != NULL)
        {
          g3k_controller_reset_hover_state (controller);
          g3k_object_emit_hover_end (last_object, controller);
        }
    }
}

void
g3k_object_manager_drag_start (G3kObjectManager *self,
                               G3kController    *controller)
{
  G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);
  if (!_is_in_list (self->draggable_objects, hover_state->object))
    return;

  g3k_controller_drag_start (controller, hover_state->object);
}

void
g3k_object_manager_update_controller (G3kObjectManager *self,
                                      G3kController    *controller)
{
  /* Drag test */
  G3kGrabState *grab_state = g3k_controller_get_grab_state (controller);
  gpointer      grabbed_object = grab_state->object;

  if (grabbed_object != NULL)
    {
      g3k_controller_update_grab (controller);
    }
  else
    {
      G3kHoverState *hover_state = g3k_controller_get_hover_state (controller);
      G3kObject     *last_hover_target = hover_state->object;

      graphene_point3d_t intersection_point;
      float              intersection_distance;
      G3kObject         *hover_target = _get_hover_target (self, controller,
                                                           &intersection_point,
                                                           &intersection_distance);

      graphene_point_t intersection_2d;
      if (hover_target != NULL)
        {
          g3k_object_get_object_space_intersection (hover_target,
                                                    &intersection_point,
                                                    &intersection_2d);
        }

      g3k_controller_update_hovered_object (controller, last_hover_target,
                                            hover_target, &intersection_point,
                                            &intersection_2d,
                                            intersection_distance);

      _emit_hover (self, controller, last_hover_target, hover_target,
                   &intersection_point, intersection_distance);
    }
}

/**
 * g3k_object_manager_get_objects:
 * @self: The #G3kObjectManager
 *
 * Returns: (element-type G3kObject) (transfer none): A list of #G3kObject.
 */
GSList *
g3k_object_manager_get_objects (G3kObjectManager *self)
{
  return self->all_objects;
}

/**
 * g3k_object_manager_get_buttons:
 * @self: The #G3kObjectManager
 *
 * Returns: (element-type G3kObject) (transfer none): A list of #G3kObject.
 */
GSList *
g3k_object_manager_get_buttons (G3kObjectManager *self)
{
  return self->buttons;
}

void
g3k_object_manager_set_hover_mode (G3kObjectManager *self, G3kHoverMode mode)
{
  self->hover_mode = mode;
}

G3kHoverMode
g3k_object_manager_get_hover_mode (G3kObjectManager *self)
{
  return self->hover_mode;
}
