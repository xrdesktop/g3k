/*
 * g3k
 * Copyright 2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "g3k-keyboard-button.h"

#include "g3k-button-priv.h"

struct _G3kKeyboardButton
{
  GObject parent;

  gchar *target;
  gchar *character;
  guint  keyval;
};

G_DEFINE_TYPE (G3kKeyboardButton, g3k_keyboard_button, G3K_TYPE_BUTTON)

enum
{
  PROP_TARGET = 1,
  PROP_CHARACTER,
  PROP_KEYVAL,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = {
  NULL,
};

static void
_set_property (GObject      *object,
               guint         property_id,
               const GValue *value,
               GParamSpec   *pspec)
{
  G3kKeyboardButton *self = G3K_KEYBOARD_BUTTON (object);

  switch (property_id)
    {
      case PROP_TARGET:
        if (self->target)
          g_free (self->target);
        self->target = g_strdup (g_value_get_string (value));
        break;
      case PROP_CHARACTER:
        if (self->character)
          g_free (self->character);
        self->character = g_strdup (g_value_get_string (value));
        break;
      case PROP_KEYVAL:
        self->keyval = g_value_get_uint (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
_get_property (GObject    *object,
               guint       property_id,
               GValue     *value,
               GParamSpec *pspec)
{
  G3kKeyboardButton *self = G3K_KEYBOARD_BUTTON (object);

  switch (property_id)
    {
      case PROP_TARGET:
        g_value_set_string (value, self->target);
        break;
      case PROP_CHARACTER:
        g_value_set_string (value, self->character);
        break;
      case PROP_KEYVAL:
        g_value_set_uint (value, self->keyval);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
g3k_keyboard_button_init (G3kKeyboardButton *self)
{
  self->target = NULL;
  self->character = NULL;
}

G3kKeyboardButton *
g3k_keyboard_button_new (G3kContext *g3k, graphene_size_t *size_meters)
{
  G3kKeyboardButton *self = (G3kKeyboardButton *)
    g_object_new (G3K_TYPE_KEYBOARD_BUTTON, 0);

  if (!g3k_button_initialize (G3K_BUTTON (self), g3k, size_meters))
    {
      g_object_unref (self);
      g_printerr ("Could not create button.\n");
      return NULL;
    }

  return self;
}

static void
_finalize (GObject *gobject)
{
  G3kKeyboardButton *self = G3K_KEYBOARD_BUTTON (gobject);
  g_free (self->character);
  g_free (self->target);
  G_OBJECT_CLASS (g3k_keyboard_button_parent_class)->finalize (gobject);
}

static void
g3k_keyboard_button_class_init (G3kKeyboardButtonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
  object_class->set_property = _set_property;
  object_class->get_property = _get_property;

  properties[PROP_TARGET] = g_param_spec_string ("target", "Target",
                                                 "Target of the button.", NULL,
                                                 G_PARAM_READWRITE);

  properties[PROP_CHARACTER]
    = g_param_spec_string ("character", "Character",
                           "Character represented by the button.", NULL,
                           G_PARAM_READWRITE);

  properties[PROP_KEYVAL]
    = g_param_spec_uint ("keyval", "Keyval",
                         "Key value represented by the button.", 0, G_MAXUINT,
                         0, G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}
