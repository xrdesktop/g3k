/*
 * xrdesktop
 * Copyright 2019-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>

typedef struct Example
{
  G3kContext *g3k;
} Example;

static void
_cleanup (Example *self)
{
  g_object_unref (self->g3k);
}

static gboolean
_init_model (Example *self, const char *model_path)
{
  GError    *model_error = NULL;
  G3kLoader *loader = g3k_loader_new_from_file (self->g3k, model_path,
                                                &model_error);

  if (!loader || model_error != NULL)
    {
      g_warning ("Could not load GLTF model from %s: %s\n", model_path,
                 model_error->message);
      g_error_free (model_error);
      return FALSE;
    }

  g_object_unref (loader);
  return TRUE;
}

static void
print_usage (void)
{
  g_print ("Usage:\n");
  g_print ("./load_gltf_headless <model_path>\n");
}

int
main (int argc, char *argv[])
{
  if (argc != 2)
    {
      print_usage ();
      return EXIT_FAILURE;
    }

  char *model_path = argv[1];
  if (!g_file_test (model_path, G_FILE_TEST_IS_REGULAR))
    {
      g_warning ("%s is not a path to a valid file.\n", model_path);
      return EXIT_FAILURE;
    }

  g_print ("Loading model from %s\n", model_path);

  GSList *device_ext_list = NULL;
  device_ext_list
    = g_slist_append (device_ext_list,
                      VK_KHR_SAMPLER_MIRROR_CLAMP_TO_EDGE_EXTENSION_NAME);

  G3kContext *g3k = g3k_context_new_full (NULL, device_ext_list, "gltf_example",
                                          G3K_VERSION_HEX);

  Example self = {
    .g3k = g3k,
  };

  if (!_init_model (&self, model_path))
    return EXIT_FAILURE;

  _cleanup (&self);

  return EXIT_SUCCESS;
}
