/*
 * G3k
 * Copyright 2021-2022 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Remco Kranenburg <remco@burgsoft.nl>
 * SPDX-License-Identifier: MIT
 */

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib-unix.h>
#include <glib.h>
#include <locale.h>

#include <g3k.h>

#define G3K_TYPE_EXAMPLE g3k_example_get_type ()
G_DECLARE_FINAL_TYPE (G3kExample, g3k_example, G3K, EXAMPLE, GObject)

struct _G3kExample
{
  GObject parent;

  GxrActionSet *action_set;

  G3kContext *g3k;

  G3kBackground *background;

  GxrManifest *manifest;

  GMainLoop *loop;
  G3kButton *quit_button;
  G3kButton *keyboard_button;

  G3kKeyboard *keyboard;

  guint64 click_source;
};

G_DEFINE_TYPE (G3kExample, g3k_example, G_TYPE_OBJECT)

static void
_finalize (GObject *gobject);

static void
g3k_example_class_init (G3kExampleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static void
_finalize (GObject *gobject)
{
  G3kExample *self = G3K_EXAMPLE (gobject);

  g_object_unref (self->background);

  g_clear_object (&self->action_set);
  g_clear_object (&self->manifest);
  g_clear_object (&self->g3k);
  G_OBJECT_CLASS (g3k_example_parent_class)->finalize (gobject);
}

static gboolean
_is_hovered (G3kExample *self, G3kButton *button)
{
  g_autoptr (GList) controllers = g3k_context_get_controllers (self->g3k);
  for (GList *l = controllers; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);
      if (g3k_controller_get_hover_state (controller)->object
          == G3K_OBJECT (button))
        return TRUE;
    }
  return FALSE;
}

static void
_check_hover_grab (G3kExample *self)
{
  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g_autoptr (GList) controllers = g3k_context_get_controllers (self->g3k);
  for (GList *l = controllers; l; l = l->next)
    {
      G3kController *controller = G3K_CONTROLLER (l->data);

      /* TODO: handle invalid pointer poses better */
      if (!gxr_controller_is_pointer_pose_valid (
            g3k_controller_get_controller (controller)))
        continue;

      g3k_object_manager_update_controller (manager, controller);
    }
}

static void
_input_poll_cb (G3kContext *context, gpointer _self)
{
  (void) context;
  G3kExample *self = G3K_EXAMPLE (_self);

  if (self->action_set == NULL)
    {
      g_printerr ("Error: Action Set not created!\n");
      return;
    }

  if (!gxr_action_sets_poll (&self->action_set, 1))
    {
      g_printerr ("Error polling actions\n");
      return;
    }

  _check_hover_grab (self);
}

static void
_action_grab_cb (GxrAction *action, GxrDigitalEvent *event, G3kExample *self)
{
  (void) action;
  G3kController *controller = g3k_context_get_controller (self->g3k,
                                                          event->controller);

  if (event->changed)
    {
      if (event->state == 1)
        g3k_controller_check_grab (controller);
      else
        g3k_controller_check_release (controller);
    }
}

static void
_button_hover_cb (G3kButton *button, G3kHoverEvent *event, gpointer _self)
{
  (void) _self;
  (void) event;
  g3k_button_set_selection_color (button, TRUE);
}

static void
_button_hover_end_cb (G3kButton     *button,
                      GxrController *controller,
                      gpointer       _self)
{
  (void) controller;
  G3kExample *self = G3K_EXAMPLE (_self);
  // G3kExamplePrivate *priv = g3k_example_get_instance_private (self);

  /* unmark if no controller is hovering over this button */
  if (!_is_hovered (self, button))
    g3k_button_reset_color (button);
}

static void
g3k_example_init (G3kExample *self)
{
  self->action_set = NULL;
  self->g3k = NULL;
  self->background = NULL;
  self->loop = g_main_loop_new (NULL, FALSE);
  self->manifest = NULL;
}

static void
_shutdown_cb (GxrContext *context, gpointer _self)
{
  (void) context;
  G3kExample *self = G3K_EXAMPLE (_self);
  g_main_loop_quit (self->loop);
}

static GxrActionSet *
_create_wm_action_set (G3kExample *self)
{
  GxrActionSet *set
    = gxr_action_set_new_from_url (g3k_context_get_gxr (self->g3k),
                                   self->manifest, "/actions/wm");

  GxrDeviceManager *dm
    = gxr_context_get_device_manager (g3k_context_get_gxr (self->g3k));
  gxr_device_manager_connect_pose_actions (dm, set, "/actions/wm/in/hand_pose",
                                           "/actions/wm/in/"
                                           "hand_pose_hand_grip");
  gxr_action_set_connect_digital_from_float (set, "/actions/wm/in/grab_window",
                                             0.25f, "/actions/wm/out/haptic",
                                             (GCallback) _action_grab_cb, self);
  return set;
}

static void
_init_input_callbacks (G3kExample *self)
{
  self->manifest = gxr_manifest_new ("/res/bindings", "wm_actions.json");
  if (!self->manifest)
    {
      g_print ("Failed to load action bindings!\n");
      return;
    }

  self->action_set = _create_wm_action_set (self);

  gxr_context_attach_action_sets (g3k_context_get_gxr (self->g3k),
                                  &self->action_set, 1);

  g_signal_connect (self->g3k, "input-poll", G_CALLBACK (_input_poll_cb), self);
  g_signal_connect (self->g3k, "shutdown", G_CALLBACK (_shutdown_cb), self);
}

static gboolean
_sigint_cb (gpointer _self)
{
  G3kExample *self = (G3kExample *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static void
_button_quit_press_cb (G3kObject     *object,
                       GxrController *controller,
                       gpointer       _self)
{
  (void) controller;
  (void) object;
  G3kExample *self = _self;
  g_main_loop_quit (self->loop);
}

static void
_button_keyboard_press_cb (G3kObject     *object,
                           GxrController *controller,
                           gpointer       _self)
{
  (void) controller;
  (void) object;
  G3kExample *self = G3K_EXAMPLE (_self);

  if (g3k_keyboard_is_visible (self->keyboard))
    g3k_keyboard_hide (self->keyboard);
  else
    g3k_keyboard_show (self->keyboard);
}

static void
_init_button (G3kExample         *self,
              G3kButton          *button,
              graphene_point3d_t *pos,
              GCallback           press_cb,
              gpointer            press_cb_data)
{
  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, pos);
  g3k_object_set_matrix (G3K_OBJECT (button), &transform);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);
  g3k_object_manager_add_object (manager, G3K_OBJECT (button),
                                 G3K_INTERACTION_HOVERABLE
                                   | G3K_INTERACTION_DESTROY_WITH_PARENT
                                   | G3K_INTERACTION_BUTTON);

  g_signal_connect (button, "grab-start-event", press_cb, press_cb_data);
  g_signal_connect (button, "hover-event", (GCallback) _button_hover_cb, self);
  g_signal_connect (button, "hover-end-event", (GCallback) _button_hover_end_cb,
                    self);

  g3k_object_add_child (g3k_context_get_root (self->g3k), G3K_OBJECT (button),
                        0);
}

static void
_init_buttons (G3kExample *self)
{
  graphene_point3d_t button_pos = {.x = -1.5f, .y = 0.0f, .z = -2.5f};

  graphene_size_t size_meters = {.6f, .6f};

  gchar *quit_str[] = {"Quit"};
  self->quit_button = g3k_button_new (self->g3k, &size_meters);

  g3k_button_set_text (self->quit_button, 1, quit_str);

  _init_button (self, self->quit_button, &button_pos,
                (GCallback) _button_quit_press_cb, self);

  button_pos.x += 0.6f;

  gchar *keyboard_str[] = {"Toggle", "Cool", "Keyboard"};
  self->keyboard_button = g3k_button_new (self->g3k, &size_meters);

  g3k_button_set_text (self->keyboard_button, 3, keyboard_str);

  _init_button (self, self->keyboard_button, &button_pos,
                (GCallback) _button_keyboard_press_cb, self);
}

static void
keyboard_pressed_cb (G3kKeyboard *keyboard,
                     G3kKeyEvent *event,
                     gpointer     user_data)
{
  (void) keyboard;
  (void) user_data;
  g_print ("Yay, key %s was pressed!\n", event->string);
}

static void
_init_keyboard (G3kExample *self)
{
  self->keyboard = g3k_keyboard_new (self->g3k);

  GHashTable *modes = g3k_keyboard_get_modes (self->keyboard);
  g_autoptr (GList) containers = g_hash_table_get_values (modes);

  G3kObjectManager *manager = g3k_context_get_manager (self->g3k);

  for (GList *container = containers; container != NULL;
       container = container->next)
    {
      g3k_object_manager_add_container (manager, container->data);

      GSList            *buttons = g3k_container_get_children (container->data);
      graphene_point3d_t dummy = {.x = 0.0f, .y = 0.0f, .z = 0.0f};

      for (GSList *button = buttons; button != NULL; button = button->next)
        {
          _init_button (self, G3K_BUTTON (button->data), &dummy,
                        G_CALLBACK (g3k_keyboard_click_cb), self->keyboard);
        }
      g_slist_free (buttons);
    }

  g_signal_connect (self->keyboard, "key-press-event",
                    G_CALLBACK (keyboard_pressed_cb), self);
}

static G3kExample *
g3k_example_new (void)
{
  G3kContext *g3k = g3k_context_new_full (NULL, NULL, "button example",
                                          G3K_VERSION_HEX);
  if (!g3k)
    {
      g_printerr ("Could not init VR runtime.\n");
      return NULL;
    }

  G3kExample *self = (G3kExample *) g_object_new (G3K_TYPE_EXAMPLE, 0);

  self->g3k = g3k;

  self->background = g3k_background_new (self->g3k);
  g3k_object_add_child (g3k_context_get_root (self->g3k),
                        G3K_OBJECT (self->background), 0);

  _init_input_callbacks (self);

  _init_buttons (self);

  _init_keyboard (self);

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  g3k_context_start_renderer (g3k);

  return self;
}

static gboolean
_render_cb (G3kExample *self)
{
  g3k_context_render (self->g3k);
  return true;
}

int
main ()
{
  /* Set all locale-related variables to their values as specified in the
   * environment. This way, you can configure keyboard layout by setting
   * the LANG environment variable.
   */
  setlocale (LC_ALL, "");

  G3kExample *example = g3k_example_new ();

  g_timeout_add (1, (GSourceFunc) _render_cb, example);

  g_main_loop_run (example->loop);
  g_main_loop_unref (example->loop);

  g_object_unref (example);

  return EXIT_SUCCESS;
}
