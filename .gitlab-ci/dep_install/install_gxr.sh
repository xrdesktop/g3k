#!/bin/bash

mkdir -p deps
cd deps

git clone https://gitlab.freedesktop.org/xrdesktop/gxr.git
cd gxr
git checkout $1
meson build --prefix /usr
ninja -C build install
cd ../..
