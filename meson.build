project('g3k', 'c', version: '0.16.0',
        meson_version: '>= 0.52.0',
        default_options : [
          'c_std=c11',
          'warning_level=3',
        ],
)

gnome = import('gnome')

config_h = configuration_data()
configure_file(
  output: 'g3k-config.h',
  configuration: config_h,
)

project_args = ['-I' + meson.build_root(),
  '-Wno-overlength-strings'
]

compiler = meson.get_compiler('c')
compiler_id = compiler.get_id()

if compiler_id == 'clang'
  project_args += [
    '-Weverything',
    '-Wno-reserved-id-macro',
    '-Wno-documentation',
    '-Wno-documentation-unknown-command',
    '-Wno-padded',
    '-Wno-overlength-strings',
    '-Wno-disabled-macro-expansion',
    '-Wno-atomic-implicit-seq-cst',
    '-Wno-float-equal',
    '-Wno-used-but-marked-unused',
    '-Wno-assign-enum',
    '-Wno-gnu-folding-constant',
    '-Wno-cast-qual', # G_DEFINE_TYPE produces this
    '-Wno-covered-switch-default',
    '-Wno-reserved-identifier', # GObject
    '-Wno-declaration-after-statement',
    '-Wno-unused-macros', # GLib
  ]
endif

add_project_arguments([project_args], language: ['c'])


# Paths
prefix = get_option('prefix')
libdir = join_paths(prefix, get_option('libdir'))
includedir = join_paths(prefix, get_option('includedir'))
datadir = join_paths(prefix, get_option('datadir'))
bindir = join_paths(prefix, get_option('bindir'))
pkgdatadir = join_paths(datadir, meson.project_name())
desktopdir = join_paths(datadir, 'applications')
icondir = join_paths(datadir, 'icons')

src_inc = include_directories('src')

### Dependencies
gxr_version = ['>=0.16.0']
gxr_proj = subproject('gxr',
  default_options: ['api_doc=false', 'tests=false', 'examples=false'],
  required: false,
  version: gxr_version,
)
if gxr_proj.found()
  gxr_dep = gxr_proj.get_variable('gxr_dep')
else
  gxr_dep = dependency('gxr-0.16', include_type: 'system')
endif

json_glib_dep = dependency('json-glib-1.0')
pango_dep = dependency('pangocairo')
c = meson.get_compiler('c')
m_dep = c.find_library('m')
json_glib_dep = dependency('json-glib-1.0', version: '>= 1.2.0')

if get_option('shaderc_link_combined')
  # On arch only linking shared libglslang works, static fails.
  # On debian, only static libglslang is shipped, but linking it works.
  glslang_lib = c.find_library('glslang', static: false, required: false)
  if not glslang_lib.found()
    glslang_lib = c.find_library('glslang', static: true)
  endif

  spirv_lib = c.find_library('SPIRV', static: true)
  stdcpp_lib = c.find_library('stdc++')
  shaderc_deps = [
    glslang_lib,
    spirv_lib,
    stdcpp_lib,
    dependency('SPIRV-Tools'),
    dependency('shaderc_combined'),
    dependency('threads'),
  ]
else
  shaderc_deps = [dependency('shaderc')]
endif
canberra_dep = dependency('libcanberra')

subdir('shaders')
subdir('res')
subdir('src')

if get_option('tests')
    subdir('tests')
endif

if get_option('examples')
    subdir('examples')
endif

if get_option('api_doc')
    subdir('doc')
endif
